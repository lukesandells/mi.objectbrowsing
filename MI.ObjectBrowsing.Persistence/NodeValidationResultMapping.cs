﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using MI.Framework.ObjectBrowsing.Validation;
using MI.Persistence.NHibernate;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	public class NodeValidationResultMapping : ClassMapping<NodeValidationResult>
	{
		public NodeValidationResultMapping()
		{
			Lazy(false);
			Id(o => o.Id, id =>
			{
				id.Access(Accessor.Property);
				id.Generator(Generators.HighLow, g => g.Params(new { max_lo = 1000 }));
			});

			Property(o => o.DisplayMessage, property => { property.Access(Accessor.NoSetter); property.NotNullable(true); });
			Property(o => o.Type, property =>
			{
				property.Access(Accessor.NoSetter);
				property.NotNullable(true);
				property.Type<NodeValidationResultTypeType>();
			});

			ManyToOne(o => o.SourceNode, manyToOne =>
			{
				manyToOne.Column("SourceNodeId");
				manyToOne.Access(Accessor.NoSetter);
				manyToOne.Lazy(LazyRelation.NoLazy);
				manyToOne.Fetch(FetchKind.Join);
				manyToOne.Cascade(Cascade.None);
			});

			Index.Declare("IDX_NodeValidationResult_SourceNodeId", IndexType.Standard, "NodeValidationResult", new[] { "SourceNodeId"});
		}
	}
}
