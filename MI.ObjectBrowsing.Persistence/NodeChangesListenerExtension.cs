﻿using MI.Core;
using MI.Persistence;
using System.Threading.Tasks;


namespace MI.Framework.ObjectBrowsing.Persistence
{
	public static class NodeChangesListenerExtension
	{
		/// <summary>
		/// Persists the changes this listener has picked up 
		/// </summary>
		/// <param name="listener"></param>
		public static async Task PersistChanges(this NodeChangesListener listener)
		{
            await Async.Using(new PersistenceScope(TransactionOption.Required), async scope =>
            {
                // Delete the permanently deleted nodes
                listener.PermanentlyDeletedNodes.ForEach(async node => await scope.DeleteAsync(node).ConfigureAwait(false));

                // Save newly created property link nodes
                listener.AddedNodes.ForEach(async node => await scope.PersistAsync(node, PersistBehaviour.SaveIfNotInferable).ConfigureAwait(false));
                scope.Done();
            }).ConfigureAwait(false);
		}
	}
}
