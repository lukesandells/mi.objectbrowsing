﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using MI.Framework.ObjectBrowsing.Transactions;
using MI.Persistence.NHibernate;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	public class SetNodePropertyTransactionLogEntryMapping : SubclassMapping<SetNodePropertyTransactionLogEntry>
	{
		public SetNodePropertyTransactionLogEntryMapping()
		{
			Lazy(false);
			Extends(typeof(TransactionLogEntry));
			DiscriminatorValue(TransactionLogEntryType.SetNodeProperty);

			Property(o => o.FormerValue, property => { property.Column("FormerPrimitiveValue"); property.Access(Accessor.NoSetter); });
			Property(o => o.MemberId, property => { property.Column("FromMemberId"); property.Access(Accessor.NoSetter); });

			ManyToOne(o => o.Node, manyToOne =>
			{
				manyToOne.Access(Accessor.NoSetter);
				manyToOne.Cascade(Cascade.None); // No cascade because it is not the log entry's responsibility to manage node persistence
				manyToOne.Column("DefinitionNodeId");
				manyToOne.Fetch(FetchKind.Join);
				manyToOne.Lazy(LazyRelation.NoLazy);
			});

			
			Index.DeclareIfNotDeclared("IDX_TransactionLogEntry_DefinitionNodeId", IndexType.Standard, "TransactionLogEntry",
				new string[] { "DefinitionNodeId" });
		}
	}
}
