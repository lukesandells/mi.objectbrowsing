﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using MI.Framework.ObjectBrowsing.Transactions;
using MI.Persistence.NHibernate;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	public class MoveNodeTransactionLogEntryMapping : SubclassMapping<MoveNodeTransactionLogEntry>
	{
		public MoveNodeTransactionLogEntryMapping()
		{
			Lazy(false);
			Extends(typeof(TransactionLogEntry));
			DiscriminatorValue(TransactionLogEntryType.MoveNode);

			Property(o => o.SourceFolderMemberId, property => { property.Column("FromMemberId"); property.Access(Accessor.NoSetter); });
			Property(o => o.DestinationFolderMemberId, property => { property.Column("ToMemberId"); property.Access(Accessor.NoSetter); });

			ManyToOne(o => o.MovedNode, manyToOne =>
			{
				manyToOne.Access(Accessor.NoSetter);
				manyToOne.Cascade(Cascade.None); // No cascade because it is not the log entry's responsibility to manage node persistence
				manyToOne.Column("BrowserNodeId");
				manyToOne.Fetch(FetchKind.Join);
				manyToOne.Lazy(LazyRelation.NoLazy);
			});

			ManyToOne(o => o.SourceNode, manyToOne =>
			{
				manyToOne.Access(Accessor.NoSetter);
				manyToOne.Cascade(Cascade.None); // No cascade because it is not the log entry's responsibility to manage node persistence
				manyToOne.Column("FromParentNodeId");
				manyToOne.Fetch(FetchKind.Join);
				manyToOne.Lazy(LazyRelation.NoLazy);
			});

			ManyToOne(o => o.DestinationNode, manyToOne =>
			{
				manyToOne.Access(Accessor.NoSetter);
				manyToOne.Cascade(Cascade.None); // No cascade because it is not the log entry's responsibility to manage node persistence
				manyToOne.Column("ToParentNodeId");
				manyToOne.Fetch(FetchKind.Join);
				manyToOne.Lazy(LazyRelation.NoLazy);
			});

			Index.DeclareIfNotDeclared("IDX_TransactionLogEntry_BrowserNodeId", IndexType.Standard, "TransactionLogEntry",
				new string[] { "BrowserNodeId" });
			Index.DeclareIfNotDeclared("IDX_TransactionLogEntry_FromParentNodeId", IndexType.Standard, "TransactionLogEntry",
				new string[] { "FromParentNodeId" });
			Index.DeclareIfNotDeclared("IDX_TransactionLogEntry_ToParentNodeId", IndexType.Standard, "TransactionLogEntry",
				new string[] { "ToParentNodeId" });
		}
	}
}
