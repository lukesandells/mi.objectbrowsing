﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

using MI.Persistence.NHibernate;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	public class BrowserFolderMapping : ClassMapping<BrowserFolder>
	{
		public BrowserFolderMapping()
		{
			Id(o => o.Id, id => id.Access(Accessor.NoSetter));

			Discriminator(discriminator => 
			{
				discriminator.Column("MetaTypeId");
				discriminator.Type<NHibernate.Type.EnumType<FolderMetaType>>();
				discriminator.NotNullable(true);
			});

			DiscriminatorValue(FolderMetaType.NodeFolder);

			Property(o => o.IsEmpty, property => property.Access(Accessor.NoSetter));
			Property(o => o.AllowsDefinitionNodes, property => property.Access(Accessor.ReadOnly));
			Property(o => o.IsPrimary, property => property.Access(Accessor.ReadOnly));
			Property(o => o.IsPropertyLinksFolder, property => property.Access(Accessor.ReadOnly));
			Property(o => o.IsInverse, property => property.Access(Accessor.ReadOnly));
			Property(o => o.IsRecycleBin, property => property.Access(Accessor.ReadOnly));
			Property(o => o.SystemManaged, property => property.Access(Accessor.ReadOnly));
			Property(o => o.MemberId, property => property.Access(Accessor.NoSetter));
			
			ManyToOne(o => o.Parent, manyToOne =>
			{
				manyToOne.Column("ParentNodeId");
				manyToOne.Access(Accessor.NoSetter);
				// Nodes are the aggregate root and folders are part of the aggregate root.  So shouldn't be querying directly for folders.
				manyToOne.Fetch(FetchKind.Select);
				manyToOne.Cascade(Cascade.ReAttach);
			});

			Set(o => o.Nodes, list =>
			{
				list.Key(k => k.Column("ParentFolderId"));
				list.Access(Accessor.Field);
				list.Fetch(CollectionFetchMode.Subselect);
				list.Cascade(Cascade.None);
				list.Inverse(true);
			}, rel => rel.OneToMany());

			Index.Declare("IDX_BrowserFolder_ParentNodeId", IndexType.Standard, nameof(BrowserFolder), new string[] { "ParentNodeId" });
		}
	}
}
