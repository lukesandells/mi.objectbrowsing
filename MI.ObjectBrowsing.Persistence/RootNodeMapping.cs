﻿using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	public class RootNodeMapping : SubclassMapping<RootNode>
	{
		public RootNodeMapping()
		{
			Extends(typeof(ParentNode));
			DiscriminatorValue(NodeMetaType.RootNode);
		}
	}
}
