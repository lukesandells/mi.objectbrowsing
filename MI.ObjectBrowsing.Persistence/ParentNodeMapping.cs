﻿using MI.Persistence.NHibernate;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	public class ParentNodeMapping : SubclassMapping<ParentNode>
	{
		public ParentNodeMapping()
		{
			Extends(typeof(BrowserNode));
			DiscriminatorValue(null);

			Set(o => o.NonEmptyNonInverseFolders, set =>
			{
				set.Key(k => k.Column("ParentNodeId"));
				set.Access(Accessor.NoSetter);
				set.Inverse(true);
				set.Fetch(CollectionFetchMode.Select);
				set.BatchSize(100);
				set.Cascade(Cascade.Persist | Cascade.ReAttach | Cascade.Merge | Cascade.Remove | Cascade.DeleteOrphans);
			}, rel => rel.OneToMany());
			
			// Defined as a bag so that the collection isn't initialised upon adding to the collection
			Bag(o => o.Descendants, bag => {
				bag.Table("BrowserNodeDescendant");
				bag.Key(k => k.Column("AscendantId"));
				bag.Inverse(true);
				bag.Fetch(CollectionFetchMode.Select);
				bag.Lazy(CollectionLazy.Extra);
				bag.Cascade(Cascade.None);
				bag.Access(Accessor.NoSetter);
			}, r => r.ManyToMany(map => map.Column("DescendantId")));

			Index.Declare("IDX_BrowserNodeDescendant_AscendantId_DescendantId", IndexType.Standard, "BrowserNodeDescendant", 
				new[] { "AscendantId", "DescendantId" });
		}
	}
}
