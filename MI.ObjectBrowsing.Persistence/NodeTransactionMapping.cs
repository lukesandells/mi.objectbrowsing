﻿using MI.Framework.ObjectBrowsing.Transactions;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	public class NodeTransactionMapping : ClassMapping<NodeTransaction>
	{
		public NodeTransactionMapping()
		{
			Mutable(false);
			Lazy(false);
			Id(o => o.Id, id =>
			{
				id.Access(Accessor.Property);
				id.Generator(Generators.HighLow, g => g.Params(new { max_lo = 100 }));
			});

			Property(o => o.State, property => 
			{
				property.NotNullable(true);
				property.Type<EnumType<NodeTransactionState>>();
				property.Access(Accessor.NoSetter);
			});
			Property(o => o.IsRollbackTransaction, property => { property.NotNullable(true); property.Access(Accessor.NoSetter); });
			Property(o => o.TransactionNumber, property => { property.NotNullable(true); property.Access(Accessor.NoSetter); });

			Bag(o => o.LogEntries, bag =>
			{
				bag.Access(Accessor.Field); // Field access because the getter returns as sorting facade
				bag.Cascade(Cascade.Persist | Cascade.DeleteOrphans);
				bag.Fetch(CollectionFetchMode.Select);
				bag.Inverse(true);
				bag.Key(key => key.Column("NodeTransactionId"));
			}, r => r.OneToMany());
		}
	}
}
