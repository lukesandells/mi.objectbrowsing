﻿namespace MI.Framework.ObjectBrowsing.Persistence
{
	public enum TransactionLogEntryType
	{
		NewNode = 0,

		DeleteNode = 1,

		MoveNode = 2,

		SetNodeProperty = 3
	}
}
