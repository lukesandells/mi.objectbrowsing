﻿using MI.Core;
using MI.Persistence;
using MI.Core.Validation;
using System.Threading.Tasks;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	public static class RecycleBinExtensions
	{
		public static async Task EmptyAndPersist(this RecycleBin recycleBin)
		{
			recycleBin = ArgumentValidation.AssertNotNull(recycleBin, nameof(recycleBin));
            await Async.Using(new PersistenceScope(TransactionOption.Required), async scope =>
            {
                await scope.AttachAsync(recycleBin.Parent).ConfigureAwait(false);
                var contents = recycleBin.Nodes.ToHashSet();
                recycleBin.Empty();
                foreach (var node in contents)
                {
                    await scope.DeleteAsync(node).ConfigureAwait(false);
                }

                scope.Done();
            }).ConfigureAwait(false);
		}
	}
}
