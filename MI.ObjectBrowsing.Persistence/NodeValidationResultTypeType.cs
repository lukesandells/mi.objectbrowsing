﻿using System;
using System.Data;
using System.Data.Common;
using NHibernate;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;
using MI.Core.Validation;
using MI.Framework.ObjectBrowsing.Validation;
using MI.Persistence.NHibernate.Types;
using NHibernate.Engine;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	/// <summary>
	/// The persistent type for <see cref="NodeValidationResultType"/>.
	/// </summary>
	public class NodeValidationResultTypeType : IUserType
	{
		/// <summary>
		/// The list of SQL types to which the <see cref="NodeValidationResultType"/> type maps.
		/// </summary>
		private SqlType[] _sqlTypes = new SqlType[] { NHibernateUtil.String.SqlType };

		/// <summary>
		/// Tests two <see cref="NodeValidationResultType" /> values for equality.
		/// </summary>
		/// <param name="x">The <see cref="NodeValidationResultType"/> value on the left hand side of the equality operator.</param>
		/// <param name="y">The <see cref="NodeValidationResultType"/> value on the right hand side of the equality operator.</param>
		/// <returns>A <see cref="Boolean"/> indicating whether the two <see cref="NodeValidationResultType"/> values are equal.</returns>
		public new bool Equals(object x, object y)
		{
			return x == y;
		}

		/// <summary>
		/// Get a hashcode for the instance, consistent with persistence "equality".
		/// </summary>
		/// <param name="x">The object for which to get the hashcode.</param>
		/// <returns>The hashcode for the given object.</returns>
		public int GetHashCode(object x)
		{
			if (x != null)
			{
				return x.GetHashCode();
			}

			return 0;
		}

		/// <summary>
		/// Creates a deep copy of a <see cref="NodeValidationResultType"/> value.
		/// </summary>
		/// <param name="value">The <see cref="NodeValidationResultType"/> value to be copied.</param>
		/// <returns>A deep copy of the given <see cref="NodeValidationResultType"/> value.</returns>
		public object DeepCopy(object value)
		{
			// The original value is returned rather than a deep copy because the NodeValidationResultType library works on the basis that
			// there is a single global instance of each NodeValidationResultType.
			return value;
		}

		/// <summary>
		/// Gets a value indicating whether the <see cref="NodeValidationResultType"/> type is mutable.
		/// </summary>
		/// <remarks>
		/// The <see cref="NodeValidationResultType"/> type is not mutable so this method always returns <c>false</c>.
		/// </remarks>
		public bool IsMutable
		{
			get { return false; }
		}

		/// <summary>
		/// Performs a read of a <see cref="NodeValidationResultType"/> value from an implementation of <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="rs">An implmentation of <see cref="IDataReader"/> containing the <see cref="NodeValidationResultType"/> value to be read.</param>
		/// <param name="names">The array of field names to map to the <see cref="NodeValidationResultType"/> value.</param>
		/// <param name="owner">The containing entity.</param>
		/// <param name="session">The session for which the operation is done. Allows access to
		/// <c>Factory.Dialect</c> and <c>Factory.ConnectionProvider.Driver</c> for adjusting to
		/// database or data provider capabilities.</param>
		/// <returns>The <see cref="NodeValidationResultType"/> value read from the <see cref="IDataReader"/> implementation.</returns>
		public object NullSafeGet(DbDataReader rs, string[] names, ISessionImplementor session, object owner)
		{
			// Get the field value from the database
			var validationResultTypeName = (string)NHibernateUtil.String.NullSafeGet(rs, names[0], session);

			// Is it null?
			if (validationResultTypeName == null)
			{
				// Yep, then return null
				return null;
			}
			else
			{
				// Not null, so get the string value and look up the NodeValidationResultType from the class
				return NodeValidationResultType.Parse(validationResultTypeName);
			}
		}

		/// <summary>
		/// Performs a write of a <see cref="ExtensibleEnumType{TEnum}"/> value to the parameters of an implementation of <see cref="IDbCommand"/>.
		/// </summary>
		/// <param name="cmd">An implementation of <see cref="IDbCommand"/> representing a write command to be executed against the database.</param>
		/// <param name="value">The <see cref="ExtensibleEnumType{TEnum}"/> value to be written to the database.</param>
		/// <param name="index">The command parameter index.</param>
		/// <param name="session">The session for which the operation is done. Allows access to
		/// <c>Factory.Dialect</c> and <c>Factory.ConnectionProvider.Driver</c> for adjusting to
		/// database or data provider capabilities.</param>
		public void NullSafeSet(DbCommand cmd, object value, int index, ISessionImplementor session)
		{
			cmd = ArgumentValidation.AssertNotNull(cmd, "cmd");

			if (value == null)
			{
				cmd.Parameters[index].Value = DBNull.Value;
			}
			else
			{
				cmd.Parameters[index].Value = ((NodeValidationResultType)value).GetType().Name;
			}
		}

		/// <summary>
		/// Gets the .NET type represented by this NHibernate type.
		/// </summary>
		public Type ReturnedType
		{
			get { return typeof(NodeValidationResultType); }
		}

		/// <summary>
		/// Gets the SQL types to which the <see cref="ExtensibleEnumType{TEnum}"/> type maps.
		/// </summary>
		public SqlType[] SqlTypes
		{
			get { return _sqlTypes; }
		}

		/// <summary>
		/// Called when an entity is read from the second-level cache.
		/// </summary>
		/// <param name="cached">The cached object.</param>
		/// <param name="owner">The owning object.</param>
		/// <returns>A copy of the entity, reconstituted from the cache.</returns>
		public object Assemble(object cached, object owner)
		{
			return DeepCopy(cached);
		}

		/// <summary>
		/// Called when an entity is written to the second-level cache.
		/// </summary>
		/// <param name="value">The object to be cached.</param>
		/// <returns>A copy of the object to be stored in the cache.</returns>
		public object Disassemble(object value)
		{
			return DeepCopy(value);
		}

		/// <summary>
		/// During merge, replace the existing (target) value in the entity we are merging to with a new 
		/// (original) value from the detached entity we are merging. For immutable objects, or null 
		/// values, it is safe to simply return the first parameter. For mutable objects, it is safe to 
		/// return a copy of the first parameter. However, since composite user types often define component 
		/// values, it might make sense to recursively replace component values in the target object.
		/// </summary>
		/// <param name="original">The new value.</param>
		/// <param name="target">The existing value.</param>
		/// <param name="owner">The owning entity.</param>
		/// <returns>The replacement value.</returns>
		public object Replace(object original, object target, object owner)
		{
			// Immuatable type, so okay to return original
			return original;
		}
	}
}
