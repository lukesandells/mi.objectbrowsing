﻿using System.Linq;
using System.Collections.Generic;
using MI.Framework.ObjectBrowsing.Transactions;
using MI.Core;
using MI.Persistence;
using MI.Core.Validation;
using System.Threading.Tasks;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	/// <summary>
	/// Persistence extension methods for the <see cref="NodeTransaction"/> class.
	/// </summary>
	public static class NodeTransactionExtensions
	{
		/// <summary>
		/// Rolls back the transaction and persists the changes to the database.
		/// </summary>
		/// <param name="transaction">The transaction to roll back.</param>
		/// <returns>The rollback transaction.</returns>
		public static async Task<NodeTransaction> RollBackAndPersist(this NodeTransaction transaction)
		{
            NodeTransaction rollbackTransaction = null;
			transaction = ArgumentValidation.AssertNotNull(transaction, nameof(transaction));
            await Async.Using(new PersistenceScope(TransactionOption.Required), async scope =>
            {
                await transaction.Attach().ConfigureAwait(false);
                using (var listener = NodeActivityLog.AddListener<NodeChangesListener>())
                {
                    rollbackTransaction = transaction.RollBack();

                    // Delete the permanently deleted nodes
                    listener.PermanentlyDeletedNodes.ForEach(async node => await scope.DeleteAsync(node).ConfigureAwait(false));

                    // Save newly created nodes
                    listener.AddedNodes.ForEach(async node => await scope.SaveAsync(node).ConfigureAwait(false));

                    scope.Done();
                }
            }).ConfigureAwait(false);

            return rollbackTransaction;
		}

		/// <summary>
		/// Attaches the transaction to the current persistence scope.
		/// </summary>
		/// <param name="transaction">The transaction to attach.</param>
		private static async Task Attach(this NodeTransaction transaction)
		{
			var nodesToAttach = new HashSet<BrowserNode>();
			foreach (var logEntry in transaction.LogEntries)
			{
				if (logEntry is NewNodeTransactionLogEntry newNodeEntry)
				{
					nodesToAttach.Add(newNodeEntry.NewNode);
				}
				else if (logEntry is DeleteNodeTransactionLogEntry deletedNodeEntry)
				{
					nodesToAttach.Add(deletedNodeEntry.DeletedNode);
					nodesToAttach.Add(deletedNodeEntry.DeletedFromParentNode);
				}
				else if (logEntry is MoveNodeTransactionLogEntry movedNodeEntry)
				{
					nodesToAttach.Add(movedNodeEntry.MovedNode);
					nodesToAttach.Add(movedNodeEntry.SourceNode);
				}
				else if (logEntry is SetNodePropertyTransactionLogEntry nodePropertySetEntry)
				{
					nodesToAttach.Add(nodePropertySetEntry.Node);
				}
			}

            await Async.Using(new PersistenceScope(TransactionOption.Supported), async scope =>
            {
                await nodesToAttach.AttachWithAscendants().ConfigureAwait(false);
                nodesToAttach.Union(nodesToAttach.SelectMany(node => node.DescendantsOrderedByDepth)).ForEach(async node => await scope.AttachAsync(node).ConfigureAwait(false));
                scope.Done();
            }).ConfigureAwait(false);
		}
	}
}
