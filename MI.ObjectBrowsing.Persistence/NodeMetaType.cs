﻿using System;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	public static class NodeMetaType
	{
		public static Guid RootNode { get; } = new Guid("cfbceb02-4eb7-4bdb-b69a-d0f9530254cb");
		public static Guid DefinitionNode { get; } = new Guid("3ad8966e-b80c-4629-b8aa-7a1c67853452");
		public static Guid LinkNode { get; } = new Guid("7e306501-f522-4611-b3ba-0be24cbdb402");
	}
}
