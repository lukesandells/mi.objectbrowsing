﻿using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System;
using MI.Persistence;
using MI.Persistence.NHibernate;
using MI.Core;
using MI.Core.Validation;
using System.Threading.Tasks;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	/// <summary>
	/// Persistence extension methods for enumerables of <see cref="BrowserNode"/> objects.
	/// </summary>
	public static class BrowserNodeEnumerableExtensions
	{
		/// <summary>
		/// Saves a collection of transient nodes and all their descendant transient nodes to the database.
		/// </summary>
		/// <param name="nodes">The nodes to save.</param>
		public static async Task Save(this IEnumerable<BrowserNode> nodes)
		{
            await Async.Using(new PersistenceScope(TransactionOption.Required), async scope =>
            {
                // Save the nodes
                nodes.ForEach(async node => await scope.PersistAsync(node, PersistBehaviour.SaveIfNotInferable).ConfigureAwait(false));

                // Need to save the definition nodes before the link nodes because the link nodes 
                // may reference the definition nodes
                var descendants = nodes.SelectMany(node => node.DescendantsOrderedByDepth);
                descendants.OfType<DefinitionNode>().ForEach(async n => await scope.PersistAsync(n, PersistBehaviour.SaveIfNotInferable).ConfigureAwait(false));
                descendants.OfType<LinkNode>().ForEach(async n => await scope.PersistAsync(n, PersistBehaviour.SaveIfNotInferable).ConfigureAwait(false));
                scope.Done();
            }).ConfigureAwait(false);
		}

		/// <summary>
		/// Updates the collection of nodes in the database.
		/// </summary>
		/// <param name="nodesToUpdate">The nodes to update.</param>
		public static async Task Update(this IEnumerable<BrowserNode> nodesToUpdate)
		{
			nodesToUpdate = ArgumentValidation.AssertNotNull(nodesToUpdate, nameof(nodesToUpdate));
            await Async.Using(new PersistenceScope(TransactionOption.Required), async scope =>
            {
                nodesToUpdate.ForEach(async nodeToUpdate => await scope.UpdateAsync(nodeToUpdate).ConfigureAwait(false));
                scope.Done();
            }).ConfigureAwait(false);
		}

		/// <summary>
		/// Attaches the collection of nodes and their ascendants to the current persistence session.
		/// </summary>
		/// <param name="nodes">The nodes to attach with their ascendants.</param>
		/// <returns>The attached nodes.</returns>
		internal static async Task<IEnumerable<BrowserNode>> AttachWithAscendants(this IEnumerable<BrowserNode> nodes)
		{
            IEnumerable<BrowserNode> attachedNodes = null;
            await Async.Using(new PersistenceScope(TransactionOption.Supported), async scope =>
            {
                attachedNodes = nodes.Union(nodes.SelectMany(node => node.Ascendants).Distinct());
                attachedNodes.ForEach(async node => await scope.AttachAsync(node).ConfigureAwait(false));
                scope.Done();
            }).ConfigureAwait(false);

            return attachedNodes;
		}

		/// <summary>
		/// Pre-fetches the descendant nodes (including any related target objects with registered pre-fetch strategies) of the given node.
		/// </summary>
		/// <param name="node">The node for which to pre-fetch descendants.</param>
		/// <param name="operationType">The type of node operation for which to pre-fetch.</param>
		public static async Task PreFetchNodeDescendants(this ParentNode node, OperationType operationType)
		{
			var stopwatch = new Stopwatch();
			stopwatch.Start();

            await Async.Using(new PersistenceScope(TransactionOption.Supported), async scope =>
            {
                // Get and execute the distinct set of registered pre-fetch strategies
                NodeType.RegisteredTypes.Select(nodeType => nodeType.Configuration.PreFetchStrategy)
                    .Where(strategy => strategy != null)
                    .Distinct()
                    .ForEach(strategy => strategy.PreFetchTargetObjects(node, operationType));

                if (operationType != OperationType.CopyTo)
                {
                    // Pre-fetch inbound links of descendants.  Not needed for CopyTo because inbound links aren't read during a copy operation.
                    scope.Query<DefinitionNode>().Where(element => element.Ascendants.Contains(node))
                        .FetchMany(element => element.InboundLinks)
                        .ThenFetchMany(element => element.Ascendants)
                        .ToFuture();
                }

                if (operationType == OperationType.MoveTo)
                {
                    // Pre-fetch validation results.  Not needed for CopyTo because validation results of copied nodes don't change.
                    // Not needed for Delete because nodes aren't validated when deleted.
                    scope.Query<BrowserNode>().Where(element => element.Ascendants.Contains(node))
                        .FetchMany(element => element.ValidationResults)
                        .ThenFetch(element => element.SourceNode)
                        .ToFuture();
                }

                // Descendants pre-fetch - parent folder and parent
                scope.Query<BrowserNode>().Where(element => element.Ascendants.Contains(node))
                    .Fetch(element => element.ParentFolder)
                    .ThenFetch(folder => folder.Parent)
                    .ToFuture();

                // Descendants pre-fetch - child folders/nodes
                scope.Query<ParentNode>().Where(element => element.Ascendants.Contains(node))
                    .FetchMany(element => element.NonEmptyNonInverseFolders)
                    .ThenFetchMany(folder => folder.Nodes)
                    .ToFuture();

                // Descendants pre-fetch - ascendants of descendants
                scope.Query<BrowserNode>().Where(element => element.Ascendants.Contains(node))
                    .FetchMany(element => element.Ascendants)
                    .ToFuture();

                // Descendants pre-fetch - decendants of descendants
                var ret = await scope.Query<ParentNode>().Where(element => element.Ascendants.Contains(node))
                    .FetchMany(element => element.Descendants)
                    .ToFuture().GetEnumerableAsync().ConfigureAwait(false);
                ret.ToList();

                scope.Done();
            }).ConfigureAwait(false);

			stopwatch.Stop();
			Console.WriteLine("Node descendant pre-fetch took {0} seconds.", stopwatch.Elapsed.TotalSeconds);
		}

		/// <summary>
		/// Copies the collection of nodes to a folder and saves the copies to the database.
		/// </summary>
		/// <param name="nodesToCopy">The nodes to copy and save.</param>
		/// <param name="destinationFolder">The destination folder in which to place the copies.</param>
		/// <param name="prefetch">Whether or not to prefetch related nodes before executing</param>
		/// <returns>A read only dictionary of the copied nodes, indexed by the original nodes.</returns>
		public static async Task<IReadOnlyDictionary<BrowserNode, BrowserNode>> CopyAndPersistTo(this IEnumerable<BrowserNode> nodesToCopy, 
			BrowserFolder destinationFolder, bool prefetch)
		{
            IReadOnlyDictionary<BrowserNode, BrowserNode> copies = null;
            nodesToCopy = ArgumentValidation.AssertNotNull(nodesToCopy, nameof(nodesToCopy));
			destinationFolder = ArgumentValidation.AssertNotNull(destinationFolder, nameof(destinationFolder));
            await Async.Using(new PersistenceScope(TransactionOption.Required), async scope =>
            {
                // Pre-fetch the descendants of the nodes being copied
                if (prefetch)
                {
                    nodesToCopy.OfType<DefinitionNode>().ForEach(async node => await node.PreFetchNodeDescendants(OperationType.CopyTo).ConfigureAwait(false));
                }

                // Attach the destination and its ascendants because their collections will change
                destinationFolder.Parent.AttachWithAscendants();

                // Execute the copy
                copies = nodesToCopy.CopyTo(destinationFolder);

                // Save all copied nodes
                await copies.Values.Save().ConfigureAwait(false);
                scope.Done();
            }).ConfigureAwait(false);

            return copies;
        }

		/// <summary>
		/// Copies the collection of nodes to a folder and saves the copies to the database.
		/// </summary>
		/// <param name="nodesToCopy">The nodes to copy and save.</param>
		/// <param name="destinationNode">The destination node to which to copy the nodes.</param>
		/// <param name="prefetch">Whether or not to prefetch related nodes before executing</param>
		/// <returns>A read only dictionary of the copied nodes, indexed by the original nodes.</returns>
		public static async Task<IReadOnlyDictionary<BrowserNode, BrowserNode>> CopyAndPersistTo(this IEnumerable<BrowserNode> nodesToCopy,
			ParentNode destinationNode, bool prefetch)
		{
            IReadOnlyDictionary<BrowserNode, BrowserNode> copies = null;
            nodesToCopy = ArgumentValidation.AssertNotNull(nodesToCopy, nameof(nodesToCopy));
			destinationNode = ArgumentValidation.AssertNotNull(destinationNode, nameof(destinationNode));
            await Async.Using(new PersistenceScope(TransactionOption.Required), async scope =>
            {
                // Pre-fetch the descendants of the nodes being copied
                if (prefetch)
                {
                    nodesToCopy.OfType<DefinitionNode>().ForEach(async node => await node.PreFetchNodeDescendants(OperationType.CopyTo).ConfigureAwait(false));
                }

                // Attach the destination and its ascendants because their collections will change
                destinationNode.AttachWithAscendants();

                // Execute the copy
                copies = nodesToCopy.CopyTo(destinationNode);

                // Save all copied nodes
                await copies.Values.Save().ConfigureAwait(false);
                scope.Done();
            }).ConfigureAwait(false);

            return copies;
		}

		/// <summary>
		/// Deletes the collection of nodes and reflects the changes to the database.
		/// </summary>
		/// <param name="nodesToDelete">The nodes to delete.</param>
		/// <param name="prefetch">Whether or not to prefetch related nodes before executing</param>
		public static async Task DeleteAndPersist(this IEnumerable<BrowserNode> nodesToDelete, bool prefetch)
		{
			await DeleteAndPersist(nodesToDelete, false, prefetch).ConfigureAwait(false);
		}

		/// <summary>
		/// Deletes the collection of nodes and reflects the changes to the database.
		/// </summary>
		/// <param name="nodesToDelete">The nodes to delete.</param>
		/// <param name="deletePermanently"><c>true</c> to delete the nodes permanently or <c>false</c> to move the nodes to the recycle bin.</param>
		/// <param name="prefetch">Whether or not to prefetch related nodes before executing</param>
		public static async Task DeleteAndPersist(this IEnumerable<BrowserNode> nodesToDelete, bool deletePermanently, bool prefetch)
		{
			nodesToDelete = ArgumentValidation.AssertNotNull(nodesToDelete, nameof(nodesToDelete));
			await Async.Using(new PersistenceScope(TransactionOption.Required), async scope =>
			{
				// Pre-fetch the descendants
				if (prefetch)
				{
					nodesToDelete.OfType<DefinitionNode>().ForEach(node => node.PreFetchNodeDescendants(OperationType.Delete).ConfigureAwait(false));
				}
				
				// Attach the ascendants because their collections will change
				await nodesToDelete.AttachWithAscendants().ConfigureAwait(false);

				using (var listener = NodeActivityLog.AddListener<NodeChangesListener>())
				{
					nodesToDelete.Delete(deletePermanently);

					// Delete the permanently deleted nodes
					listener.PermanentlyDeletedNodes.ForEach(async node => await scope.DeleteAsync(node).ConfigureAwait(false));
					scope.Done();
				}
			}).ConfigureAwait(false);
		}

		/// <summary>
		/// Adds the collection of definition nodes to a folder as a link.
		/// </summary>
		/// <param name="nodesToAdd">The definition nodes to add to the destination folder.</param>
		/// <param name="destinationFolder">The folder into which to add the definition nodes (as links).</param>
		/// <returns>The new link nodes, indexed by the added definition nodes.</returns>
		public static async Task<IReadOnlyDictionary<DefinitionNode, LinkNode>> AddAndPersistTo(this IEnumerable<DefinitionNode> nodesToAdd, 
			BrowserFolder destinationFolder)
		{
            IReadOnlyDictionary<DefinitionNode, LinkNode> linkNodes = null;
			nodesToAdd = ArgumentValidation.AssertNotNull(nodesToAdd, nameof(nodesToAdd));
			destinationFolder = ArgumentValidation.AssertNotNull(destinationFolder, nameof(destinationFolder));
            await Async.Using(new PersistenceScope(TransactionOption.Required), async scope =>
            {
                // Attach the destination node and its ascendants because their collections will change
                destinationFolder.Parent.AttachWithAscendants();

                linkNodes = nodesToAdd.AddTo(destinationFolder);
                await linkNodes.Values.Save().ConfigureAwait(false);
                scope.Done();
            }).ConfigureAwait(false);

            return linkNodes;
		}

		/// <summary>
		/// Adds the collection of definition nodes to a parent node as a link.
		/// </summary>
		/// <param name="nodesToAdd">The definition nodes to add to the destination node.</param>
		/// <param name="destinationNode">The parent node into which to add the definition nodes (as links).</param>
		/// <returns>The new link nodes, indexed by the added definition nodes.</returns>
		public static async Task<IReadOnlyDictionary<DefinitionNode, LinkNode>> AddAndPersistTo(this IEnumerable<DefinitionNode> nodesToAdd, 
			DefinitionNode destinationNode)
		{
            IReadOnlyDictionary<DefinitionNode, LinkNode> linkNodes = null;
            nodesToAdd = ArgumentValidation.AssertNotNull(nodesToAdd, nameof(nodesToAdd));
			destinationNode = ArgumentValidation.AssertNotNull(destinationNode, nameof(destinationNode));
            await Async.Using(new PersistenceScope(TransactionOption.Required), async scope =>
            {
                // Attach the destination node and its ascendants because their collections will change
                destinationNode.AttachWithAscendants();

                linkNodes = nodesToAdd.AddTo(destinationNode);
                await linkNodes.Values.Save().ConfigureAwait(false);
                scope.Done();
            }).ConfigureAwait(false);

            return linkNodes;
		}

		/// <summary>
		/// Moves the collection of nodes to the given destination folder and reflects the changes to the database.
		/// </summary>
		/// <param name="nodesToMove">The nodes to move.</param>
		/// <param name="destinationFolder">The folder to which to move the nodes.</param>
		public static async Task MoveAndPersistTo(this IEnumerable<BrowserNode> nodesToMove, BrowserFolder destinationFolder)
		{
			nodesToMove = ArgumentValidation.AssertNotNull(nodesToMove, nameof(nodesToMove));
			destinationFolder = ArgumentValidation.AssertNotNull(destinationFolder, nameof(destinationFolder));
            await Async.Using(new PersistenceScope(TransactionOption.Required), async scope =>
            {
                // Attach the ascendants of the nodes to be moved and the destination because their collections will change
                await nodesToMove.Union(new[] { destinationFolder.Parent }).AttachWithAscendants().ConfigureAwait(false);

                // Execute the move
                nodesToMove.OfType<DefinitionNode>().ForEach(async node => await node.PreFetchNodeDescendants(OperationType.MoveTo).ConfigureAwait(false));
                using (var listener = NodeActivityLog.AddListener<NodeChangesListener>())
                {
                    nodesToMove.MoveTo(destinationFolder);
                    listener.PersistChanges();
                    scope.Done();
                }
            }).ConfigureAwait(false);
		}

		/// <summary>
		/// Moves the collection of nodes to the given destination node and reflects the changes to the database.
		/// </summary>
		/// <param name="nodesToMove">The nodes to move.</param>
		/// <param name="destinationNode">The node to which to move the nodes.</param>
		/// <param name="prefetch">Whether or not to prefetch related nodes before executing</param>
		public static async Task MoveAndPersistTo(this IEnumerable<BrowserNode> nodesToMove, ParentNode destinationNode, bool prefetch)
		{
			nodesToMove = ArgumentValidation.AssertNotNull(nodesToMove, nameof(nodesToMove));
            await Async.Using(new PersistenceScope(TransactionOption.Required), async scope =>
            {
                // Attach the ascendants of the nodes to be moved and the destination because their collections will change
                await nodesToMove.Union(new[] { destinationNode }).AttachWithAscendants().ConfigureAwait(false);

                // Execute the move
                if (prefetch)
                {
                    nodesToMove.OfType<DefinitionNode>().ForEach(async node => await node.PreFetchNodeDescendants(OperationType.MoveTo).ConfigureAwait(false));
                }

                using (var listener = NodeActivityLog.AddListener<NodeChangesListener>())
                {
                    nodesToMove.MoveTo(destinationNode);
                    listener.PersistChanges();
                    scope.Done();
                }
            }).ConfigureAwait(false);
		}
	}
}