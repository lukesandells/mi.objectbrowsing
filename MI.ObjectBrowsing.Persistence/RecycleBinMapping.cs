﻿using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	public class RecycleBinMapping : SubclassMapping<RecycleBin>
	{
		public RecycleBinMapping()
		{
			Extends(typeof(BrowserFolder));
			DiscriminatorValue(FolderMetaType.RecycleBin);
		}
	}
}
