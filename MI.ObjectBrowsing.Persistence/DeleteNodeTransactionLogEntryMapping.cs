﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using MI.Framework.ObjectBrowsing.Transactions;
using MI.Persistence.NHibernate;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	public class DeleteNodeTransactionLogEntryMapping : SubclassMapping<DeleteNodeTransactionLogEntry>
	{
		public DeleteNodeTransactionLogEntryMapping()
		{
			Lazy(false);
			Extends(typeof(TransactionLogEntry));
			DiscriminatorValue(TransactionLogEntryType.DeleteNode);

			Property(o => o.DeletedFromFolderMemberId, property => { property.Column("FromMemberId"); property.Access(Accessor.NoSetter); });

			ManyToOne(o => o.DeletedNode, manyToOne =>
			{
				manyToOne.Access(Accessor.NoSetter);
				manyToOne.Cascade(Cascade.None); // No cascade because it is not the log entry's responsibility to manage node persistence
				manyToOne.Column("BrowserNodeId");
				manyToOne.Fetch(FetchKind.Join);
				manyToOne.Lazy(LazyRelation.NoLazy);
			});

			ManyToOne(o => o.DeletedFromParentNode, manyToOne =>
			{
				manyToOne.Access(Accessor.NoSetter);
				manyToOne.Cascade(Cascade.None); // No cascade because it is not the log entry's responsibility to manage node persistence
				manyToOne.Column("FromParentNodeId");
				manyToOne.Fetch(FetchKind.Join);
				manyToOne.Lazy(LazyRelation.NoLazy);
			});

			Index.DeclareIfNotDeclared("IDX_TransactionLogEntry_BrowserNodeId", IndexType.Standard, "TransactionLogEntry", 
				new string[] { "BrowserNodeId" });
			Index.DeclareIfNotDeclared("IDX_TransactionLogEntry_FromParentNodeId", IndexType.Standard, "TransactionLogEntry", 
				new string[] { "FromParentNodeId" });
		}
	}
}
