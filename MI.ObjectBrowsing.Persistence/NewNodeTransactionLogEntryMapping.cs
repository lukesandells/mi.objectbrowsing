﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using MI.Framework.ObjectBrowsing.Transactions;
using MI.Persistence.NHibernate;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	public class NewNodeTransactionLogEntryMapping : SubclassMapping<NewNodeTransactionLogEntry>
	{
		public NewNodeTransactionLogEntryMapping()
		{
			Lazy(false);
			Extends(typeof(TransactionLogEntry));
			DiscriminatorValue(TransactionLogEntryType.NewNode);

			ManyToOne(o => o.NewNode, manyToOne =>
			{
				manyToOne.Access(Accessor.NoSetter);
				manyToOne.Cascade(Cascade.None); // No cascade because it is not the log entry's responsibility to manage node persistence
				manyToOne.Column("BrowserNodeId");
				manyToOne.Fetch(FetchKind.Join);
				manyToOne.Lazy(LazyRelation.NoLazy);
			});

			Index.DeclareIfNotDeclared("IDX_TransactionLogEntry_BrowserNodeId", IndexType.Standard, "TransactionLogEntry", 
				new string[] { "BrowserNodeId" });
		}
	}
}
