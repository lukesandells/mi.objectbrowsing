﻿using System.Linq;
using System.Collections.Generic;
using MI.Core;
using MI.Framework.ObjectBrowsing.Transactions;
using MI.Persistence;
using System.Threading.Tasks;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	public class PersistentTransactionLog : ITransactionLog
	{
		public virtual IQueryable<NodeTransaction> Transactions => _transactions.AsQueryable();
		private IList<NodeTransaction> _transactions = new List<NodeTransaction>();

		private PersistentTransactionLog()
		{
		}

		public static async Task<PersistentTransactionLog> Load()
		{
            PersistentTransactionLog log = null;
            await Async.Using(new PersistenceScope(TransactionOption.Supported), async scope =>
            {
                var nodeList = await scope.Query<NodeTransaction>().OrderBy(transaction => transaction.TransactionNumber).ToListAsync().ConfigureAwait(false);
                log = new PersistentTransactionLog() { _transactions = nodeList };

                scope.Done();
            }).ConfigureAwait(false);

            return log;
		}

		public async Task Add(NodeTransaction transaction)
		{
            await Async.Using(new PersistenceScope(TransactionOption.Required), async scope =>
            {
                await scope.SaveAsync(transaction).ConfigureAwait(false);
                _transactions.Add(transaction);
                scope.Done();
            }).ConfigureAwait(false);
		}

		public async Task ClearFinalised()
		{
            await Async.Using(new PersistenceScope(TransactionOption.Required), async scope =>
            {
                foreach (var transaction in Transactions.Where(transaction => transaction.IsFinalised).ToList())
                {
                    await scope.DeleteAsync(transaction).ConfigureAwait(false);
                    _transactions.Remove(transaction);
                }

                scope.Done();
            }).ConfigureAwait(false);
		}
	}
}
