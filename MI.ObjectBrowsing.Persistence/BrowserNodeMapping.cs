﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using MI.Persistence.NHibernate;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	public class BrowserNodeMapping : ClassMapping<BrowserNode>
	{
		public BrowserNodeMapping()
		{
			Id(o => o.Id, id => id.Access(Accessor.NoSetter));

			Discriminator(discriminator => 
			{
				discriminator.Column("MetaTypeId");
				discriminator.Type<GuidType>();
				discriminator.NotNullable(true);
			});

			DiscriminatorValue(null);

			Property(o => o.TypeId, property => { property.NotNullable(true); property.Access(Accessor.NoSetter); });
			Property(o => o.IsDefinitionNode, property => { property.NotNullable(true); property.Access(Accessor.ReadOnly); });
			Property(o => o.IsLinkNode, property => { property.NotNullable(true); property.Access(Accessor.ReadOnly); });
			Property(o => o.IsRootNode, property => { property.NotNullable(true); property.Access(Accessor.ReadOnly); });
			Property(o => o.IsDeleted, property => { property.NotNullable(true); property.Access(Accessor.NoSetter); });
			Property(o => o.DisplayId, property => { property.NotNullable(true); property.Access(Accessor.NoSetter); });
			Property(o => o.Description, property => { property.NotNullable(true); property.Access(Accessor.NoSetter); });
			Property(o => o.Depth, property => { property.NotNullable(true); property.Access(Accessor.NoSetter); });

			ManyToOne(o => o.RootNode, manyToOne =>
			{
				manyToOne.Column("RootNodeId");
				manyToOne.Access(Accessor.Property);
				manyToOne.Fetch(FetchKind.Select);
				manyToOne.Cascade(Cascade.None);
			});

			ManyToOne(o => o.ParentFolder, manyToOne =>
			{
				manyToOne.Column("ParentFolderId");
				manyToOne.Access(Accessor.NoSetter); // NoSetter because the setter refreshes the parent node and ascendants
				// Nodes are the aggregate root and child folders are part of the aggregate root, not the parent folder.
				manyToOne.Fetch(FetchKind.Select);
				manyToOne.Cascade(Cascade.None);
			});

			ManyToOne(o => o.ParentNode, manyToOne =>
			{
				manyToOne.Column("ParentNodeId");
				// Read only because the BrowserNode class derives/calculates the parent node property from the Parent property of its ParentFolder
				manyToOne.Access(Accessor.ReadOnly);
				manyToOne.Fetch(FetchKind.Select);
				manyToOne.Cascade(Cascade.None);
			});

			// Defined as a bag so that the collection isn't initialised upon adding to the collection
			Bag(o => o.Ascendants, bag => {
				bag.Table("BrowserNodeDescendant");
				bag.Key(k => k.Column("DescendantId"));
				bag.Fetch(CollectionFetchMode.Select);
				bag.Lazy(CollectionLazy.Extra);
				bag.Cascade(Cascade.None);
				bag.Access(Accessor.Field); // Must be field access because the getter returns a wrapper that sorts by depth descending
			}, r => r.ManyToMany(manyToMany => manyToMany.Column("AscendantId")));

			Set(o => o.ValidationResults, set =>
			{
				set.Table("BrowserNodeNodeValidationResult");
				set.Key(k => k.Column("NodeValidationResultId"));
				set.Fetch(CollectionFetchMode.Select);
				set.Cascade(Cascade.All);
				set.Access(Accessor.NoSetter);
			}, r => r.ManyToMany(manyToMany => manyToMany.Column("BrowserNodeId")));

			Index.Declare("IDX_BrowserNode_ParentFolderId", IndexType.Standard, nameof(BrowserNode), new string[] { "ParentFolderId" });
			Index.Declare("IDX_BrowserNode_DisplayId_MetaTypeId_TypeId", IndexType.Standard, nameof(BrowserNode), 
				new string[] { "DisplayId", "MetaTypeId", "TypeId" });
			Index.Declare("IDX_BrowserNode_RootNodeId", IndexType.Standard, nameof(BrowserNode), new string[] { "RootNodeId" });
			Index.Declare("IDX_BrowserNode_ParentNodeId", IndexType.Standard, nameof(BrowserNode), new string[] { "ParentNodeId" });
			Index.Declare("IDX_BrowserNodeDescendant_DescendantId_AscendantId", IndexType.Unique, "BrowserNodeDescendant",
				new[] { "DescendantId", "AscendantId" });
			Index.Declare("IDX_BrowserNodeNodeValidationResult_BrowserNodeId_NodeValidationResultId", IndexType.Unique, "BrowserNodeNodeValidationResult",
				new[] { "BrowserNodeId", "NodeValidationResultId" });
		}
	}
}
