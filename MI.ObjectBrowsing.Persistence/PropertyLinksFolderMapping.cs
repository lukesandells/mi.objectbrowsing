﻿using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	public class PropertyLinksFolderMapping : SubclassMapping<PropertyLinksFolder>
	{
		public PropertyLinksFolderMapping()
		{
			Extends(typeof(BrowserFolder));
			DiscriminatorValue(FolderMetaType.PropertyLinksFolder);
		}
	}
}
