﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

using MI.Persistence.NHibernate;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	public class LinkNodeMapping : SubclassMapping<LinkNode>
	{
		public LinkNodeMapping()
		{
			Extends(typeof(BrowserNode));
			DiscriminatorValue(NodeMetaType.LinkNode);

			Property(o => o.IsPropertyLink, property => property.Access(Accessor.ReadOnly));
			Property(o => o.FromMemberId, property => property.Access(Accessor.NoSetter));
			ManyToOne(o => o.LinkedDefinitionNode, manyToOne =>
			{
				manyToOne.Column("LinkedDefinitionNodeId");
				manyToOne.Access(Accessor.NoSetter);
				// Reattach cascade to be symetrical with the fetch
				manyToOne.Fetch(FetchKind.Select);
				manyToOne.Cascade(Cascade.ReAttach);
			});

			Index.Declare("IDX_BrowserNode_LinkedDefinitionNodeId", IndexType.Standard, nameof(BrowserNode), new string[] { "LinkedDefinitionNodeId" });
		}
	}
}
