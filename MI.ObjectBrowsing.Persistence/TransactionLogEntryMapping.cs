﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Type;
using MI.Framework.ObjectBrowsing.Transactions;
using MI.Persistence.NHibernate;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	public class TransactionLogEntryMapping : ClassMapping<TransactionLogEntry>
	{
		public TransactionLogEntryMapping()
		{
			Abstract(true);
			Mutable(false);
			Lazy(false);
			Id(o => o.Id, id =>
			{
				id.Access(Accessor.Property);
				id.Generator(Generators.HighLow, g => g.Params(new { max_lo = 1000 }));
			});

			Discriminator(discriminator =>
			{
				discriminator.Column("TransactionLogEntryTypeId");
				discriminator.Type<EnumType<TransactionLogEntryType>>();
				discriminator.NotNullable(true);
			});

			DiscriminatorValue(null);

			Property(o => o.LogEntryNumber, property => { property.NotNullable(true); property.Access(Accessor.NoSetter); });

			ManyToOne(o => o.Transaction, manyToOne =>
			{
				manyToOne.Access(Accessor.NoSetter);
				manyToOne.Cascade(Cascade.None);
				manyToOne.Column("NodeTransactionId");
				manyToOne.Fetch(FetchKind.Select); // Won't ever query a transaction log entry in isolation from a node transaction
				manyToOne.NotNullable(true);
			});

			Index.Declare("IDX_TransactionLogEntry_NodeTransactionId_LogEntryNumber", IndexType.Standard, "TransactionLogEntry",
				new string[] { "NodeTransactionId", "LogEntryNumber" });
		}
	}
}
