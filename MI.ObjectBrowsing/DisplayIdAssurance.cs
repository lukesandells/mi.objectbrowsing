﻿using MI.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MI.Framework.ObjectBrowsing
{
	/// <summary>
	/// Facility for assuring available display IDs at a destination of a move or copy operation.
	/// </summary>
	internal static class DisplayIdAssurance
	{
		/// <summary>
		/// Assures the destination IDs at a destination folder for a move or copy operation.
		/// </summary>
		/// <param name="sourceNodes">The source nodes to be moved or copied.</param>
		/// <param name="destinationPath">The destination path to receive the source nodes.</param>
		/// <param name="excludeSourceNodes">Stipulates whether to exclude the IDs of the source nodes in the determination of
		/// ID availability at the destination path.</param>
		/// <param name="idClashSuffixFunc">Function to provide the ID clash suffix to append to IDs until an available ID is found
		/// for a give node type.</param>
		/// <returns>A map of nodes to their assured destination IDs.</returns>
		internal static IReadOnlyDictionary<DefinitionNode, string> AssureDestinationIds(IEnumerable<DefinitionNode> sourceNodes,
			FolderPath destinationPath, bool excludeSourceNodes, Func<NodeType, string> idClashSuffixFunc)
		{
			var stopwatch = new System.Diagnostics.Stopwatch();
			stopwatch.Start();

			var assuredAvailableDestinationIds = AssuredAvailableDestinationIds(sourceNodes, destinationPath, new Dictionary<NodeType, ISet<string>>());

			stopwatch.Stop();
			Console.WriteLine("Assuring destination IDs took {0} seconds", stopwatch.Elapsed.TotalSeconds);
			return assuredAvailableDestinationIds;

			IReadOnlyDictionary<DefinitionNode, string> AssuredAvailableDestinationIds(IEnumerable<DefinitionNode> recursingSourceNodes,
				FolderPath recursingDestinationPath, IDictionary<NodeType, ISet<string>> globallyUniqueIdCache)
			{
				var assuredAvailableDesinationIds = new Dictionary<DefinitionNode, string>();

				// Get the distinct set of source folders
				var sourceFolders = recursingSourceNodes.ToLookup(node => node.ParentFolder);
				foreach (var sourceFolder in sourceFolders)
				{
					// Get the unavailable IDs, caching any that are globally unique
					IDictionary<NodeType, ISet<string>> unavailableIdsByNodeType = new Dictionary<NodeType, ISet<string>>();
					foreach (var nodeType in sourceFolder.Key.Nodes.Select(node => node.Type).Distinct())
					{
						if (nodeType.Configuration.DisplayId.IsGloballyUnique)
						{
							if (globallyUniqueIdCache.TryGetValue(nodeType, out var unavailableIds))
							{
								unavailableIdsByNodeType[nodeType] = unavailableIds;
							}
							else
							{
                                // If excluding source nodes, all IDs found within the source nodes will be
                                // available at the destination because the IDs are globally unique
                                unavailableIdsByNodeType[nodeType] = excludeSourceNodes ? new HashSet<string>()
                                    : MI.Core.EnumerableExtensions.ToHashSet(recursingDestinationPath.UnavailableIds(nodeType));//.ToHashSet();
								globallyUniqueIdCache[nodeType] = unavailableIdsByNodeType[nodeType];
							}
						}
						else
						{
							// Find unavailable IDs, excluding the source nodes for this source folder if requested
							var exclusions = excludeSourceNodes ? sourceFolder.Select(node => node.TargetObject) : new object[] { };
                            unavailableIdsByNodeType[nodeType] = MI.Core.EnumerableExtensions.ToHashSet(recursingDestinationPath.UnavailableIds(nodeType, exclusions));//.ToHashSet();
						}
					}

					foreach (var sourceNode in sourceFolder)
					{
						// Get the available ID at the destination, noting that if the node is of a type that has globally unique identifers,
						// its existing ID is guaranteed to be available at the destination
						assuredAvailableDesinationIds[sourceNode] = FolderPath.AssuredAvailableId(
							sourceNode.DisplayId, idClashSuffixFunc(sourceNode.Type), unavailableIdsByNodeType[sourceNode.Type]);

						// Repeat for all child nodes
						foreach (var folder in sourceNode.NonEmptyNonSystemFolders.Where(f => f.AllowsDefinitionNodes))
						{
							var relativePath = FolderPath.To(folder).RelativeTo(FolderPath.To(sourceFolder.Key));
							relativePath.Last().ParentNodeDisplayId = assuredAvailableDesinationIds[sourceNode];
							assuredAvailableDesinationIds.Add(AssuredAvailableDestinationIds(folder.Nodes.OfType<DefinitionNode>(),
								FolderPath.Combine(recursingDestinationPath, relativePath), globallyUniqueIdCache));
						}
					}
				}

				return assuredAvailableDesinationIds;
			}
		}
	}
}
