﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using MI.Core;
using MI.Core.Validation;
using MI.Framework.ObjectBrowsing.Configuration;
using MI.Framework.ObjectBrowsing.Mapping;

namespace MI.Framework.ObjectBrowsing
{
	public class NodeType
	{
		private class StandardNodeTypeResolver : INodeTypeResolver
		{
			/// <summary>
			/// Gets the node type with the given node type ID.
			/// </summary>
			/// <param name="nodeTypeId">The node type ID.</param>
			/// <returns>The node type with the given node type ID or <c>null</c> if not found.</returns>
			public NodeType NodeTypeWithId(Guid nodeTypeId)
			{
				if (_registeredTypes.TryGetValue(nodeTypeId, out var nodeType))
				{
					return nodeType;
				}

				return null;
			}

			/// <summary>
			/// Gets the node type for the given the given target object type.
			/// </summary>
			/// <param name="targetObjectType">The type of target object.</param>
			/// <returns>The node type with the given target object type or <c>null</c> if not found.</returns>
			public NodeType NodeTypeFor(Type targetObjectType)
			{
				NodeType nodeType;
				while (!_registeredTypesByTargetObjectType.TryGetValue(targetObjectType, out nodeType))
				{
					if (targetObjectType.BaseType == null)
					{
						return null;
					}
					targetObjectType = targetObjectType.BaseType;
				}
				return nodeType;
			}
		}

		public static INodeTypeResolver Resolver { get; set; } = new StandardNodeTypeResolver();
		
		public static IEnumerable<NodeType> RegisteredTypes => _registeredTypes.Values;
		private static IDictionary<Guid, NodeType> _registeredTypes = new Dictionary<Guid, NodeType>();
		private static IDictionary<Type, NodeType> _registeredTypesByTargetObjectType = new Dictionary<Type, NodeType>();

		public Guid Id => _id;
		private Guid _id;

		public Type TargetObjectType => _configuration.TargetObjectType;
		public string DisplayName => _configuration.DisplayName;

		public NodeTypeConfiguration Configuration => _configuration;
		private NodeTypeConfiguration _configuration;

		internal NodeType(NodeTypeConfiguration configuration)
		{
			_configuration = ArgumentValidation.AssertNotNull(configuration, nameof(configuration));
			_id = _configuration.NodeTypeId;
		}

		public static void AddMappingAssembly(Assembly assembly)
		{
			assembly = ArgumentValidation.AssertNotNull(assembly, nameof(assembly));
			var nodeMappings = new HashSet<INodeMapping>();
			foreach (var type in assembly.GetTypes().Where(type => type.HasInterface(typeof(INodeMapping)) && !type.IsAbstract))
			{
				if (type.HasInterface(typeof(INodeMapping)))
				{
					nodeMappings.Add((INodeMapping)Activator.CreateInstance(type));
				}
			}

			Register(NodeMappingCompiler.Compile(nodeMappings));
		}

		public static void ClearAllMappings()
		{
			_registeredTypes.Clear();
			_registeredTypesByTargetObjectType.Clear();
		}

		public static void Register(NodeType nodeType)
		{
			nodeType = ArgumentValidation.AssertNotNull(nodeType, nameof(nodeType));
			if (_registeredTypes.ContainsKey(nodeType.Id))
			{
				// A type has already been registered with the given ID.
				throw new ArgumentException(string.Format(ExceptionMessage.NodeTypeAlreadyRegistered, nodeType.Id), nameof(nodeType));
			}

			_registeredTypes.Add(nodeType.Id, nodeType);
			_registeredTypesByTargetObjectType.Add(nodeType.TargetObjectType, nodeType);
		}

		public static void Register(IEnumerable<NodeType> nodeTypes)
		{
			nodeTypes = ArgumentValidation.AssertNotNull(nodeTypes, nameof(nodeTypes));
			nodeTypes.ForEach(Register);
		}

		public static void Deregister(NodeType nodeType)
		{
			nodeType = ArgumentValidation.AssertNotNull(nodeType, nameof(nodeType));
			_registeredTypes.Remove(nodeType.Id);
			_registeredTypesByTargetObjectType.Remove(nodeType.TargetObjectType);
		}

		public DefinitionNode NewDefinitionNode()
		{
			return NewDefinitionNode(null);
		}

		public DefinitionNode NewDefinitionNode(object[] parameters)
		{
			return NewDefinitionNode(null, parameters);
		}

		/// <summary>
		/// Creates a new browser node of this node type.
		/// </summary>
		/// <returns>A new browser node of this node type.</returns>
		public DefinitionNode NewDefinitionNode(string displayId, object[] parameters = null)
		{
			// Create the definition node of the configured type
			var newNode = (DefinitionNode)Activator.CreateInstance(Configuration.NodeType, true);
			newNode.SetTargetObject(Configuration.IsSelfReferencing ? newNode : Activator.CreateInstance(TargetObjectType, true));
			NodeActivityLog.LogNodeCreated(newNode);
			newNode.SetPropertyValue(Configuration.DisplayId.Property.MemberId, displayId, refreshLinkNodes: false, evaluatePropertyForumula: false);

			// Check whether the right number of parameters were passed
			if ((parameters?.Length ?? 0) != Configuration.Initialiser.Properties.Count())
			{
				// Different number of parameters passed than configured for the initialiser.
				throw new ArgumentException(string.Format(ExceptionMessage.DifferentNumberOfInitialisationParametersPassedThanConfigured, DisplayName),
					nameof(parameters));
			}

			// Initialise with the given parameters
			var parameterIndex = 0;
			foreach (PropertyConfiguration property in Configuration.Initialiser.Properties)
			{
				newNode.SetPropertyValue(property.MemberId, parameters[parameterIndex], refreshLinkNodes: true, evaluatePropertyForumula: false);
				parameterIndex++;
			}

			return newNode;
		}

		public override string ToString()
		{
			return DisplayName;
		}

		/// <summary>
		/// Gets the node type with the given node type ID.
		/// </summary>
		/// <param name="nodeTypeId">The node type ID.</param>
		/// <returns>The node type with the given node type ID.</returns>
		public static NodeType WithId(Guid nodeTypeId)
		{
			var nodeType = Resolver.NodeTypeWithId(nodeTypeId);
			if (nodeType == null)
			{
				// No node type is configured with the given node type ID
				throw new ArgumentException(string.Format(ExceptionMessage.NoNodeTypeConfiguredWithId, nodeTypeId), nameof(nodeTypeId));
			}

			return nodeType;
		}

		public static bool IsConfiguredFor(Type targetObjectType)
		{
			return Resolver.NodeTypeFor(targetObjectType) != null;
		}

		public static NodeType For(Type targetObjectType)
		{
			var nodeType = Resolver.NodeTypeFor(targetObjectType);
			if (nodeType == null)
			{
				// No node type is configured for the given object
				targetObjectType = ArgumentValidation.AssertNotNull(targetObjectType, nameof(targetObjectType));
				throw new ArgumentException(string.Format(ExceptionMessage.NoNodeTypeConfiguredForTargetObjectType, targetObjectType.FullName),
					nameof(targetObjectType));
			}

			return nodeType;
		}

		
	}
}
