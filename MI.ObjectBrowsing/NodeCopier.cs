﻿using MI.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using MI.Core.Validation;

namespace MI.Framework.ObjectBrowsing
{
	/// <summary>
	/// Copies one or more nodes to a destination node or folder.
	/// </summary>
	internal class NodeCopier
	{
		private IDictionary<BrowserNode, BrowserNode> _originalToCopyMap;
		private IDictionary<BrowserNode, IDictionary<Guid, BrowserFolder>> _copiedFolders;

		internal BrowserNode Copy(BrowserNode sourceNode, IValidated<BrowserFolder> folder)
		{
			return Copy(new[] { sourceNode }, folder).Single().Value;
		}

		internal IReadOnlyDictionary<BrowserNode, BrowserNode> Copy(IEnumerable<BrowserNode> sourceNodes, IValidated<BrowserFolder> folder)
		{
			if (sourceNodes.Any(node => node.IsDeleted))
			{
				// Can't copy deleted nodes
				throw new InvalidOperationException(ExceptionMessage.CannotCopyDeletedNodes);
			}

			_originalToCopyMap = new Dictionary<BrowserNode, BrowserNode>();
			_copiedFolders = new Dictionary<BrowserNode, IDictionary<Guid, BrowserFolder>>();

			// Get the assured available destination IDs for all the definition nodes nodes to be copied
			var assuredAvailableDestinationIds = DisplayIdAssurance.AssureDestinationIds(sourceNodes.OfType<DefinitionNode>(),
				FolderPath.To(folder.Value), false, nodeType => nodeType.Configuration.DisplayId.IdClashOnCopySuffix);

			// Execute the copy.  Definition nodes are copied before link nodes because the link nodes that reference the copied
			// definition nodes need to be created at the destination by adding the copies of their linked definition nodes to 
			// the destination.  This requires the definition nodes to all be at the destination before all the link nodes can be
			// created because the first link node to be copied may reference the last definition node to be copied.
			CopyDefinitionNodes(sourceNodes.OfType<DefinitionNode>(), folder.Value);

			// Copy the link nodes.  Don't copy property links because they will be automatically created at the destination when the 
			// property values are copied.
			CopyLinkNodes(sourceNodes.SelectMany(node => node.DescendantsOrderedByDepth.OfType<LinkNode>()));

			// Replace IDs of copied nodes with their assured available IDs
			_originalToCopyMap.Keys.OfType<DefinitionNode>()
				.ForEach(original => _originalToCopyMap[original].Cast<DefinitionNode>()
					.SetPropertyValue(original.Type.Configuration.DisplayId.Property.MemberId, assuredAvailableDestinationIds[original],
						refreshLinkNodes: false, evaluatePropertyForumula: false));

			// Invoke OnCopy action for all copied nodes
			_originalToCopyMap.Keys.OfType<DefinitionNode>()
				.ForEach(original => original.Type.Configuration.OnCopyAction?.Invoke(original, _originalToCopyMap[original]));

			return _originalToCopyMap.Where(kvp => sourceNodes.Contains(kvp.Key)).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
		}

		private void CopyDefinitionNodes(IEnumerable<DefinitionNode> sourceNodes, BrowserFolder folder)
		{
			// Copy all the definition nodes
			sourceNodes.Where(node => !_originalToCopyMap.Keys.Contains(node))
				.ForEach(original => _originalToCopyMap.Add(original, CopyDefinitionNode(original, folder)));
		}

		private BrowserNode CopyDefinitionNode(DefinitionNode node, BrowserFolder folder)
		{
			// Create the initialisation parameters
			var parameters = new List<object>();
			foreach (var parameter in node.Type.Configuration.Initialiser.Properties)
			{
				parameters.Add(node.Properties[parameter.MemberId].Value);
			}

			// Create the new node and add it to the destination folder
			var copy = folder.Parent.AddNew(node.Type, folder, Validatable.Validated(node.DisplayId), parameters.ToArray());

			// Copy all the properties, property links are done seperately
			foreach (var pc in node.Type.Configuration.Properties.Values.Where(p => !p.IsReadOnly && !p.IsNodeType))
			{
				// Exclude the display ID because that's handled elsewhere
				if (pc.MemberId != node.Type.Configuration.DisplayId.Property.MemberId)
				{
					copy.SetPropertyValue(pc.MemberId, node.GetPropertyValue(pc.MemberId), refreshLinkNodes: true, evaluatePropertyForumula: false);
				}
			}

			// Copy all the child nodes/folders 
			node.NonEmptyNonInverseFolders.ForEach(sourceFolder => CopyDefinitionNodes(sourceFolder.Nodes.OfType<DefinitionNode>()
				.Where(n => !_originalToCopyMap.Values.Contains(n)), LookUpCopiedBrowserFolder(copy, sourceFolder.MemberId)));

			return copy;
		}

		private void CopyLinkNodes(IEnumerable<LinkNode> sourceNodes)
		{
			// Copy all the link nodes that aren't linked to copied definition nodes
			sourceNodes.Where(node => !_originalToCopyMap.Keys.Contains(node))
				.Where(node => !_originalToCopyMap.ContainsKey(node.LinkedDefinitionNode))
				.OfType<LinkNode>()
				.ForEach(original =>
				{
					if (original.IsPropertyLink && !original.CorrespondingPropertyConfiguration.IsReadOnly)
					{
						_originalToCopyMap[original.FromDefinitionNode].Cast<DefinitionNode>().Properties[original.FromMemberId].Value = original.LinkedDefinitionNode;
					} else if (!original.IsPropertyLink)
					{
						_originalToCopyMap.Add(original, original.LinkedDefinitionNode
							.AddTo(Validatable.Validated(LookUpCopiedBrowserFolder(_originalToCopyMap[original.FromDefinitionNode].Cast<DefinitionNode>(),
								original.ParentFolder.MemberId))));
					}
				});

			// Add the remaining link nodes to their parent copies
			sourceNodes.Where(node => !_originalToCopyMap.Keys.Contains(node))
				.Where(node => _originalToCopyMap.ContainsKey(node.LinkedDefinitionNode))
				.OfType<LinkNode>()
				.ForEach(original =>
				{
					if (original.IsPropertyLink && !original.CorrespondingPropertyConfiguration.IsReadOnly)
					{
						((DefinitionNode) _originalToCopyMap[original.FromDefinitionNode]).Properties[original.FromMemberId].Value = _originalToCopyMap[original.LinkedDefinitionNode];
					}
					else if (!original.IsPropertyLink)
					{
						_originalToCopyMap.Add(original, _originalToCopyMap[original.LinkedDefinitionNode]
							.AddTo(Validatable.Validated(LookUpCopiedBrowserFolder(_originalToCopyMap[original.FromDefinitionNode].Cast<DefinitionNode>(),
								original.ParentFolder.MemberId))));
					}
				});
		}

		private BrowserFolder LookUpCopiedBrowserFolder(ParentNode copy, Guid folderMemberId)
		{
			if (!_copiedFolders.TryGetValue(copy, out var folderLookup))
			{
				_copiedFolders.Add(copy, folderLookup = copy.AllFolders.ToDictionary(f => f.MemberId));
			}

			return folderLookup[folderMemberId];
		}
	}
}
