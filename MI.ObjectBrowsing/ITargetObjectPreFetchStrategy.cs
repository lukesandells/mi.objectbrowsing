﻿namespace MI.Framework.ObjectBrowsing
{
	/// <summary>
	/// Strategy for pre-fetching all target objects of all descendant nodes of a node.
	/// </summary>
	public interface ITargetObjectPreFetchStrategy
	{
		/// <summary>
		/// Pre-fetches the target objects of all descendant nodes of the given node.
		/// </summary>
		/// <param name="node">The node for which the target of objects of its descendants shall be pre-fetched.</param>
		/// <param name="operationType">The type of node operation for which to pre-fetch.</param>
		void PreFetchTargetObjects(ParentNode node, OperationType operationType);
	}
}
