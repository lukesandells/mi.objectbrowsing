﻿using System;
using MI.Core.Validation;

namespace MI.Framework.ObjectBrowsing
{
	/// <summary>
	/// A node meta-type supporting flexible node type evaluation.
	/// </summary>
	public class NodeMetaType
	{
		/// <summary>
		/// Stipulates whether a link node is type-compatible.
		/// </summary>
		private readonly bool _linkNode;

		/// <summary>
		/// Indicates whether a definition node is type-compatible.
		/// </summary>
		private readonly bool _definitionNode;

		/// <summary>
		/// The type of definition node.
		/// </summary>
		private readonly Type _definitionNodeType;

		/// <summary>
		/// The type of the node's target object.
		/// </summary>
		private readonly Type _targetObjectType;

		/// <summary>
		/// Initialises a new instance of the <see cref="NodeMetaType"/> class.
		/// </summary>
		/// <param name="definitionNodeType">The type of definition node for the new meta-type.</param>
		/// <param name="targetObjectType">The type of the node target object for the new meta-type.</param>
		/// <param name="linkNode">Stipulates whether a link node is type-compatible for the new meta-type.</param>
		/// <param name="definitionNode">Indicates whether a definition node is type-compatible for the new meta-type.</param>
		internal NodeMetaType(Type definitionNodeType, Type targetObjectType, bool linkNode, bool definitionNode)
		{
			_definitionNodeType = definitionNodeType;
			_targetObjectType = targetObjectType;
			_linkNode = linkNode;
			_definitionNode = definitionNode;
		}

		/// <summary>
		/// Returns a link node meta-type.
		/// </summary>
		/// <returns>A link node meta-type.</returns>
		public static NodeMetaType LinkNode()
		{
			return new NodeMetaType(typeof(DefinitionNode), typeof(object), true, false);
		}

		/// <summary>
		/// Returns a meta-type for a link node to a given type of definition node.
		/// </summary>
		/// <typeparam name="TDefinitionNode">The type of definition node.</typeparam>
		/// <returns>A meta-type for a link node to the given type of definition node.</returns>
		public static NodeMetaType LinkNodeTo<TDefinitionNode>() 
			where TDefinitionNode : DefinitionNode
		{
			return new NodeMetaType(typeof(TDefinitionNode), typeof(object), true, false);
		}

		/// <summary>
		/// Returns a meta-type for a link node to a given type of definition node with a given type of target object.
		/// </summary>
		/// <typeparam name="TDefinitionNode">The type of definition node.</typeparam>
		/// <typeparam name="TTargetObject">The type of target object.</typeparam>
		/// <returns>A meta-type for a link node to a given type of definition node with the given type of target object.</returns>
		public static NodeMetaType LinkNodeTo<TDefinitionNode, TTargetObject>() 
			where TDefinitionNode : DefinitionNode
			where TTargetObject : class
		{
			return new NodeMetaType(typeof(TDefinitionNode), typeof(TTargetObject), true, false);
		}

		/// <summary>
		/// Returns a meta-type for a definition node.
		/// </summary>
		/// <returns>A meta-type for a definition node.</returns>
		public static NodeMetaType DefinitionNode()
		{
			return new NodeMetaType(typeof(DefinitionNode), typeof(object), false, true);
		}

		/// <summary>
		/// Returns a meta-type for a definition node of a given type.
		/// </summary>
		/// <typeparam name="TDefinitionNode">The type of definition node.</typeparam>
		/// <returns>A meta-type for a definition node of the given type.</returns>
		public static NodeMetaType DefinitionNode<TDefinitionNode>() 
			where TDefinitionNode : DefinitionNode
		{
			return new NodeMetaType(typeof(TDefinitionNode), typeof(object), false, true);
		}

		/// <summary>
		/// Returns a meta-type for a definition node of a given type with a given type of target object.
		/// </summary>
		/// <typeparam name="TDefinitionNode">The type of definition node.</typeparam>
		/// <typeparam name="TTargetObject">The type of target object.</typeparam>
		/// <returns>A meta-type for a definition node of a given type with the given type of target object.</returns>
		public static NodeMetaType DefinitionNode<TDefinitionNode, TTargetObject>()
			where TDefinitionNode : DefinitionNode
			where TTargetObject : class
		{
			return new NodeMetaType(typeof(TDefinitionNode), typeof(TTargetObject), false, true);
		}

		/// <summary>
		/// Returns a meta-type for any type of node.
		/// </summary>
		/// <returns>A meta-type for any type of node.</returns>
		public static NodeMetaType Any()
		{
			return new NodeMetaType(typeof(DefinitionNode), typeof(object), true, true);
		}

		/// <summary>
		/// Returns a meta-type for any type of node (link node or definition node) corresponding to a given type of definition node.
		/// </summary>
		/// <typeparam name="TDefinitionNode">The type of definition node.</typeparam>
		/// <returns>A meta-type for any type of node (link node or definition node) corresponding to the given type of definition node.</returns>
		public static NodeMetaType Any<TDefinitionNode>() 
			where TDefinitionNode : DefinitionNode
		{
			return new NodeMetaType(typeof(TDefinitionNode), typeof(object), true, true);
		}

		/// <summary>
		/// Returns a meta-type for any type of node (link node or definition node) corresponding to a given type of definition node with a given 
		/// type of target object.
		/// </summary>
		/// <typeparam name="TDefinitionNode">The type of definition node.</typeparam>
		/// <returns>A meta-type for any type of node (link node or definition node) corresponding to the given type of definition node with 
		/// the given type of target object.</returns>
		public static NodeMetaType Any<TDefinitionNode, TTargetObject>()
			where TDefinitionNode : DefinitionNode
			where TTargetObject : class
		{
			return new NodeMetaType(typeof(TDefinitionNode), typeof(TTargetObject), true, true);
		}

		/// <summary>
		/// Determines whether the meta-type is type-compatible with a given meta-type.
		/// </summary>
		/// <param name="metaType">The meta-type with which to compare.</param>
		/// <returns><c>true</c> if the meta-type is deemed to be compatible with the given meta-type.</returns>
		public bool IsAssignableFrom(NodeMetaType metaType)
		{
			metaType = ArgumentValidation.AssertNotNull(metaType, nameof(metaType));
			return _definitionNodeType.IsAssignableFrom(metaType._definitionNodeType)
				&& _targetObjectType.IsAssignableFrom(metaType._targetObjectType)
				&& (_linkNode && metaType._linkNode || _definitionNode && metaType._definitionNode);
		}

		/// <summary>
		/// Determines whether the meta-type is equal to the given object.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns><c>true</c> if the meta-type is equal to the given object; otherwise <c>false</c>.</returns>
		public override bool Equals(object obj)
		{
			// If parameter is null return false.
			if (obj == null)
			{
				return false;
			}

			// If parameter cannot be cast to NodeMetaType return false.
			NodeMetaType metaType = obj as NodeMetaType;
			if ((object)metaType == null)
			{
				return false;
			}

			// Return true if the fields match:
			return metaType._definitionNodeType == _definitionNodeType
				&& metaType._targetObjectType == _targetObjectType
				&& metaType._linkNode == _linkNode 
				&& metaType._definitionNode == _definitionNode;
		}

		/// <summary>
		/// Gets the hash code for the meta-type.
		/// </summary>
		/// <returns>The hash code for the meta-type.</returns>
		public override int GetHashCode()
		{
			return (_definitionNodeType, _targetObjectType, _linkNode, _definitionNode).GetHashCode();
		}

		/// <summary>
		/// Determines whether the given two meta-types are equal.
		/// </summary>
		/// <param name="left">The left-hand operand.</param>
		/// <param name="right">The right-hand operatnd.</param>
		/// <returns><c>true</c> if the given node meta-types are equal; otherwise <c>false</c>.</returns>
		public static bool operator ==(NodeMetaType left, NodeMetaType right)
		{
			// If both are null, or both are same instance, return true.
			if (ReferenceEquals(left, right))
			{
				return true;
			}

			// If one is null, but not both, return false.
			if ((object)left == null || (object)right == null)
			{
				return false;
			}

			// Return true if the fields match:
			return left._definitionNodeType == right._definitionNodeType 
				&& left._targetObjectType == right._targetObjectType
				&& left._linkNode == right._linkNode
				&& left._definitionNode == right._definitionNode;
		}

		/// <summary>
		/// Determines whether the given two meta-types are not equal.
		/// </summary>
		/// <param name="left">The left-hand operand.</param>
		/// <param name="right">The right-hand operatnd.</param>
		/// <returns><c>true</c> if the given node meta-types are not equal; otherwise <c>false</c>.</returns>
		public static bool operator !=(NodeMetaType left, NodeMetaType right)
		{
			return !(left == right);
		}
	}
}
