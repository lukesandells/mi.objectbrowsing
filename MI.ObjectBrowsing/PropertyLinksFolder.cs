﻿using System;
using System.Collections.Generic;
using System.Linq;

using MI.Framework.ObjectBrowsing.Configuration;

namespace MI.Framework.ObjectBrowsing
{
	public class PropertyLinksFolder : BrowserFolder
	{
		/// <summary>
		/// Gets the link nodes within the property links folder.
		/// </summary>
		/// <remarks>
		/// Nodes within a property links folder are all link nodes.  This property replaces the <see cref="BrowserFolder.Nodes"/> property,
		/// providing the same nodes, but cast to the <see cref="LinkNode"/> class.
		/// </remarks>
		public new virtual IEnumerable<LinkNode> Nodes => base.Nodes.Cast<LinkNode>();

		/// <summary>
		/// Gets the definition node to which the property links folder belongs.
		/// </summary>
		/// <remarks>
		/// A property links folder can only belong to a definition node.  This property replaces the <see cref="BrowserFolder.Parent"/> property
		/// with one that is specifically typed as a <see cref="DefinitionNode"/>.
		/// </remarks>
		public new virtual DefinitionNode Parent => base.Parent.Cast<DefinitionNode>();

		/// <summary>
		/// Initialises a new instance of the <see cref="PropertyLinksFolder"/> class.
		/// </summary>
		protected PropertyLinksFolder()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="PropertyLinksFolder"/> class.
		/// </summary>
		/// <param name="parent">The parent node containing the new property links folder.</param>
		internal PropertyLinksFolder(DefinitionNode parent)
			: base(FolderConfiguration.PropertyLinksFolder.MemberId, parent)
		{
		}
	}
}
