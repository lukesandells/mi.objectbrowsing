﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework.ObjectBrowsing.Configuration;
using MI.Core;
using MI.Core.Validation;

namespace MI.Framework.ObjectBrowsing
{
	/// <summary>
	/// A property of a node.
	/// </summary>
	public class NodeProperty
	{
		public IEnumerable<DefinitionNode> ParentNodes => _parentNodes;
		private IEnumerable<DefinitionNode> _parentNodes;

		public PropertyConfiguration Configuration => _configuration;
		private PropertyConfiguration _configuration;

		public object Value
		{
			get
			{
				var distinctValues = _parentNodes.Select(node => node.GetPropertyValue(Configuration.MemberId)).Distinct();
				if (distinctValues.Count() <= 1)
				{
					return distinctValues.SingleOrDefault();
				}
				else
				{
					return null;
				}
			}
			set
			{
				if (Configuration.IsReadOnly)
				{
					// Cannot update read-only property
					throw new InvalidOperationException(string.Format(ExceptionMessage.CannotUpdateReadOnlyProperty, Configuration.DisplayName));
				}

				_parentNodes.ForEach(node => node.SetPropertyValue(Configuration.MemberId, value, refreshLinkNodes: true, evaluatePropertyForumula: true));
			}
		}

		public LinkNode CorrespondingLinkNode => ParentNodes.SingleOrDefault().PropertyLinksFolder.Nodes.Single(n => n.FromMemberId == Configuration.MemberId);

		/// <summary>
		/// Determines weather or not a property is applicable
		/// </summary>
		public bool IsApplicable => _parentNodes.All(p => Configuration.ApplicableIfFunc.Invoke(p));

		internal NodeProperty(DefinitionNode node, PropertyConfiguration configuration)
			: this(new[]{ node }, configuration)
		{
		}

		internal NodeProperty(IEnumerable<DefinitionNode> nodes, PropertyConfiguration configuration)
		{
			_configuration = ArgumentValidation.AssertNotNull(configuration, nameof(configuration));
			_parentNodes = nodes;
		}

		public bool ValidateInput(object input, out string displayMessage)
		{
			object validated = input;
			string message = displayMessage = string.Empty;
			foreach (var validator in Configuration.Validators)
			{
				if (!validator.ValidateInput(this, input, out validated, out message))
				{
					displayMessage = message;
					return false;
				}

				input = validated;
			}

			displayMessage = null;
			Value = validated;
			return true;
		}
	}
}
