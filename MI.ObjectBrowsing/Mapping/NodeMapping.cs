﻿using System;
using System.Reflection;
using MI.Core;
using MI.Framework.ObjectBrowsing.Configuration;

namespace MI.Framework.ObjectBrowsing.Mapping
{
	/// <summary>
	/// A mapping between a node and its target object.
	/// </summary>
	/// <typeparam name="TTarget">The type of target object.</typeparam>
	/// <typeparam name="TNode">The native .NET type for the node.</typeparam>
	/// <typeparam name="TConfiguration">The type of configuration for the node.</typeparam>
	public abstract class NodeMapping<TTarget, TNode, TConfiguration> : INodeMapping
		where TTarget : class
		where TNode : BrowserNode
		where TConfiguration : NodeTypeConfiguration
	{
		/// <summary>
		/// The configuration built by the mapping.
		/// </summary>
		protected TConfiguration Configuration => _configuration;
		private TConfiguration _configuration = (TConfiguration)Activator.CreateInstance(typeof(TConfiguration),
			BindingFlags.NonPublic | BindingFlags.Instance, null, new object[] { typeof(TNode), typeof(TTarget) }, null);

		/// <summary>
		/// Configures a type of operation permitted to be performed on all nodes of the type being mapped.
		/// </summary>
		/// <param name="operationType">The type of operation to permit.</param>
		public void PermitNodeOperation(OperationType operationType)
		{
			PermitNodeOperation(operationType, null);
		}

		/// <summary>
		/// Configures a type of operation permitted to be performed on all nodes of the type being mapped for which the given predicate is true.
		/// </summary>
		/// <param name="operationType">The type of operation to permit.</param>
		/// <param name="predicateFunc">The predicate function that determines whether the given operation type is permitted for a given node 
		/// of the type being mapped.</param>
		public void PermitNodeOperation(OperationType operationType, Func<BrowserNode, bool> predicateFunc)
		{
			Configuration.CustomNodeOperations.Add(operationType, predicateFunc);
		}

		/// <summary>
		/// Compiles the node mapping into a node type.
		/// </summary>
		/// <param name="passNumber">The compile pass number.</param>
		/// <param name="furtherPassNeeded">Stipulates whether a further compiler pass is needed.</param>
		/// <returns>The compiled node type.</returns>
		public NodeType Compile(int passNumber, out bool furtherPassNeeded)
		{
			if (passNumber == 1)
			{
				furtherPassNeeded = true;
				return new NodeType(_configuration);
			}
			else if (passNumber == 2)
			{
				Configuration.Folders.ForEach(folder => folder.ResolvePermittedTypes());
			}

			furtherPassNeeded = false;
			return null;
		}
	}
}
