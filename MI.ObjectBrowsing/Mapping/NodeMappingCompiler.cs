﻿using System.Collections.Generic;
using MI.Core.Validation;

namespace MI.Framework.ObjectBrowsing.Mapping
{
	/// <summary>
	/// Compiles a set of node mappings.
	/// </summary>
	public static class NodeMappingCompiler
	{
		/// <summary>
		/// Compiles the given collection of node mappings and registers them in the node type system.
		/// </summary>
		/// <param name="nodeMappings">The node mapping types to compile.</param>
		public static IEnumerable<NodeType> Compile(IEnumerable<INodeMapping> nodeMappings)
		{
			nodeMappings = ArgumentValidation.AssertNotNull(nodeMappings, nameof(nodeMappings));

			int passNumber = 0;
			bool furtherPassNeeded;
			do
			{
				passNumber++;
				furtherPassNeeded = false;
				foreach (var mapping in nodeMappings)
				{
					var nodeType = mapping.Compile(passNumber, out var itemFurtherPassNeeded);
					if (nodeType != null)
					{
						yield return nodeType;
					}

					furtherPassNeeded = itemFurtherPassNeeded ? true : furtherPassNeeded;
				}
			}
			while (furtherPassNeeded);
		}
	}
}
