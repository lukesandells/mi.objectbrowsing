﻿using MI.Framework.ObjectBrowsing.Configuration;

namespace MI.Framework.ObjectBrowsing.Mapping
{
	/// <summary>
	/// Provides information about the mapping between a node type and a target object type.
	/// </summary>
	/// <typeparam name="TTarget">The type of target object.</typeparam>
	public class TypeMapping<TTarget>
	{
		/// <summary>
		/// The configuration built by the mapping.
		/// </summary>
		private NodeTypeConfiguration _configuration;
		
		/// <summary>
		/// Mapping of initialiser properties.
		/// </summary>
		public InitialiserMapping<TTarget> InitialiseWith => new InitialiserMapping<TTarget>(_configuration);
		
		/// <summary>
		/// Initialises a new instance of the <see cref="TypeMapping{TTarget}"/> class.
		/// </summary>
		/// <param name="configuration"></param>
		public TypeMapping(NodeTypeConfiguration configuration)
		{
			_configuration = configuration;
		}

		/// <summary>
		/// Configures a strategy to pre-fetch target objects corresponding to a node and its descendants from the database to improve
		/// the performance of node operations.
		/// </summary>
		/// <param name="preFetchStrategy">The pre-fetch strategy to use.</param>
		/// <returns>The type mapping.</returns>
		public TypeMapping<TTarget> PreFetchWith(ITargetObjectPreFetchStrategy preFetchStrategy)
		{
			_configuration.PreFetchStrategy = preFetchStrategy;
			return this;
		}
	}
}
