﻿using System;
using System.Linq.Expressions;

namespace MI.Framework.ObjectBrowsing.Mapping
{
	/// <summary>
	/// Extension methods for a member access expression.
	/// </summary>
	internal static class MemberAccessExpressionExtensions
	{
		/// <summary>
		/// Gets the member name of a member access expression for a property member of a target object.
		/// </summary>
		/// <typeparam name="TTarget">The type of target object.</typeparam>
		/// <typeparam name="TProperty">The type of value held by the property.</typeparam>
		/// <param name="expression">The member access expression.</param>
		/// <returns>The member name of the property member of the target object type.</returns>
		public static string GetMemberName<TTarget, TProperty>(this Expression<Func<TTarget, TProperty>> expression)
		{
			if (expression.Body.NodeType != ExpressionType.MemberAccess)
			{
				if ((expression.Body.NodeType == ExpressionType.Convert) && (expression.Body.Type == typeof(TProperty)))
				{
					return ((MemberExpression)((UnaryExpression)expression.Body).Operand).Member.Name;
				}

				throw new ArgumentException(ExceptionMessage.PropertyExpressionMustBeMemberAccessExpression, nameof(expression));
			}

			return ((MemberExpression)expression.Body).Member.Name;
		}
	}
}
