﻿using System;
using MI.Core.Validation;
using MI.Framework.ObjectBrowsing.Configuration;

namespace MI.Framework.ObjectBrowsing.Mapping
{
	/// <summary>
	/// Establishes the configuration for a type of root node.
	/// </summary>
	/// <typeparam name="TRootNode">The type of root node.</typeparam>
	/// <typeparam name="TConfiguration">The type of configuration for the root node.</typeparam>
	public class RootNodeMapping<TRootNode, TConfiguration> : NodeMapping<TRootNode, TRootNode, TConfiguration>
		where TRootNode : RootNode
		where TConfiguration : NodeTypeConfiguration
	{
		/// <summary>
		/// Configures a folder member for the root node type.
		/// </summary>
		public class FolderMapping
		{
			/// <summary>
			/// The configuration built by the mapping.
			/// </summary>
			private FolderConfiguration _configuration;

			/// <summary>
			/// Initialises a new instance of the <see cref="FolderMapping"/> class.
			/// </summary>
			/// <param name="configuration">The configuration to be built by the mapping.</param>
			internal FolderMapping(FolderConfiguration configuration)
			{
				_configuration = configuration;
			}

			/// <summary>
			/// Adds a new permitted type to the folder configuration.
			/// </summary>
			/// <typeparam name="TElement">The type of element permitted in the folder.</typeparam>
			/// <param name="permittedTypeMapping">An action that establishes the mapping for the permitted type.</param>
			/// <returns>The folder mapping to which the permitted type was added.</returns>
			public FolderMapping AddPermittedType<TElement>(Action<PermittedNodeTypeMapping<TElement>> permittedTypeMapping)
				where TElement : class
			{
				var permittedTypeConfiguration = new FolderConfiguration.PermittedTypeConfiguration(_configuration);
				permittedTypeMapping(new PermittedNodeTypeMapping<TElement>(permittedTypeConfiguration));
				_configuration.AddPermittedType(typeof(TElement), permittedTypeConfiguration);
				return this;
			}
		}

		/// <summary>
		/// Provides a means of providing folder-specific configuration for each node type permitted within a folder.
		/// </summary>
		/// <typeparam name="TElement">The type of element permitted within a folder.</typeparam>
		public class PermittedNodeTypeMapping<TElement>
			where TElement : class
		{
			/// <summary>
			/// The permitted type configuration established by the mapping.
			/// </summary>
			private FolderConfiguration.PermittedTypeConfiguration _configuration;

			/// <summary>
			/// Initialises a new instance of the <see cref="PermittedNodeTypeMapping{TElement}"/> class.
			/// </summary>
			/// <param name="configuration">The configuration to be established by the mapping.</param>
			internal PermittedNodeTypeMapping(FolderConfiguration.PermittedTypeConfiguration configuration)
			{
				_configuration = configuration;
			}

			/// <summary>
			/// Establishes supplementary logic for determining whether a node with a given target object may be added to the folder.
			/// </summary>
			/// <param name="canAddPredicate">The predicate for determining whether a node with the given target object may be added to the folder.</param>
			/// <returns>The mapping upon which the predicate was set.</returns>
			public PermittedNodeTypeMapping<TElement> CanAddIf(Func<TRootNode, TElement, bool> canAddPredicate)
			{
				_configuration.CanAddPredicate = (rootNode, element) => canAddPredicate?.Invoke((TRootNode)rootNode, (TElement)element) ?? true;
				return this;
			}

			/// <summary>
			/// Establishes supplementary logic for determining whether a new node with a given set of initialisation parameters may be added to the folder.
			/// </summary>
			/// <param name="canAddNewPredicate">The predicate for determining whether a node with the given set of initialisation parameters 
			/// may be added to the folder.</param>
			/// <returns>The mapping upon which the predicate was set.</returns>
			public PermittedNodeTypeMapping<TElement> CanAddNewIf(Func<TRootNode, object[], bool> canAddNewPredicate)
			{
				_configuration.CanAddNewPredicate = (rootNode, @params) => canAddNewPredicate?.Invoke((TRootNode)rootNode, @params) ?? true;
				return this;
			}
		}

		/// <summary>
		/// Maps details pertaining to the root node type.
		/// </summary>
		/// <param name="nodeTypeId">Unique identifier of the root node type.</param>
		public void Type(Guid rootNodeTypeId)
		{
			Configuration.NodeTypeId = rootNodeTypeId;
			Configuration.AddFolder(FolderConfiguration.RecycleBin);
		}

		/// <summary>
		/// Maps a primary folder for the root node type.
		/// </summary>
		/// <typeparam name="TElement">A type of element permitted in the folder.</typeparam>
		/// <param name="memberId">Uniquely identifies the folder member of the root node type.</param>
		/// <returns>A folder mapping for providing additional mapping information for the folder.</returns>
		public FolderMapping PrimaryFolder<TElement>(Guid memberId) where TElement : class
		{
			return PrimaryFolder<TElement>(memberId, null);
		}

		/// <summary>
		/// Maps a primary folder for the root node type.
		/// </summary>
		/// <typeparam name="TElement">A type of element permitted in the folder.</typeparam>
		/// <param name="memberId">Uniquely identifies the folder member of the root node type.</param>
		/// <param name="permittedTypeMapping">An action that uses a permitted type mapping to configure folder-specific permitted type information.</param>
		/// <returns>A folder mapping for providing additional mapping information for the folder.</returns>
		public FolderMapping PrimaryFolder<TElement>(Guid memberId, Action<PermittedNodeTypeMapping<TElement>> permittedTypeMapping) where TElement : class
		{
			var folder = new FolderConfiguration(memberId)
			{
				AllowsDefinitionNodes = true,
				IsPrimary = true
			};

			var permittedTypeConfiguration = new FolderConfiguration.PermittedTypeConfiguration(folder);
			permittedTypeMapping?.Invoke(new PermittedNodeTypeMapping<TElement>(permittedTypeConfiguration));
			folder.AddPermittedType(typeof(TElement), permittedTypeConfiguration);
			Configuration.AddFolder(folder);
			return new FolderMapping(folder);
		}

		/// <summary>
		/// Maps a secondary folder for the root node type.
		/// </summary>
		/// <typeparam name="TElement">A type of element that may be held in the folder.</typeparam>
		/// <param name="memberId">Uniquely identifies the folder member of the root node type.</param>
		/// <param name="displayName">The display name for the folder.</param>
		/// <returns>A folder mapping for providing additional mapping information for the folder.</returns>
		public FolderMapping SecondaryFolder<TElement>(Guid memberId, string displayName) where TElement : class
		{
			return SecondaryFolder<TElement>(memberId, displayName, null);
		}

		/// <summary>
		/// Maps a secondary folder for the node type.
		/// </summary>
		/// <typeparam name="TElement">A type of element that may be held in the folder.</typeparam>
		/// <param name="memberId">Uniquely identifies the folder member of the root node type.</param>
		/// <param name="displayName">The display name for the folder.</param>
		/// <param name="permittedTypeMapping">An action that uses a permitted type mapping to configure folder-specific permitted type information.</param>
		/// <returns>A folder mapping for providing additional mapping information for the folder.</returns>
		public FolderMapping SecondaryFolder<TElement>(Guid memberId, string displayName,
			Action<PermittedNodeTypeMapping<TElement>> permittedTypeMapping) where TElement : class
		{
			var folder = new FolderConfiguration(memberId)
			{
				DisplayName = ArgumentValidation.AssertNotNullOrEmpty(displayName, nameof(displayName)),
				AllowsDefinitionNodes = true
			};

			var permittedTypeConfiguration = new FolderConfiguration.PermittedTypeConfiguration(folder);
			permittedTypeMapping?.Invoke(new PermittedNodeTypeMapping<TElement>(permittedTypeConfiguration));
			folder.AddPermittedType(typeof(TElement), permittedTypeConfiguration);
			Configuration.AddFolder(folder);
			return new FolderMapping(folder);
		}
	}
}
