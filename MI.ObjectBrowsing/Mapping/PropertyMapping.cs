﻿using System;
using MI.Framework.ObjectBrowsing.Configuration;
using MI.Core.Validation;
using MI.Framework.ObjectBrowsing.Validation;

namespace MI.Framework.ObjectBrowsing.Mapping
{
	/// <summary>
	/// Mapping of a property member of a node type to the property member of its corresponding target object.
	/// </summary>
	/// <typeparam name="TNode">The type of node to which the property being mapped belongs.</typeparam>
	public class PropertyMapping<TNode>
		where TNode : DefinitionNode
	{
		/// <summary>
		/// The configuration being built by the mapping.
		/// </summary>
		internal PropertyConfiguration Configuration => _configuration;
		private PropertyConfiguration _configuration;

		/// <summary>
		/// Initialises a new instance of the <see cref="PropertyMapping{TNode}"/> class.
		/// </summary>
		/// <param name="configuration">The configuration to be built by the mapping.</param>
		internal PropertyMapping(PropertyConfiguration configuration)
		{
			_configuration = configuration;
		}

		/// <summary>
		/// A function containing a formula for automatically determining the value of the property.
		/// </summary>
		/// <param name="formulaFunc">The function containing the formula for determining the property value.</param>
		/// <returns>The property mapping.</returns>
		public PropertyMapping<TNode> Formula(Func<TNode, object> formulaFunc)
		{
			formulaFunc = ArgumentValidation.AssertNotNull(formulaFunc, nameof(formulaFunc));
			_configuration.FormulaFunc = (node) => formulaFunc((TNode)node);
			return this;
		}

		/// <summary>
		/// Configures an input validator for the property being mapped.
		/// </summary>
		/// <param name="validator">The input validator to apply.</param>
		/// <returns>The property mapping.</returns>
		/// <remarks>
		/// Multiple input validators may be configured for the property.  The validators shall be invoked in a chain in the order
		/// in which this method is invoked in the mapping.  The output of each validator shall be provided as the input to the next
		/// validator in the chain.
		/// </remarks>
		public PropertyMapping<TNode> ValidateWith(IInputValidator validator)
		{
			validator = ArgumentValidation.AssertNotNull(validator, nameof(validator));
			_configuration.Validators.Add(validator);
			return this;
		}

		/// <summary>
		/// A function containning a formula for automatically determining whether or not the property is applicable.
		/// </summary>
		/// <param name="applicableIfFunc">The function containing the formulate for determining if the property is applicable</param>
		/// <returns></returns>
		public PropertyMapping<TNode> ApplicableIf(Func<TNode, bool> applicableIfFunc)
		{
			applicableIfFunc = ArgumentValidation.AssertNotNull(applicableIfFunc, nameof(applicableIfFunc));
			_configuration.ApplicableIfFunc = (node) => applicableIfFunc((TNode) node);
			return this;
		}

		/// <summary>
		/// Sets the property as readonly
		/// </summary>
		public void ReadOnly(bool readOnly)
		{
			_configuration.IsReadOnly = readOnly;
		}
	}
}
