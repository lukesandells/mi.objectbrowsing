﻿using System;
using System.Linq;
using System.Linq.Expressions;
using MI.Core;
using MI.Core.Validation;
using MI.Framework.ObjectBrowsing.Configuration;
using MI.Framework.ObjectBrowsing.Validation;

namespace MI.Framework.ObjectBrowsing.Mapping
{
	/// <summary>
	/// A mapping between a definition node and its target object.
	/// </summary>
	/// <typeparam name="TTarget">The type of target object.</typeparam>
	/// <typeparam name="TNode">The type of definition node.</typeparam>
	/// <typeparam name="TConfiguration">The type of configuration for the definition node.</typeparam>
	public class DefinitionNodeMapping<TTarget, TNode, TConfiguration> : NodeMapping<TTarget, TNode, TConfiguration>
		where TTarget : class
		where TNode : DefinitionNode
		where TConfiguration : NodeTypeConfiguration
	{
		/// <summary>
		/// A mapping between a folder member of a node type and a corresponding collection member of its target object.
		/// </summary>
		/// <remarks>
		/// Note that it is acceptable for a node folder to have no coreresponding collection member for its target object.
		/// </remarks>
		public class FolderMapping
		{
			/// <summary>
			/// The folder configuration to which the folder mapping pertains.
			/// </summary>
			private FolderConfiguration _configuration;

			/// <summary>
			/// Initialises a new instance of the <see cref="FolderMapping"/> class.
			/// </summary>
			/// <param name="configuration">The folder configuration to be built by the new mapping.</param>
			internal FolderMapping(FolderConfiguration configuration)
			{
				_configuration = configuration;
			}

			/// <summary>
			/// Adds a new permitted type to the folder configuration.
			/// </summary>
			/// <typeparam name="TElement">The type of element permitted in the folder.</typeparam>
			/// <param name="permittedTypeMapping">An action that establishes the mapping for the permitted type.</param>
			/// <returns>The folder mapping to which the permitted type was added.</returns>
			public FolderMapping AddPermittedType<TElement>(Action<PermittedNodeTypeMapping<TElement>> permittedTypeMapping)
				where TElement : class
			{
				var permittedTypeConfiguration = new FolderConfiguration.PermittedTypeConfiguration(_configuration);
				permittedTypeMapping(new PermittedNodeTypeMapping<TElement>(permittedTypeConfiguration));
				_configuration.AddPermittedType(typeof(TElement), permittedTypeConfiguration);
				return this;
			}

			/// <summary>
			/// Adds a new permitted operation to the folder configuration.
			/// </summary>
			/// <param name="operationType">The type of permitted operation to add.</param>
			/// <returns>The folder mapping to which the permitted operation was added.</returns>
			public FolderMapping AddPermittedOperation(OperationType operationType)
			{
				AddPermittedOperation(operationType, null);
				return this;
			}

			/// <summary>
			/// Adds a new permitted operation to the folder configuration with a predicate stipulating the conditions under 
			/// which the type of operation is permitted.
			/// </summary>
			/// <param name="operationType">The type of permitted operation to add.</param>
			/// <param name="predicateFunc">A predicate stipulating the conditions under which the type of operation is permitted.</param>
			/// <returns>The folder mapping to which the permitted operation was added.</returns>
			public FolderMapping AddPermittedOperation(OperationType operationType, Func<BrowserFolder, bool> predicateFunc)
			{
				_configuration.CustomOperations.Add(operationType, predicateFunc);
				return this;
			}
		}

		/// <summary>
		/// Provides a means of configuring an inverse folder member of a node type.
		/// </summary>
		public class InverseFolderMapping
		{
			/// <summary>
			/// The folder configuration established by the inverse folder mapping.
			/// </summary>
			private FolderConfiguration _configuration;

			/// <summary>
			/// Initialises a new instance of the <see cref="InverseFolderMapping"/> class.
			/// </summary>
			/// <param name="configuration">The folder configuration to be established by the inverse folder mapping.</param>
			internal InverseFolderMapping(FolderConfiguration configuration)
			{
				_configuration = configuration;
			}

			/// <summary>
			/// Adds a permitted type to the inverse folder configuration.
			/// </summary>
			/// <typeparam name="TElement">The type of element permitted in the inverse folder.</typeparam>
			/// <returns>The folder mapping to which the permitted type was added.</returns>
			public InverseFolderMapping AddPermittedType<TElement>()
				where TElement : class
			{
				_configuration.AddPermittedType(typeof(TElement), new FolderConfiguration.PermittedTypeConfiguration(_configuration));
				return new InverseFolderMapping(_configuration);
			}
		}

		/// <summary>
		/// Provides a means of providing folder-specific configuration for each node type permitted within a folder.
		/// </summary>
		/// <typeparam name="TElement">The type of element permitted within a folder.</typeparam>
		public class PermittedNodeTypeMapping<TElement>
			where TElement : class
		{
			/// <summary>
			/// The permitted type configuration established by the mapping.
			/// </summary>
			private FolderConfiguration.PermittedTypeConfiguration _configuration;

			/// <summary>
			/// Initialises a new instance of the <see cref="PermittedNodeTypeMapping{TElement}"/> class.
			/// </summary>
			/// <param name="configuration">The configuration to be established by the mapping.</param>
			internal PermittedNodeTypeMapping(FolderConfiguration.PermittedTypeConfiguration configuration)
			{
				_configuration = configuration;
			}

			/// <summary>
			/// Sets the action that will effect the addition of a new node to the folder in a definition node's target object.
			/// </summary>
			/// <param name="addAction">The action that will effect the addition of a new node to the folder in a definition node's target object.</param>
			/// <returns>The mapping upon which the add action was set.</returns>
			public PermittedNodeTypeMapping<TElement> AddWith(Action<TTarget, TElement> addAction)
			{
				_configuration.AddAction = (parent, child) => addAction?.Invoke((TTarget)parent, (TElement)child);
				return this;
			}

			/// <summary>
			/// Sets the action that will effect the removal of a node from the folder in a definition node's target object.
			/// </summary>
			/// <param name="removeAction">The action that will effect the removal of a node from the folder in a definition node's target object.</param>
			/// <returns>The mapping upon which the remove action was set.</returns>
			public PermittedNodeTypeMapping<TElement> RemoveWith(Action<TTarget, TElement> removeAction)
			{
				_configuration.RemoveAction = (parent, child) => removeAction?.Invoke((TTarget)parent, (TElement)child);
				return this;
			}

			/// <summary>
			/// Establishes supplementary logic for determining whether a node with a given target object may be added to the folder.
			/// </summary>
			/// <param name="canAddPredicate">The predicate for determining whether a node with the given target object may be added to the folder.</param>
			/// <returns>The mapping upon which the predicate was set.</returns>
			public PermittedNodeTypeMapping<TElement> CanAddIf(Func<TTarget, TElement, bool> canAddPredicate)
			{
				_configuration.CanAddPredicate = (parent, child) => canAddPredicate?.Invoke((TTarget)parent, (TElement)child) ?? true;
				return this;
			}

			/// <summary>
			/// Establishes supplementary logic for determining whether a new node with a given set of initialisation parameters may be added to the folder.
			/// </summary>
			/// <param name="canAddNewPredicate">The predicate for determining whether a node with the given set of initialisation parameters 
			/// may be added to the folder.</param>
			/// <returns>The mapping upon which the predicate was set.</returns>
			public PermittedNodeTypeMapping<TElement> CanAddNewIf(Func<TTarget, object[], bool> canAddNewPredicate)
			{
				_configuration.CanAddNewPredicate = (parent, @params) => canAddNewPredicate?.Invoke((TTarget)parent, @params) ?? true;
				return this;
			}

			/// <summary>
			/// Configures the inverse property member for the type permitted within the folder.
			/// </summary>
			/// <param name="inverseMemberId">Identifies the inverse property member.</param>
			/// <param name="updateOnAddAndRemove">Stipulates whether the framework shall automatically update the inverse property of definition nodes
			/// that are added to and removed from the folder.</param>
			/// <returns>The mapping upon which the inverse property was set.</returns>
			public PermittedNodeTypeMapping<TElement> InverseProperty(Guid inverseMemberId, bool updateOnAddAndRemove)
			{
				if (updateOnAddAndRemove && !_configuration.FolderConfiguration.AllowsDefinitionNodes)
				{
					// Updates on add/remove can only be performed on definition nodes and the <folder name> folder is configured to 
					// not permit definition nodes.
					throw new ArgumentException(string.Format(ExceptionMessage.CannotAutomaticallyUpdateInversePropertyOnAddAndRemove,
						_configuration.FolderConfiguration.DisplayName), nameof(updateOnAddAndRemove));
				}

				_configuration.InversePropertyMemberId = inverseMemberId;
				return this;
			}
		}

		/// <summary>
		/// Maps details pertaining to the node type.
		/// </summary>
		/// <param name="nodeTypeId">Unique identifier of the node type.</param>
		/// <param name="displayName">The display name of the node type.</param>
		/// <returns>A type mapping for providing additional information about the node type being mapped.</returns>
		public TypeMapping<TTarget> Type(Guid nodeTypeId, string displayName)
		{
			Configuration.NodeTypeId = nodeTypeId;
			Configuration.DisplayName = displayName;
			Configuration.AddFolder(FolderConfiguration.PropertyLinksFolder);
			return new TypeMapping<TTarget>(Configuration);
		}

		/// <summary>
		/// Maps details pertaining to how link node types are established when creating links to definition nodes.
		/// </summary>
		/// <typeparam name="TLinkTargetObject">The type of target object for link nodes.</typeparam>
		/// <param name="linkNodeTypeId">Identifies the type of link node that should be established when creating links to definition nodes.</param>
		/// <param name="linkTargetObjectFunc">A function that returns the target object for link nodes.</param>
		public void LinkAs<TLinkTargetObject>(Guid linkNodeTypeId, Func<TTarget, TLinkTargetObject> linkTargetObjectFunc)
		{
			linkTargetObjectFunc = ArgumentValidation.AssertNotNull(linkTargetObjectFunc, nameof(linkTargetObjectFunc));
			Configuration.LinkAs = new LinkAsConfiguration()
			{
				LinkNodeTypeId = linkNodeTypeId,
				LinkTargetObjectFunc = o => linkTargetObjectFunc((TTarget)o)
			};
		}

		/// <summary>
		/// Maps the display ID member of a node type.
		/// </summary>
		/// <param name="memberId">Uniquely identifies the display ID property member.</param>
		/// <param name="expression">Expression for accessing the display ID property of the target object.</param>
		/// <param name="displayName">The display name for the display ID property member.</param>
		/// <returns>A display ID mapping for providing additional display ID property mapping information.</returns>
		public DisplayIdMapping<TTarget, TNode> DisplayId(Guid memberId, Expression<Func<TTarget, string>> expression, string displayName)
		{
			return DisplayId(memberId, expression.GetMemberName(), displayName);
		}

		/// <summary>
		/// Maps the display ID member of a node type.
		/// </summary>
		/// <param name="memberId">Uniquely identifies the display ID property member.</param>
		/// <param name="memberName">The name of the field or property display ID member of the target object.</param>
		/// <param name="displayName">The display name for the display ID property member.</param>
		/// <returns>A display ID mapping for providing additional display ID property mapping information.</returns>
		public DisplayIdMapping<TTarget, TNode> DisplayId(Guid memberId, string memberName, string displayName)
		{
			Configuration.DisplayId.Property = Property(memberId, memberName, displayName, false).Configuration;
			Configuration.DisplayId.Property.Validators.Add(new DisplayIdInputValidator(true));
			return new DisplayIdMapping<TTarget, TNode>(Configuration.DisplayId);
		}

		/// <summary>
		/// Maps the description member of a node type.
		/// </summary>
		/// <param name="memberId">Uniquely identifies the description property member.</param>
		/// <param name="expression">Expression for accessing the description property of the target object.</param>
		/// <param name="displayName">The display name for the description property member.</param>
		/// <returns>A property mapping for providing additional description property mapping information.</returns>
		public PropertyMapping<TNode> Description(Guid memberId, Expression<Func<TTarget, string>> expression, string displayName)
		{
			return Description(memberId, expression.GetMemberName(), displayName);
		}

		/// <summary>
		/// Maps the description member of a node type.
		/// </summary>
		/// <param name="memberId">Uniquely identifies the description property member.</param>
		/// <param name="memberName">The name of the field or property description member of the target object.</param>
		/// <param name="displayName">The display name for the description property member.</param>
		/// <returns>A property mapping for providing additional description property mapping information.</returns>
		public PropertyMapping<TNode> Description(Guid memberId, string memberName, string displayName)
		{
			var propertyMapping = Property(memberId, memberName, displayName);
			Configuration.DescriptionProperty = propertyMapping.Configuration;
			return propertyMapping;
		}

		/// <summary>
		/// Maps a primary folder for the node type.
		/// </summary>
		/// <typeparam name="TElement">A type of element permitted in the folder.</typeparam>
		/// <param name="memberId">Uniquely identifies the folder member of the node type.</param>
		/// <returns>A folder mapping for providing additional mapping information for the folder.</returns>
		public FolderMapping PrimaryFolder<TElement>(Guid memberId) where TElement : class
		{
			return PrimaryFolder<TElement>(memberId, null);
		}

		/// <summary>
		/// Maps a primary folder for the node type.
		/// </summary>
		/// <typeparam name="TElement">A type of element permitted in the folder.</typeparam>
		/// <param name="memberId">Uniquely identifies the folder member of the node type.</param>
		/// <param name="permittedTypeMapping">An action that uses a permitted type mapping to configure folder-specific permitted type information.</param>
		/// <returns>A folder mapping for providing additional mapping information for the folder.</returns>
		public FolderMapping PrimaryFolder<TElement>(Guid memberId, Action<PermittedNodeTypeMapping<TElement>> permittedTypeMapping) where TElement : class
		{
			var folder = new FolderConfiguration(memberId)
			{
				AllowsDefinitionNodes = true,
				IsPrimary = true
			};

			var permittedTypeConfiguration = new FolderConfiguration.PermittedTypeConfiguration(folder);
			permittedTypeMapping?.Invoke(new PermittedNodeTypeMapping<TElement>(permittedTypeConfiguration));
			folder.AddPermittedType(typeof(TElement), permittedTypeConfiguration);
			Configuration.AddFolder(folder);
			return new FolderMapping(folder);
		}

		/// <summary>
		/// Maps a secondary folder for the node type.
		/// </summary>
		/// <typeparam name="TElement">A type of element that may be held in the folder.</typeparam>
		/// <param name="memberId">Uniquely identifies the folder member of the node type.</param>
		/// <param name="displayName">The display name for the folder.</param>
		/// <param name="allowsDefinitionNodes">Stipulates whether the folder may contain definition nodes.</param>
		/// <param name="allowsLinkNodes">Stipulates whether the folder may contain link nodes</param>
		/// <returns>A folder mapping for providing additional mapping information for the folder.</returns>
		public FolderMapping SecondaryFolder<TElement>(Guid memberId, string displayName, bool allowsDefinitionNodes, bool allowsLinkNodes) where TElement : class
		{
			return SecondaryFolder<TElement>(memberId, displayName, allowsDefinitionNodes, allowsLinkNodes, null);
		}

		/// <summary>
		/// Maps a secondary folder for the node type.
		/// </summary>
		/// <typeparam name="TElement">A type of element that may be held in the folder.</typeparam>
		/// <param name="memberId">Uniquely identifies the folder member of the node type.</param>
		/// <param name="displayName">The display name for the folder.</param>
		/// <param name="allowsDefinitionNodes">Stipulates whether the folder may contain definition nodes.</param>
		/// <param name="allowsLinkNodes">Stipulates whether the folder may contain link nodes.</param>
		/// <param name="permittedTypeMapping">An action that uses a permitted type mapping to configure folder-specific permitted type information.</param>
		/// <returns>A folder mapping for providing additional mapping information for the folder.</returns>
		public FolderMapping SecondaryFolder<TElement>(Guid memberId, string displayName, bool allowsDefinitionNodes, bool allowsLinkNodes,
			Action<PermittedNodeTypeMapping<TElement>> permittedTypeMapping) where TElement : class
		{
			var folder = new FolderConfiguration(memberId)
			{
				DisplayName = ArgumentValidation.AssertNotNullOrEmpty(displayName, nameof(displayName)),
				AllowsDefinitionNodes = allowsDefinitionNodes,
				AllowsLinkNodes = allowsLinkNodes
			};

			var permittedTypeConfiguration = new FolderConfiguration.PermittedTypeConfiguration(folder);
			permittedTypeMapping?.Invoke(new PermittedNodeTypeMapping<TElement>(permittedTypeConfiguration));
			folder.AddPermittedType(typeof(TElement), permittedTypeConfiguration);
			Configuration.AddFolder(folder);
			return new FolderMapping(folder);
		}

		/// <summary>
		/// Maps an inverse folder for the node type.
		/// </summary>
		/// <typeparam name="TElement">A type of element that may be held in the folder.</typeparam>
		/// <param name="memberId">Uniquely identifies the folder member of the node type.</param>
		/// <param name="displayName">The display name for the folder.</param>
		/// <param name="inverseMemberId">Identifies the inverse folder member of any types of nodes containing links to 
		/// nodes of the node type being mapped, for which inverse links shall be "mirrored" to the inverse folder.</param>
		/// <returns></returns>
		public InverseFolderMapping InverseFolder<TElement>(Guid memberId, string displayName, Guid inverseMemberId)
		{
			var folder = new FolderConfiguration(memberId)
			{
				DisplayName = ArgumentValidation.AssertNotNullOrEmpty(displayName, nameof(displayName)),
				InverseFolderMemberId = inverseMemberId,
				IsInverse = true,
				AllowsLinkNodes = true
			};

			folder.AddPermittedType(typeof(TElement), new FolderConfiguration.PermittedTypeConfiguration(folder));
			Configuration.AddFolder(folder);
			return new InverseFolderMapping(folder);
		}

		/// <summary>
		/// Stipulates a type of operation permitted for all folders of all nodes of the type being mapped.
		/// </summary>
		/// <param name="operationType">The permitted operation type.</param>
		public void PermitFolderOperation(OperationType operationType)
		{
			PermitFolderOperation(operationType, null);
		}

		/// <summary>
		/// Stipulates a type of operation permitted for all folders for which a given predicate function is true of all nodes of the type being mapped.
		/// </summary>
		/// <param name="operationType">The permitted operation type.</param>
		/// <param name="predicateFunc">A predicate function that stipulates whether the given type of operation is permitted for a particular folder.</param>
		public void PermitFolderOperation(OperationType operationType, Func<BrowserFolder, bool> predicateFunc)
		{
			Configuration.CustomFolderOperations.Add(operationType, predicateFunc);
			Configuration.Folders.Where(fc => !fc.SystemManaged).ForEach(fc => fc.CustomOperations.Add(operationType, predicateFunc));
		}

		/// <summary>
		/// Maps a property between a node type and its target object type.
		/// </summary>
		/// <typeparam name="TProperty">The type of value held by the property.</typeparam>
		/// <param name="memberId">Unique identifier of the property member within the node type.</param>
		/// <param name="expression">Expression for accessing the property value from the target object.</param>
		/// <param name="displayName">The property display name.</param>
		/// <param name="supportsBatchUpdate">Stipulates whether the mapped property supports a single value being applied to multiple 
		/// instances of the property across multiple definition nodes.</param>
		/// <param name="groupName">The group name for the mapped property. Group names allow node properties to be grouped for display purposes.</param>
		/// <returns>A property mapping for providing additional mapping information for the property.</returns>
		public PropertyMapping<TNode> Property<TProperty>(Guid memberId, Expression<Func<TTarget, TProperty>> expression, string displayName, bool supportsBatchUpdate = true, string groupName = "")
		{
			return Property(memberId, expression.GetMemberName(), displayName, supportsBatchUpdate, groupName);
		}

		/// <summary>
		/// Maps a property between a node type and its target object type.
		/// </summary>
		/// <typeparam name="TProperty">The type of value held by the property.</typeparam>
		/// <param name="memberId">Unique identifier of the property member within the node type.</param>
		/// <param name="memberName">The name of the corresponding field or property member of the target object.</param>
		/// <param name="displayName">The property display name.</param>
		/// <param name="supportsBatchUpdate">Stipulates whether the mapped property supports a single value being applied to multiple 
		/// instances of the property across multiple definition nodes.</param>
		/// <param name="groupName">The group name for the mapped property. Group names allow node properties to be grouped for display purposes.</param>
		/// <returns>A property mapping for providing additional mapping information for the property.</returns>
		public PropertyMapping<TNode> Property(Guid memberId, string memberName, string displayName, bool supportsBatchUpdate = true, string groupName = "")
		{
			var property = new PropertyConfiguration(memberId, typeof(TTarget), memberName, displayName)
			{
				SupportsBatchUpdate = supportsBatchUpdate,
				GroupName = ArgumentValidation.AssertNotNull(groupName, nameof(groupName)),
				
			};

			Configuration.AddProperty(property);
			return new PropertyMapping<TNode>(property);
		}

		/// <summary>
		/// Configures an action to be invoked when a new node of the type being mapped is initialised.
		/// </summary>
		/// <param name="onInitialiseAction">The action to be invoked.</param>
		/// <remarks>
		/// Multiple actions may be configured by invoking this method for each action.
		/// </remarks>
		public void OnInitialise(Action<TNode, FolderPath> onInitialiseAction)
		{
			Configuration.OnInitialiseAction += (element, path) => onInitialiseAction?.Invoke((TNode)element, path);
		}

		/// <summary>
		/// Configures an action to be invoked when a node of the type being mapped is copied.
		/// </summary>
		/// <param name="onCopyAction">The action to be invoked.</param>
		/// <remarks>
		/// Multiple actions may be configured by invoking this method for each action.
		/// </remarks>
		public void OnCopy(Action<TNode, TNode> onCopyAction)
		{
			Configuration.OnCopyAction += (original, copy) => onCopyAction?.Invoke((TNode)original, (TNode)copy);
		}

		/// <summary>
		/// Configures an action to be invoked when a node of the type being mapped is moved to a node of a given type.
		/// </summary>
		/// <typeparam name="TDestination">The type of node for which the action shall be invoked.</typeparam>
		/// <param name="onMoveToAction">The action to be invoked.</param>
		/// <remarks>
		/// Multiple actions may be configured for a given destination type by invoking this method for each action.
		/// </remarks>
		public void OnMoveTo<TDestination>(Action<TDestination, TNode> onMoveToAction)
			where TDestination : DefinitionNode
		{
			if (!Configuration.OnMoveToActions.ContainsKey(typeof(TDestination)))
			{
				Configuration.OnMoveToActions.Add(typeof(TDestination), null);
			}

			Configuration.OnMoveToActions[typeof(TDestination)] += 
				(destination, element) => onMoveToAction?.Invoke((TDestination)destination, (TNode)element);
		}

		/// <summary>
		/// Configures an action to be invoked when a node of the type being mapped is added to a node of a given type.
		/// </summary>
		/// <typeparam name="TDestination">The type of node for which the action shall be invoked.</typeparam>
		/// <param name="onMoveToAction">The action to be invoked.</param>
		/// <remarks>
		/// Multiple actions may be configured for a given destination type by invoking this method for each action.
		/// </remarks>
		public void OnAddTo<TDestination>(Action<TDestination, TNode> onAddToAction)
			where TDestination : DefinitionNode
		{
			if (!Configuration.OnAddToActions.ContainsKey(typeof(TDestination)))
			{
				Configuration.OnAddToActions.Add(typeof(TDestination), null);
			}

			Configuration.OnAddToActions[typeof(TDestination)] +=
				(destination, element) => onAddToAction?.Invoke((TDestination)destination, (TNode)element);
		}

		/// <summary>
		/// Configures an action to be invoked when a node of the type being mapped is deleted.
		/// </summary>
		/// <param name="onCopyAction">The action to be invoked.</param>
		/// <remarks>
		/// Multiple actions may be configured by invoking this method for each action.
		/// </remarks>
		public void OnDelete(Action<TNode> onDeleteAction)
		{
			Configuration.OnDeleteAction = deleted => onDeleteAction?.Invoke((TNode)deleted);
		}

		/// <summary>
		/// Configures a validator with which to validate nodes of the type being mapped.
		/// </summary>
		/// <param name="validator">The validator with which to validate the nodes.</param>
		/// <remarks>
		/// Multiple validators may be configured by invoking this method for each validator.
		/// </remarks>
		public void ValidateWith(INodeValidator validator)
		{
			validator = ArgumentValidation.AssertNotNull(validator, nameof(validator));
			Configuration.Validators.Add(validator);
			validator.ResultTypes.ForEach(resultType => NodeValidationResultType.Register(resultType));
		}
	}
}
