﻿using MI.Framework.ObjectBrowsing.Configuration;

namespace MI.Framework.ObjectBrowsing.Mapping
{
	/// <summary>
	/// A mapping between a definition node and its target object.
	/// </summary>
	/// <typeparam name="TTarget">The type of target object.</typeparam>
	/// <typeparam name="TNode">The type of definition node.</typeparam>
	public class DefinitionNodeMapping<TTarget, TNode> : DefinitionNodeMapping<TTarget, TNode, NodeTypeConfiguration>
		where TTarget : class
		where TNode : DefinitionNode
	{
	}
}
