﻿using MI.Framework.ObjectBrowsing.Configuration;

namespace MI.Framework.ObjectBrowsing.Mapping
{
	/// <summary>
	/// Establishes the configuration for a type of root node.
	/// </summary>
	/// <typeparam name="TRootNode">The type of root node.</typeparam>
	public class RootNodeMapping<TRootNode> : RootNodeMapping<TRootNode, NodeTypeConfiguration>
		where TRootNode : RootNode
	{
	}
}
