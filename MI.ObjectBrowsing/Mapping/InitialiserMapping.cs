﻿using System;
using System.Linq.Expressions;
using MI.Core.Validation;
using MI.Framework.ObjectBrowsing.Configuration;

namespace MI.Framework.ObjectBrowsing.Mapping
{
	/// <summary>
	/// Maps the set of properties that must be set upon node initialisation between the node type and target object type.
	/// </summary>
	/// <typeparam name="TTarget">The type of target object.</typeparam>
	public class InitialiserMapping<TTarget>
	{
		/// <summary>
		/// The configuration of the node type to which the initialiser pertains.
		/// </summary>
		private NodeTypeConfiguration _configuration;

		/// <summary>
		/// Initialises a new instance of the <see cref="InitialiserMapping{TTarget}"/> class.
		/// </summary>
		/// <param name="configuration">The configuration to be built by the mapping.</param>
		internal InitialiserMapping(NodeTypeConfiguration configuration)
		{
			_configuration = configuration;
		}

		/// <summary>
		/// Maps a property that must be set during node initialisation.
		/// </summary>
		/// <typeparam name="TProperty">The type of value held by the property.</typeparam>
		/// <param name="memberId">Unique identifier of the property member within the node type.</param>
		/// <param name="expression">Expression for accessing the property value from the target object.</param>
		/// <param name="displayName">The property display name.</param>
		/// <param name="supportsBatchUpdate">Stipulates whether the mapped property supports a single value being applied to multiple 
		/// instances of the property across multiple definition nodes.</param>
		/// <param name="groupName">The group name for the mapped property. Group names allow node properties to be grouped for display purposes.</param>
		/// <returns>A property mapping for providing additional mapping information for the property.</returns>
		public PropertyConfiguration Property<TProperty>(Guid memberId, Expression<Func<TTarget,TProperty>> expression, string displayName,
			bool supportsBatchUpdate = true, string groupName = "")
		{
			return Property(memberId, expression.GetMemberName(), displayName, supportsBatchUpdate, groupName);
		}

		/// <summary>
		/// Maps a property that must be set during node initialisation.
		/// </summary>
		/// <typeparam name="TProperty">The type of value held by the property.</typeparam>
		/// <param name="memberId">Unique identifier of the property member within the node type.</param>
		/// <param name="memberName">The name of the corresponding field or property member of the target object.</param>
		/// <param name="displayName">The property display name.</param>
		/// <param name="supportsBatchUpdate">Stipulates whether the mapped property supports a single value being applied to multiple 
		/// instances of the property across multiple definition nodes.</param>
		/// <param name="groupName">The group name for the mapped property. Group names allow node properties to be grouped for display purposes.</param>
		/// <returns>A property mapping for providing additional mapping information for the property.</returns>
		public PropertyConfiguration Property(Guid memberId, string memberName, string displayName, bool supportsBatchUpdate = true, string groupName = "")
		{
			var property = new PropertyConfiguration(memberId, typeof(TTarget), memberName, displayName)
			{
				GroupName = ArgumentValidation.AssertNotNull(groupName, nameof(groupName)),
				SupportsBatchUpdate = supportsBatchUpdate
			};

			_configuration.Initialiser.AddProperty(property);
			_configuration.AddProperty(property);
			return property;
		}
	}
}
