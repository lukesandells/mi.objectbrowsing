﻿using System;
using System.Linq;
using System.Collections.Generic;
using MI.Core.Validation;
using MI.Framework.ObjectBrowsing.Configuration;
using MI.Framework.ObjectBrowsing.Validation;

namespace MI.Framework.ObjectBrowsing.Mapping
{
	/// <summary>
	/// A mapping between a display ID node type member and a field or property member of a node's target object.
	/// </summary>
	/// <typeparam name="TTarget">The type of target object.</typeparam>
	/// <typeparam name="TNode">The native .NET type for the node.</typeparam>
	public class DisplayIdMapping<TTarget, TNode>
		where TTarget : class
		where TNode : DefinitionNode
	{
		/// <summary>
		/// The configuration built by the mapping.
		/// </summary>
		private DisplayIdConfiguration _configuration;

		/// <summary>
		/// Initialises a new instance of the <see cref="DisplayIdMapping{TTarget, TNode}"/> class.
		/// </summary>
		/// <param name="configuration">The configuration to be built by the mapping.</param>
		internal DisplayIdMapping(DisplayIdConfiguration configuration)
		{
			_configuration = configuration;
		}

		/// <summary>
		/// Sets a function that determines the display ID prefix for a new definition node.
		/// </summary>
		/// <param name="prefixFunc">The function that determines the display ID prefix for a new definition node.</param>
		/// <returns>The display ID mapping.</returns>
		public DisplayIdMapping<TTarget, TNode> Prefix(Func<object[], string> prefixFunc)
		{
			_configuration.PrefixFunc = ArgumentValidation.AssertNotNull(prefixFunc, nameof(prefixFunc));
			return this;
		}

		/// <summary>
		/// Sets a function that finds the unavailable display IDs in a given folder path.
		/// </summary>
		/// <param name="findUnavailableIdsFunc">The function that finds the unavailable display IDs in a given folder path.</param>
		/// <returns>The display ID mapping.</returns>
		public DisplayIdMapping<TTarget, TNode> FindUnavailableWith(Func<FolderPath, IQueryable<string>> findUnavailableIdsFunc)
		{
			findUnavailableIdsFunc = ArgumentValidation.AssertNotNull(findUnavailableIdsFunc, nameof(findUnavailableIdsFunc));
			_configuration.FindUnavailableIdsFunc = (path, exclusions) => findUnavailableIdsFunc(path);
			return this;
		}

		/// <summary>
		/// Sets a function that finds the unavailable display IDs in a given folder path, excluding a set of objects that shall not be considered
		/// in determining the unavailable IDs.
		/// </summary>
		/// <param name="findUnavailableIdsFunc">The function that finds the unavailable display IDs in a given folder path, excluding a set of 
		/// objects that shall not be considered in determining the unavailable IDs</param>
		/// <returns>The display ID mapping.</returns>
		public DisplayIdMapping<TTarget, TNode> FindUnavailableWith(Func<FolderPath, IEnumerable<TTarget>, IQueryable<string>> findUnavailableIdsFunc)
		{
			findUnavailableIdsFunc = ArgumentValidation.AssertNotNull(findUnavailableIdsFunc, nameof(findUnavailableIdsFunc));
			_configuration.FindUnavailableIdsFunc = (path, exclusions) => findUnavailableIdsFunc(path, exclusions.Cast<TTarget>());
			return this;
		}

		/// <summary>
		/// Sets the suffix to automatically append to display IDs during copy operations in order to assure their availability at the destination.
		/// </summary>
		/// <param name="suffix">The suffix to append.</param>
		/// <returns>The display ID mapping.</returns>
		public DisplayIdMapping<TTarget, TNode> IdClashOnCopySuffix(string suffix)
		{
			_configuration.IdClashOnCopySuffix = suffix ?? string.Empty;
			return this;
		}

		/// <summary>
		/// Sets the suffix to automatically append to display IDs during move operations in order to assure their availability at the destination.
		/// </summary>
		/// <param name="suffix">The suffix to append.</param>
		/// <returns>The display ID mapping.</returns>
		public DisplayIdMapping<TTarget, TNode> IdClashOnMoveSuffix(string suffix)
		{
			_configuration.IdClashOnMoveSuffix = suffix ?? string.Empty;
			return this;
		}

		/// <summary>
		/// Stipulates whether the display ID for the node type is globally unique across all nodes, including all root nodes.
		/// </summary>
		/// <param name="isGloballyUnique"><c>true</c> if the display ID is globally unqiue; otherwise <c>false</c>.</param>
		/// <returns>The display ID mapping.</returns>
		public DisplayIdMapping<TTarget, TNode> GloballyUnique(bool isGloballyUnique)
		{
			_configuration.IsGloballyUnique = isGloballyUnique;
			return this;
		}

		/// <summary>
		/// Configures an input validator for the display ID property being mapped.
		/// </summary>
		/// <param name="validator">The input validator to apply.</param>
		/// <returns>The display ID mapping.</returns>
		/// <remarks>
		/// Multiple input validators may be configured for the display ID.  The validators shall be invoked in a chain in the order
		/// in which this method is invoked in the mapping.  The output of each validator shall be provided as the input to the next
		/// validator in the chain.
		/// </remarks>
		public DisplayIdMapping<TTarget, TNode> ValidateWith(IInputValidator validator)
		{
			validator = ArgumentValidation.AssertNotNull(validator, nameof(validator));
			_configuration.Property.Validators.Add(validator);
			return this;
		}
	}
}
