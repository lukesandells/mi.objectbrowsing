﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MI.Core;

namespace MI.Framework.ObjectBrowsing
{
	public class PropertyCollection : IEnumerable<NodeProperty>
	{
		private IEnumerable<DefinitionNode> _nodes;
		private IDictionary<Guid, NodeProperty> _nodeProperties = new Dictionary<Guid, NodeProperty>();
		public NodeProperty this[Guid memberId] => _nodeProperties.ContainsKey(memberId)?_nodeProperties[memberId]:null;

		internal PropertyCollection(DefinitionNode node)
			: this(new[] { node })
		{
		}

		internal PropertyCollection(IEnumerable<DefinitionNode> nodes)
		{
			_nodes = nodes;
			var propertyConfigurations = nodes.Select(node => node.Type.Configuration.Properties.Values
				.Where(property => property.SupportsBatchUpdate || nodes.Count() == 1)).Intersection();
			
			propertyConfigurations.ForEach(propertyConfiguration => _nodeProperties.Add(propertyConfiguration.MemberId, 
				new NodeProperty(_nodes, propertyConfiguration)));
		}
		
		public IEnumerator<NodeProperty> GetEnumerator()
		{
			return _nodeProperties.Values.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return _nodeProperties.Values.GetEnumerator();
		}
	}
}
