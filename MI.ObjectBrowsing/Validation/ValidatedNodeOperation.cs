﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Core;

namespace MI.Framework.ObjectBrowsing.Validation
{
	/// <summary>
	/// A validated operation performed against one or more nodes.
	/// </summary>
	/// <remarks>
	/// Each type of node may have zero or more configured validators (see the <see cref="INodeValidator"/> interface and 
	/// <see cref="Mapping.DefinitionNodeMapping{TTarget, TNode}.ValidateWith(INodeValidator)"/> method) that are each triggered 
	/// on a configured set of trigger conditions (see <see cref="NodeValidationTriggerCondition"/>) declared by each validator.
	/// The <see cref="ValidatedNodeOperation"/> class determines the most efficient plan of execution of validators against
	/// nodes, based on a given type of node operation performed against a given set of nodes.
	/// </remarks>
	public class ValidatedNodeOperation : IDisposable
	{
		/// <summary>
		/// Listens to node activity in order to determine what validation trigger events have occurred.
		/// </summary>
		private class ValidationActivityListener : NodeActivityListener
		{
			/// <summary>
			/// The logged set of events that may trigger node validators to be run.
			/// </summary>
			public IEnumerable<NodeValidationTriggerEvent> TriggerEvents => _triggerEvents.Values.SelectMany(eventList => eventList);
			private IDictionary<BrowserNode, ISet<NodeValidationTriggerEvent>> _triggerEvents 
				= new Dictionary<BrowserNode, ISet<NodeValidationTriggerEvent>>();

			/// <summary>
			/// Adds an event to the event list.
			/// </summary>
			/// <param name="triggerEvent">The event to add.</param>
			private void AddEvent(NodeValidationTriggerEvent triggerEvent)
			{
				if (!_triggerEvents.TryGetValue(triggerEvent.Node, out var eventList))
				{
					_triggerEvents.Add(triggerEvent.Node, eventList = new HashSet<NodeValidationTriggerEvent>());
				}

				eventList.Add(triggerEvent);
			}

			/// <summary>
			/// Removes all events for a node.
			/// </summary>
			/// <param name="node">The node for which to remove all events.</param>
			private void RemoveAllEventsForNode(BrowserNode node)
			{
				_triggerEvents.Remove(node);
			}

			/// <summary>
			/// Invoked when a new node is added.
			/// </summary>
			/// <param name="newNode">The newly added node.</param>
			public override void NodeCreated(BrowserNode newNode)
			{
				AddEvent(new NodeValidationTriggerEvent(NodeValidationTriggerEventType.AddedNew, newNode, 0));
				foreach (var ascendant in newNode.Ascendants.Where(n => !n.IsRootNode))
				{
					AddEvent(new NodeValidationTriggerEvent(NodeValidationTriggerEventType.DescendantAdded, ascendant, newNode.Depth - ascendant.Depth));
				}
			}

			/// <summary>
			/// Invoked when a node is deleted.
			/// </summary>
			/// <param name="deletedNode">The deleted node.</param>
			/// <param name="deletedFrom">The folder from which the node was deleted.</param>
			public override void NodeDeleted(BrowserNode deletedNode, BrowserFolder deletedFrom, bool permanentlyDeleted)
			{
				// Remove all previous trigger events logged for the deleted node.  No need to validate deleted nodes.
				RemoveAllEventsForNode(deletedNode);

				// Add the descendant removed event for the ascendant nodes except for a root node.  No validators are for root nodes.
				foreach (var ascendant in deletedNode.Ascendants.Where(n => !n.IsRootNode))
				{
					AddEvent(new NodeValidationTriggerEvent(NodeValidationTriggerEventType.DescendantRemoved, ascendant, deletedNode.Depth - ascendant.Depth));
				}
			}

			/// <summary>
			/// Invoked when a node is moved between folders.
			/// </summary>
			/// <param name="movedNode">The moved node.</param>
			/// <param name="movedFrom">The folder from which the node was moved.</param>
			/// <param name="movedTo">The foler to which the node was moved.</param>
			public override void NodeMoved(BrowserNode movedNode, BrowserFolder movedFrom, BrowserFolder movedTo)
			{
				AddEvent(new NodeValidationTriggerEvent(NodeValidationTriggerEventType.PathChanged, movedNode, 0));
				foreach (var descendant in movedNode.Descendants)
				{
					AddEvent(new NodeValidationTriggerEvent(NodeValidationTriggerEventType.PathChanged, descendant, descendant.Depth - movedNode.Depth));
				}

				foreach (var ascendant in movedTo.Parent.Ascendants.Where(n => !n.IsRootNode))
				{
					AddEvent(new NodeValidationTriggerEvent(NodeValidationTriggerEventType.DescendantAdded, ascendant,
						movedTo.Parent.Depth - ascendant.Depth));
				}

				foreach (var ascendant in movedFrom.Parent.Ascendants.Where(n => !n.IsRootNode))
				{
					AddEvent(new NodeValidationTriggerEvent(NodeValidationTriggerEventType.DescendantRemoved, ascendant, 
						movedFrom.Parent.Depth - ascendant.Depth));
				}
			}
		}

		/// <summary>
		/// Indicates whether the validated operation is disposed.
		/// </summary>
		private bool _isDisposed;

		/// <summary>
		/// Node activity listener for recording validation trigger operations.
		/// </summary>
		private ValidationActivityListener _nodeActivityListener;

		/// <summary>
		/// Initialises a new instance of the <see cref="ValidatedNodeOperation"/> class.
		/// </summary>
		public ValidatedNodeOperation()
		{
			_nodeActivityListener = NodeActivityLog.AddListener<ValidationActivityListener>();
		}

		/// <summary>
		/// Disposes of the validated node operation.
		/// </summary>
		/// <param name="disposing"><c>true</c> if invoked by the <see cref="Dispose"/> method; otherwise <c>false</c>.</param>
		protected virtual void Dispose(bool disposing)
		{
			if (disposing && !_isDisposed)
			{
				// Index the trigger events by node type then event type, then depth
				var triggerEvents = _nodeActivityListener.TriggerEvents.GroupBy(evt => evt.Node.Type)
					.ToDictionary(g => g.Key, g => g.GroupBy(evt => evt.EventType)
					.ToDictionary(g2 => g2.Key, g2 => g2.ToLookup(evt => evt.Depth)));
				
				// Dispose of the node activity listener
				_nodeActivityListener.Dispose();
				_nodeActivityListener = null;

				var stopwatch = new System.Diagnostics.Stopwatch();
				stopwatch.Start();

				// Go through all the configured validators for all the registered types and evaluate their trigger conditions
				// against the set of trigger operations recorded by the node activity listener.
				var executionPlan = new Dictionary<INodeValidator, ISet<BrowserNode>>();
				foreach (var nodeType in NodeType.RegisteredTypes)
				{
					foreach (var validator in nodeType.Configuration.Validators)
					{
						if (!executionPlan.TryGetValue(validator, out var runList))
						{
							executionPlan.Add(validator, runList = new HashSet<BrowserNode>());
						}

						if (triggerEvents.TryGetValue(nodeType, out var eventsForNodeTypeByEventType))
						{
							var nodesRequiringValidation = validator.TriggerCondition.Evaluate(eventsForNodeTypeByEventType);
							foreach (var node in nodesRequiringValidation)
							{
								runList.Add(node);
							}
						}
					}
				}

				Console.WriteLine("Building the validation execution plan took {0} seconds.", stopwatch.Elapsed.TotalSeconds);
				stopwatch.Restart();

				// Execute the execution plan
				executionPlan.ForEach(kvp => kvp.Value.ForEach(n => kvp.Key.ValidateNode(n)));
				Console.WriteLine("Executing the validation execution plan took {0} seconds.", stopwatch.Elapsed.TotalSeconds);
				stopwatch.Stop();
			}

			_isDisposed = true;
		}
		
		/// <summary>
		/// Disposes of the validated node operation.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
		}
	}
}
