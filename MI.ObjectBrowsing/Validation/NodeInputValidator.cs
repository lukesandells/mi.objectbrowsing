﻿namespace MI.Framework.ObjectBrowsing.Validation
{
	/// <summary>
	/// Validates a node value for a property.
	/// </summary>
	/// <typeparam name="TDefinitionNode">The type of node to validate.</typeparam>
	public class NodeInputValidator<TDefinitionNode> : InputValidator<TDefinitionNode, TDefinitionNode>
	{
		/// <summary>
		/// Initialises a new instance of the <see cref="NodeInputValidator{TDefinitionNode}"/> class.
		/// </summary>
		/// <param name="predicate">A custom input validation predicate.</param>
		public NodeInputValidator(InputValidationPredicate<TDefinitionNode> predicate)
			: base(predicate)
		{
		}

		/// <summary>
		/// Validates the given input node.
		/// </summary>
		/// <param name="property">The property to receive the input.</param>
		/// <param name="input">The input node to be validated.</param>
		/// <param name="validated">If the input is valid, the validated input; otherwise <c>null</c>.</param>
		/// <param name="displayMessage">If the input is invalid, a display message describing the reason.</param>
		/// <returns>The validated input cast to the appropriate type.</returns>
		protected override bool ValidateInput(NodeProperty property, TDefinitionNode input, out TDefinitionNode validated, out string displayMessage)
		{
			validated = input;
			displayMessage = null;
			return true;
		}
	}
}
