﻿using System;

namespace MI.Framework.ObjectBrowsing.Validation
{
	/// <summary>
	/// Validates an input as numeric.
	/// </summary>
	public class NumericInputValidator<TNumeric> : InputValidator<string, TNumeric>
		where TNumeric : struct, IComparable<TNumeric>
	{
		/// <summary>
		/// The minimum allowable value for the input.
		/// </summary>
		private TNumeric? _minValue;

		/// <summary>
		/// The maximum allowable value for the input.
		/// </summary>
		private TNumeric? _maxValue;

		/// <summary>
		/// Initialises a new instance of the <see cref="NumericInputValidator{TNumeric}"/> class.
		/// </summary>
		public NumericInputValidator()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="NumericInputValidator{TNumeric}"/> class.
		/// </summary>
		/// <param name="predicate">A custom input validation predicate.</param>
		public NumericInputValidator(InputValidationPredicate<TNumeric> predicate)
			: base(predicate)
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="NumericInputValidator{TNumeric}"/> class.
		/// </summary>
		/// <param name="minValue">The minimum allowable value for the input.</param>
		/// <param name="maxValue">The maximum allowable value for the input.</param>
		public NumericInputValidator(TNumeric minValue, TNumeric maxValue)
			: this(minValue, maxValue, null)
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="NumericInputValidator{TNumeric}"/> class.
		/// </summary>
		/// <param name="minValue">The minimum allowable value for the input.</param>
		/// <param name="maxValue">The maximum allowable value for the input.</param>
		/// <param name="predicate">A custom input validation predicate.</param>
		public NumericInputValidator(TNumeric minValue, TNumeric maxValue, InputValidationPredicate<TNumeric> predicate)
			: base(predicate)
		{
			_minValue = minValue;
			_maxValue = maxValue;
		}

		/// <summary>
		/// Validates the given input string.
		/// </summary>
		/// <param name="property">The property to receive the input.</param>
		/// <param name="input">The input string to be validated.</param>
		/// <param name="validated">If the input is valid, the validated input; otherwise <c>null</c>.</param>
		/// <param name="displayMessage">If the input is invalid, a display message describing the reason.</param>
		/// <returns>The validated input cast to the appropriate type.</returns>
		protected override bool ValidateInput(NodeProperty property, string input, out TNumeric validated, out string displayMessage)
		{
			// Attempt to convert to the target type
			TNumeric converted;
			validated = default(TNumeric);
			try
			{
				converted = (TNumeric)Convert.ChangeType(input, typeof(TNumeric));
			}
			catch (InvalidCastException)
			{
				displayMessage = ValidationMessage.NotAnInteger;
				return false;
			}
			catch (OverflowException)
			{
				displayMessage = ValidationMessage.ValueOutOfRange;
				return false;
			}

			// Check the min/max ranges
			if (_minValue.HasValue && _maxValue.HasValue)
			{
				if (_minValue.Value.CompareTo(converted) < 0 || _maxValue.Value.CompareTo(converted) > 0)
				{
					displayMessage = string.Format(ValidationMessage.ValueOutOfExplicitRange, _minValue, _maxValue);
					return false;
				}
			}

			validated = converted;
			displayMessage = null;
			return true;
		}
	}
}
