﻿using System;

namespace MI.Framework.ObjectBrowsing.Validation
{
	/// <summary>
	/// Validates an input as an integer.
	/// </summary>
	public class IntegerInputValidator<TInteger> : NumericInputValidator<TInteger>
		where TInteger : struct, IComparable<TInteger>
	{
		/// <summary>
		/// Initialises a new instance of the <see cref="IntegerInputValidator{TInteger}"/> class.
		/// </summary>
		public IntegerInputValidator()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="IntegerInputValidator{TInteger}"/> class.
		/// </summary>
		/// <param name="minValue">The minimum allowable value for the input.</param>
		/// <param name="maxValue">The maximum allowable value for the input.</param>
		public IntegerInputValidator(TInteger minValue, TInteger maxValue)
			: base(minValue, maxValue)
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="IntegerInputValidator{TInteger}"/> class.
		/// </summary>
		/// <param name="predicate">A custom input validation predicate.</param>
		public IntegerInputValidator(InputValidationPredicate<TInteger> predicate)
			: base(predicate)
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="IntegerInputValidator{TInteger}"/> class.
		/// </summary>
		/// <param name="minValue">The minimum allowable value for the input.</param>
		/// <param name="maxValue">The maximum allowable value for the input.</param>
		/// <param name="predicate">A custom input validation predicate.</param>
		public IntegerInputValidator(TInteger minValue, TInteger maxValue, InputValidationPredicate<TInteger> predicate)
			: base(minValue, maxValue, predicate)
		{
		}

		/// <summary>
		/// Validates the given input string.
		/// </summary>
		/// <param name="property">The property to receive the input.</param>
		/// <param name="input">The input string to be validated.</param>
		/// <param name="validated">If the input is valid, the validated input; otherwise <c>null</c>.</param>
		/// <param name="displayMessage">If the input is invalid, a display message describing the reason.</param>
		/// <returns>The validated input cast to the appropriate type.</returns>
		protected override bool ValidateInput(NodeProperty property, string input, out TInteger validated, out string displayMessage)
		{
			if (base.ValidateInput(property, input, out validated, out displayMessage))
			{
				// If the input contains a decimal place, then it is not an integer
				if (input.Contains("."))
				{
					displayMessage = ValidationMessage.NotAnInteger;
					return false;
				}
			}

			return true;
		}
	}
}
