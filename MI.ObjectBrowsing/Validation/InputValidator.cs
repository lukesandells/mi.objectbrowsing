﻿using MI.Core.Validation;

namespace MI.Framework.ObjectBrowsing.Validation
{
	/// <summary>
	/// Base class for input validators.
	/// </summary>
	/// <typeparam name="TInput">The type of the input provided to the validator.</typeparam>
	/// <typeparam name="TProperty">The type of the property to receive the input.</typeparam>
	/// <remarks>
	/// This base class performs argument validation and provides a strongly typed out parameter for derived classes.
	/// </remarks>
	public abstract class InputValidator<TInput, TProperty> : IInputValidator
	{
		/// <summary>
		/// The custom input validation predicate.
		/// </summary>
		private InputValidationPredicate<TProperty> _predicate;

		/// <summary>
		/// Initialises a new instance of the <see cref="InputValidator{TInput, TProperty}"/> class.
		/// </summary>
		protected InputValidator()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="InputValidator{TInput, TProperty}"/> class.
		/// </summary>
		/// <param name="predicate">A custom validation predicate.</param>
		protected InputValidator(InputValidationPredicate<TProperty> predicate)
		{
			_predicate = predicate;
		}

		/// <summary>
		/// Validates the given input value.
		/// </summary>
		/// <param name="property">The property to receive the input.</param>
		/// <param name="input">The input value to be validated.</param>
		/// <param name="validated">If the input is valid, the validated input as an instance of the appropriate type; otherwise <c>null</c>.</param>
		/// <param name="displayMessage">If the input is invalid, a display message describing the reason.</param>
		/// <returns>The validated input cast to the appropriate type.</returns>
		public bool ValidateInput(NodeProperty property, object input, out object validated, out string displayMessage)
		{
			property = ArgumentValidation.AssertNotNull(property, nameof(property));
			var isValid = ValidateInput(property, (TInput)input, out TProperty validatedInput, out displayMessage);
			if (isValid && _predicate != null)
			{
				isValid = _predicate(validatedInput, out displayMessage);
			}

			validated = validatedInput;
			return isValid;
		}

		/// <summary>
		/// Validates the given input string.
		/// </summary>
		/// <param name="property">The property to receive the input.</param>
		/// <param name="input">The input string to be validated.</param>
		/// <param name="validated">If the input is valid, the validated input; otherwise <c>null</c>.</param>
		/// <param name="displayMessage">If the input is invalid, a display message describing the reason.</param>
		/// <returns>The validated input cast to the appropriate type.</returns>
		protected abstract bool ValidateInput(NodeProperty property, TInput input, out TProperty validated, out string displayMessage);
	}
}
