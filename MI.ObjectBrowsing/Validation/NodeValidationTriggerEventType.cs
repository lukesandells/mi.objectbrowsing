﻿namespace MI.Framework.ObjectBrowsing.Validation
{
	/// <summary>
	/// Identifies a type of event that may trigger node validation.
	/// </summary>
	public enum NodeValidationTriggerEventType
	{
		/// <summary>
		/// The node was added as a new child/descendant node.
		/// </summary>
		AddedNew,

		/// <summary>
		/// A descendant of the node was added.
		/// </summary>
		DescendantAdded,

		/// <summary>
		/// A descendant of the node was removed.
		/// </summary>
		DescendantRemoved,

		/// <summary>
		/// The node's path changed as a result of it or one of its ascendants being moved.
		/// </summary>
		PathChanged
	}
}
