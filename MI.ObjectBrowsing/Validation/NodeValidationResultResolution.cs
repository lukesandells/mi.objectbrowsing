﻿using System;
using System.Linq;
using MI.Core.Validation;

namespace MI.Framework.ObjectBrowsing.Validation
{
	/// <summary>
	/// A resolution to a node validation result.
	/// </summary>
	public class NodeValidationResultResolution
	{
		/// <summary>
		/// The resolution of ignoring the validation result.
		/// </summary>
		/// <remarks>
		/// The <see cref="IgnoreValidationResult"/> resolution does nothing to resolve the validation error.  Therefore when
		/// the resolution is passed to the <see cref="NodeValidationResult.Resolve(NodeValidationResultResolution)"/> method,
		/// nothing is done and the validation result is cleared.
		/// </remarks>
		public static NodeValidationResultResolution IgnoreValidationResult = new NodeValidationResultResolution(ResolutionMessage.Ignore, node => { });

		/// <summary>
		/// A description for the resolution.
		/// </summary>
		public string DisplayMessage => _displayMessage;
		private string _displayMessage;

		/// <summary>
		/// The action to apply the resolution.
		/// </summary>
		private Action<BrowserNode> _resolutionAction;

		/// <summary>
		/// Initialises a new instance of the <see cref="NodeValidationResultResolution"/> class.
		/// </summary>
		/// <param name="displayMessage">The display message describing the resolution.</param>
		/// <param name="resolutionAction">The action to be undertaken to apply the resolution.</param>
		internal NodeValidationResultResolution(string displayMessage, Action<BrowserNode> resolutionAction)
		{
			_displayMessage = ArgumentValidation.AssertNotNullOrEmpty(displayMessage, nameof(displayMessage));
			_resolutionAction = ArgumentValidation.AssertNotNull(resolutionAction, nameof(resolutionAction));
		}

		/// <summary>
		/// Applies the resolution to the given node.
		/// </summary>
		/// <param name="node">The node to which to apply the resolution.</param>
		internal void ApplyTo(BrowserNode node)
		{
			node = ArgumentValidation.AssertNotNull(node, nameof(node));
			if (!node.ValidationResults.Any(result => result.SourceNode == node && result.PossibleResolutions.Contains(this)))
			{
				// The given node has no validation result for which this resolution is available
				throw new ArgumentException(string.Format(ExceptionMessage.NodeHasNoConfiguredValidationResultForResolution, 
					node.Type.DisplayName, node.DisplayId, DisplayMessage), nameof(node));
			}

			_resolutionAction(node);
		}
	}
}
