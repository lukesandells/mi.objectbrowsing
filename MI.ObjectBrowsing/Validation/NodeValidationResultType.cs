﻿using System;
using System.Collections.Generic;
using MI.Core;
using MI.Core.Validation;

namespace MI.Framework.ObjectBrowsing.Validation
{
	/// <summary>
	/// A type of validation result.
	/// </summary>
	public abstract class NodeValidationResultType
	{
		/// <summary>
		/// Lookup of declared validation result types.
		/// </summary>
		internal static IReadOnlyDictionary<string, NodeValidationResultType> DeclaredTypes => _declaredTypes;
		private static Dictionary<string, NodeValidationResultType> _declaredTypes = new Dictionary<string, NodeValidationResultType>();

		/// <summary>
		/// The severity of the validation result.
		/// </summary>
		public abstract NodeValidationResultSeverity Severity { get; }

		/// <summary>
		/// Suggests possible resolutions for the type of validation result.
		/// </summary>
		public abstract IEnumerable<NodeValidationResultResolution> SuggestResolutions(NodeValidationResult validationResult);

		/// <summary>
		/// Parses a string to get a <see cref="NodeValidationResultType"/> object.
		/// </summary>
		/// <param name="s">The string to parse.</param>
		/// <returns>The resultant <see cref="NodeValidationResultType"/> object.</returns>
		public static NodeValidationResultType Parse(string s)
		{
			return DeclaredTypes[s];
		}

		/// <summary>
		/// Registers a new validation result type.
		/// </summary>
		/// <param name="resultTypeType">The type of node validation result type. Must be a type deriving from <see cref="NodeValidationResultType"/>.</param>
		internal static void Register(Type resultTypeType)
		{
			resultTypeType = ArgumentValidation.AssertNotNull(resultTypeType, nameof(resultTypeType));
			if (!resultTypeType.DerivesFrom(typeof(NodeValidationResultType)))
			{
				// Result type type must derive from the NodeValidationResultType class.
				throw new ArgumentException(string.Format(ExceptionMessage.InvalidValidationResultTypeType, resultTypeType.FullName), 
					nameof(resultTypeType));
			}

			if (!DeclaredTypes.ContainsKey(resultTypeType.Name))
			{
				_declaredTypes.Add(resultTypeType.Name, (NodeValidationResultType)Activator.CreateInstance(resultTypeType));
			}
		}
	}
}
