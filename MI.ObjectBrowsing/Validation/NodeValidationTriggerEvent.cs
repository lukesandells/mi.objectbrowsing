﻿namespace MI.Framework.ObjectBrowsing.Validation
{
	/// <summary>
	/// An event that may trigger node validation in accordance with the trigger conditions declared by each validator.
	/// </summary>
	public class NodeValidationTriggerEvent
	{
		/// <summary>
		/// The type of validation trigger event.
		/// </summary>
		public NodeValidationTriggerEventType EventType => _eventType;
		private readonly NodeValidationTriggerEventType _eventType;

		/// <summary>
		/// The node for which the event was raised.
		/// </summary>
		public BrowserNode Node => _node;
		private readonly BrowserNode _node;

		/// <summary>
		/// The depth of the node against which the operation occurred that triggered the event, relative to the
		/// depth of the node for which the event was raised.
		/// </summary>
		public int Depth => _depth;
		private int _depth;

		/// <summary>
		/// Initialises a new instance of the <see cref="NodeValidationTriggerEvent"/> class.
		/// </summary>
		/// <param name="eventType">The type of validation trigger event.</param>
		/// <param name="node">The node for which the event is raised.</param>
		/// <param name="depth">The depth of the node against which the operation occurred that triggered the event, relative to the
		/// depth of the node for which the event is raised.</param>
		internal NodeValidationTriggerEvent(NodeValidationTriggerEventType eventType, BrowserNode node, int depth)
		{
			_eventType = eventType;
			_node = node;
			_depth = depth;
		}

		/// <summary>
		/// Determines whether the event equals a given object.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns><c>true</c> if the event equals the given object; otherwise <c>false</c>.</returns>
		public override bool Equals(object obj)
		{
			// If parameter is null return false.
			if (obj == null)
			{
				return false;
			}

			// If parameter cannot be cast to Point return false.
			NodeValidationTriggerEvent evt = obj as NodeValidationTriggerEvent;
			if ((object)evt == null)
			{
				return false;
			}

			// Return true if the fields match
			return evt._node == _node && evt._eventType == _eventType && evt._depth == _depth;
		}

		/// <summary>
		/// Returns the hash code for the event.
		/// </summary>
		/// <returns>The hash code for the event.</returns>
		public override int GetHashCode()
		{
			return (_node, _eventType, _depth).GetHashCode();
		}

		/// <summary>
		/// Overload for the == operator.
		/// </summary>
		/// <param name="left">The left-hand operand.</param>
		/// <param name="right">The right-hand operand.</param>
		/// <returns><c>true</c> if the left and right-hand operands are equal; otherwise <c>false</c>.</returns>
		public static bool operator ==(NodeValidationTriggerEvent left, NodeValidationTriggerEvent right)
		{
			// If both are null, or both are same instance, return true.
			if (ReferenceEquals(left, right))
			{
				return true;
			}

			// If one is null, but not both, return false.
			if (((object)left == null) || ((object)right == null))
			{
				return false;
			}

			// Return true if the fields match
			return left._node == right._node && left._eventType == right._eventType && left._depth == right._depth;
		}

		/// <summary>
		/// Overload for the != operator.
		/// </summary>
		/// <param name="left">The left-hand operand.</param>
		/// <param name="right">The right-hand operand.</param>
		/// <returns><c>true</c> if the left and right-hand operands are not equal; otherwise <c>false</c>.</returns>
		public static bool operator !=(NodeValidationTriggerEvent left, NodeValidationTriggerEvent right)
		{
			return !(left == right);
		}
	}
}
