﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Core.Validation;

namespace MI.Framework.ObjectBrowsing.Validation
{
	/// <summary>
	/// A specified condition for triggering validation.
	/// </summary>
	public abstract class NodeValidationTriggerCondition : INodeValidationTriggerCondition
	{
		/// <summary>
		/// Returns the set of nodes that must be validated because they meet the validation trigger condition.
		/// </summary>
		/// <param name="triggerEvents">The set of events that may trigger node validation, indexed by event type then depth.</param>
		/// <returns>The set of nodes meeting the validation trigger condition.</returns>
		public abstract IEnumerable<BrowserNode> Evaluate(IReadOnlyDictionary<NodeValidationTriggerEventType, ILookup<int, NodeValidationTriggerEvent>> triggerEvents);

		/// <summary>
		/// Returns the logical 'or' of the given two conditions.
		/// </summary>
		/// <param name="left">The left-hand operand.</param>
		/// <param name="right">The right-hand operand.</param>
		/// <returns>The logical 'or' of the two conditions.</returns>
		public static NodeValidationTriggerCondition operator |(NodeValidationTriggerCondition left, NodeValidationTriggerCondition right)
		{
			left = ArgumentValidation.AssertNotNull(left, nameof(left));
			right = ArgumentValidation.AssertNotNull(right, nameof(right));
			return CompoundNodeValidationTriggerCondition.Combine(left, right);
		}

		/// <summary>
		/// Evaluates whether the given condition is true.
		/// </summary>
		/// <param name="condition">The condition to evaluate.</param>
		/// <returns>Always returns <c>false</c>.</returns>
		/// <remarks>
		/// C# requires that the true operator must be overloaded in order for the logical '||' operator to be overloaded.  Always
		/// returns <c>false</c> to ensure that all logical '||' operands are executed.
		/// </remarks>
		public static bool operator true(NodeValidationTriggerCondition condition)
		{
			return false;
		}

		/// <summary>
		/// Evaluates whether the given condition is false.
		/// </summary>
		/// <param name="condition">The condition to evaluate.</param>
		/// <returns>Always returns <c>true</c>.</returns>
		/// <remarks>
		/// C# requires that the true operator must be overloaded in order for the logical '||' operator to be overloaded.  Always
		/// returns <c>true</c> to ensure that all logical '||' operands are executed.
		/// </remarks>
		public static bool operator false(NodeValidationTriggerCondition condition)
		{
			return true;
		}
		
		/// <summary>
		/// An node validation trigger condition that specifies trigger events corresponding to a single node meta-type and single trigger event type.
		/// </summary>
		private class AtomicNodeValidationTriggerCondition : NodeValidationTriggerCondition
		{
			/// <summary>
			/// The type of trigger event specified by the trigger condition.
			/// </summary>
			private NodeValidationTriggerEventType _eventType;

			/// <summary>
			/// The type of node specified by the trigger condition.
			/// </summary>
			private NodeMetaType _nodeMetaType;

			/// <summary>
			/// The required depth of the triggering node relative to the node for which validation must occur.
			/// </summary>
			private DepthRange _depthRange;

			/// <summary>
			/// Sets the event type for the trigger condition.
			/// </summary>
			/// <param name="eventType">The event type to set.</param>
			/// <returns>The updated trigger condition.</returns>
			public AtomicNodeValidationTriggerCondition SetEventType(NodeValidationTriggerEventType eventType)
			{
				_eventType = eventType;
				return this;
			}

			/// <summary>
			/// Sets the node meta-type for the trigger conition.
			/// </summary>
			/// <param name="nodeMetaType">The node meta-type to set.</param>
			/// <returns>The updated trigger condition.</returns>
			public AtomicNodeValidationTriggerCondition SetNodeMetaType(NodeMetaType nodeMetaType)
			{
				_nodeMetaType = nodeMetaType;
				return this;
			}

			/// <summary>
			/// Sets the depth range for the trigger condition.
			/// </summary>
			/// <param name="depthRange">The depth range to set.</param>
			/// <returns>The updated trigger condition.</returns>
			public AtomicNodeValidationTriggerCondition SetDepthRange(DepthRange depthRange)
			{
				_depthRange = depthRange;
				return this;
			}

			/// <summary>
			/// Returns the set of nodes that must be validated because they meet the validation trigger condition.
			/// </summary>
			/// <param name="triggerEvents">The set of events that may trigger node validation, indexed by event type then depth.</param>
			/// <returns>The set of nodes meeting the validation trigger condition.</returns>
			public override IEnumerable<BrowserNode> Evaluate(IReadOnlyDictionary<NodeValidationTriggerEventType, ILookup<int, NodeValidationTriggerEvent>> triggerEvents)
			{
				if (triggerEvents.TryGetValue(_eventType, out var triggerEventsByDepth))
				{
					foreach (var triggerEvent in triggerEventsByDepth
						.Where(grouping => _depthRange?.Contains(grouping.Key) ?? true)
						.SelectMany(grouping => grouping))
					{
						if (triggerEvent.Node.Is(_nodeMetaType))
						{
							yield return triggerEvent.Node;
						}
					}
				}
			}
		}

		/// <summary>
		/// A compound validation trigger condition, comprising one or more atomic trigger conditions.
		/// </summary>
		private class CompoundNodeValidationTriggerCondition : NodeValidationTriggerCondition
		{
			/// <summary>
			/// The atomic trigger conditions comprising the compound trigger condition.
			/// </summary>
			private ISet<NodeValidationTriggerCondition> _triggerConditions = new HashSet<NodeValidationTriggerCondition>();

			/// <summary>
			/// Combines two validation trigger conditions into a compound trigger condition.
			/// </summary>
			/// <param name="left">The left-hand operand.</param>
			/// <param name="right">The right-hand operand.</param>
			/// <returns>A compound trigger condition representing the logical 'or' of the left and right trigger conditions.</returns>
			public static CompoundNodeValidationTriggerCondition Combine(NodeValidationTriggerCondition left, NodeValidationTriggerCondition right)
			{
				var result = left as CompoundNodeValidationTriggerCondition ?? right as CompoundNodeValidationTriggerCondition;
				if (result != null)
				{
					var otherCondition = result == left ? right : left;
					if (otherCondition is CompoundNodeValidationTriggerCondition otherCompountCondition)
					{
						result._triggerConditions.UnionWith(otherCompountCondition._triggerConditions);
					}
					else
					{
						result._triggerConditions.Add(otherCondition);
					}

					return result;
				}

				result = new CompoundNodeValidationTriggerCondition();
				result._triggerConditions.Add(left);
				result._triggerConditions.Add(right);
				return result;
			}

			/// <summary>
			/// Returns the set of nodes that must be validated because they meet the validation trigger condition.
			/// </summary>
			/// <param name="triggerEvents">The set of events that may trigger node validation, indexed by event type then depth.</param>
			/// <returns>The set of nodes meeting the validation trigger condition.</returns>
			public override IEnumerable<BrowserNode> Evaluate(IReadOnlyDictionary<NodeValidationTriggerEventType, ILookup<int, NodeValidationTriggerEvent>> triggerEvents)
			{
				foreach (var triggerCondition in _triggerConditions)
				{
					foreach (var node in triggerCondition.Evaluate(triggerEvents))
					{
						yield return node;
					}
				}
			}
		}

		/// <summary>
		/// Interface for creating validation trigger conditions.
		/// </summary>
		public static class When
		{
			/// <summary>
			/// Trigger conditions for a node of a given meta-type.
			/// </summary>
			public static INodeTypeSelector<INodeOfTypeTriggerConditions> NodeOfType => 
				new NodeTypeSelector<INodeOfTypeTriggerConditions>(typeof(NodeOfTypeTriggerConditions), new AtomicNodeValidationTriggerCondition());

			/// <summary>
			/// Trigger conditions for a parent node based on events that occur on its children of a given meta-type.
			/// </summary>
			public static INodeTypeSelector<IDescendantTriggerConditions> ChildOfType =>
				new NodeTypeSelector<IDescendantTriggerConditions>(typeof(DescendantTriggerConditions), 
					new AtomicNodeValidationTriggerCondition().SetDepthRange(new DepthRange(1, 1)));

			/// <summary>
			/// Trigger conditions for an ascendant node based on events that occur on its descendants of a give meta-type
			/// and a given depth relative to the ascendant.
			/// </summary>
			public static INodeTypeSelector<IOfDepthClause> DescendantOfType =>
				new NodeTypeSelector<IOfDepthClause>(typeof(OfDepthClause), new AtomicNodeValidationTriggerCondition());
		}

		/// <summary>
		/// Clause for launching the depth selector.
		/// </summary>
		public interface IOfDepthClause
		{
			/// <summary>
			/// The descendant depth selector.
			/// </summary>
			IDepthSelector OfDepth { get; }
		}

		/// <summary>
		/// Clause for launching the depth selector.  Implementation of the <see cref="IOfDepthClause"/> interface.
		/// </summary>
		private class OfDepthClause : IOfDepthClause
		{
			/// <summary>
			/// The descendant depth selector.
			/// </summary>
			public IDepthSelector OfDepth => _ofDepth;
			private IDepthSelector _ofDepth;

			/// <summary>
			/// Initialises a new instance of the <see cref="OfDepthClause"/>
			/// </summary>
			/// <param name="triggerCondition">The trigger condition being built.</param>
			public OfDepthClause(AtomicNodeValidationTriggerCondition triggerCondition)
			{
				_ofDepth = new DepthSelector(triggerCondition);
			}
		}

		/// <summary>
		/// Selects a required depth of a descendant node upon which events occur relative to its ascendant upon which validation shall be triggered.
		/// </summary>
		public interface IDepthSelector
		{
			/// <summary>
			/// Specifies any depth.
			/// </summary>
			/// <returns>A node type selector.</returns>
			IDescendantTriggerConditions Any();

			/// <summary>
			/// Specifies a minimum depth.
			/// </summary>
			/// <param name="minDepth">The minimum depth to specify.</param>
			/// <returns>A node type selector.</returns>
			IDescendantTriggerConditions Min(int minDepth);

			/// <summary>
			/// Specifies a maximum depth.
			/// </summary>
			/// <param name="maxDepth">The maximum depth to specify.</param>
			/// <returns>A node type selector.</returns>
			IDescendantTriggerConditions Max(int maxDepth);

			/// <summary>
			/// Specifies a range of depths.
			/// </summary>
			/// <param name="minDepth">The minimum depth to specify.</param>
			/// <param name="maxDepth">The maximum depth to specify.</param>
			/// <returns>A node type selector.</returns>
			IDescendantTriggerConditions Range(int minDepth, int maxDepth);
		}

		/// <summary>
		/// Selects a required depth of a descendant node upon which events occur relative to its ascendant upon which validation shall be triggered.
		/// Implementation of the <see cref="IDepthSelector"/> interface.
		/// </summary>
		private class DepthSelector : IDepthSelector
		{
			/// <summary>
			/// The trigger condition being built.
			/// </summary>
			private readonly AtomicNodeValidationTriggerCondition _triggerCondition;

			/// <summary>
			/// Initialises a new instance of the <see cref="DepthSelector"/> class.
			/// </summary>
			/// <param name="triggerCondition">The trigger condition being built.</param>
			public DepthSelector(AtomicNodeValidationTriggerCondition triggerCondition)
			{
				_triggerCondition = triggerCondition;
			}

			/// <summary>
			/// Specifies any depth.
			/// </summary>
			/// <returns>A node type selector.</returns>
			public IDescendantTriggerConditions Any()
			{
				return new DescendantTriggerConditions(_triggerCondition.SetDepthRange(new DepthRange(null, null)));
			}

			/// <summary>
			/// Specifies a minimum depth.
			/// </summary>
			/// <param name="minDepth">The minimum depth to specify.</param>
			/// <returns>A node type selector.</returns>
			public IDescendantTriggerConditions Min(int minDepth)
			{
				return new DescendantTriggerConditions(_triggerCondition.SetDepthRange(new DepthRange(minDepth, null)));
			}

			/// <summary>
			/// Specifies a maximum depth.
			/// </summary>
			/// <param name="maxDepth">The maximum depth to specify.</param>
			/// <returns>A node type selector.</returns>
			public IDescendantTriggerConditions Max(int maxDepth)
			{
				return new DescendantTriggerConditions(_triggerCondition.SetDepthRange(new DepthRange(null, maxDepth)));
			}

			/// <summary>
			/// Specifies a range of depths.
			/// </summary>
			/// <param name="minDepth">The minimum depth to specify.</param>
			/// <param name="maxDepth">The maximum depth to specify.</param>
			/// <returns>A node type selector.</returns>
			public IDescendantTriggerConditions Range(int minDepth, int maxDepth)
			{
				return new DescendantTriggerConditions(_triggerCondition.SetDepthRange(new DepthRange(minDepth, maxDepth)));
			}
		}

		/// <summary>
		/// Defines a range of allowable descendant node depth for a triggering node relative to an ascendant for which validation must occur.
		/// </summary>
		private class DepthRange
		{
			/// <summary>
			/// The minimum allowable depth.
			/// </summary>
			private int? _minDepth;

			/// <summary>
			/// The maximum allowable depth.
			/// </summary>
			private int? _maxDepth;

			/// <summary>
			/// Initialises a new instance of the <see cref="DepthRange"/> class.
			/// </summary>
			/// <param name="minDepth">The minimum allowable depth for the new range.</param>
			/// <param name="maxDepth">The maximum allowable depth for the new range.</param>
			public DepthRange(int? minDepth, int? maxDepth)
			{
				_minDepth = minDepth;
				_maxDepth = maxDepth;
			}

			/// <summary>
			/// Determines whether the range contains a given depth.
			/// </summary>
			/// <param name="depth">The depth to evaluate whether it is in the range.</param>
			/// <returns><c>true</c> if the given depth is within the defined range; otherwise <c>false</c>.</returns>
			public bool Contains(int depth)
			{
				return (!_minDepth.HasValue || depth >= _minDepth.Value) && (!_maxDepth.HasValue || depth <= _maxDepth.Value);
			}
		}

		/// <summary>
		/// Node meta-type selector.
		/// </summary>
		/// <typeparam name="TTriggerConditionSelector">The type of trigger conditions that will be made available.</typeparam>
		public interface INodeTypeSelector<TTriggerConditionSelector>
		{
			/// <summary>
			/// Specifies a link node.
			/// </summary>
			/// <returns>The available trigger conditions.</returns>
			TTriggerConditionSelector LinkNode();

			/// <summary>
			/// Specifies a link node to a given type of definition node.
			/// </summary>
			/// <typeparam name="TDefinitionNode">The type of definition node.</typeparam>
			/// <returns>The available trigger conditions.</returns>
			TTriggerConditionSelector LinkNodeTo<TDefinitionNode>() 
				where TDefinitionNode : DefinitionNode;

			/// <summary>
			/// Specifies a link node to a given type of definition node with a given type of target object.
			/// </summary>
			/// <typeparam name="TDefinitionNode">The type of definition node.</typeparam>
			/// <typeparam name="TTargetObject">The type of target object.</typeparam>
			/// <returns>The available trigger conditions.</returns>
			TTriggerConditionSelector LinkNodeTo<TDefinitionNode, TTargetObject>() 
				where TDefinitionNode : DefinitionNode 
				where TTargetObject : class;

			/// <summary>
			/// Specifies a definition node.
			/// </summary>
			/// <returns>The available trigger conditions.</returns>
			TTriggerConditionSelector DefinitionNode();

			/// <summary>
			/// Specifies a definition node of a given type.
			/// </summary>
			/// <typeparam name="TDefinitionNode">The type of definition node.</typeparam>
			/// <returns>The available trigger conditions.</returns>
			TTriggerConditionSelector DefinitionNode<TDefinitionNode>() 
				where TDefinitionNode : DefinitionNode;

			/// <summary>
			/// Specifies a definition node of a given type with a given type of target object.
			/// </summary>
			/// <typeparam name="TDefinitionNode">The type of definition node.</typeparam>
			/// <typeparam name="TTargetObject">The type of target object.</typeparam>
			/// <returns>The available trigger conditions.</returns>
			TTriggerConditionSelector DefinitionNode<TDefinitionNode, TTargetObject>() 
				where TDefinitionNode : DefinitionNode 
				where TTargetObject : class;

			/// <summary>
			/// Specifies any type of node.
			/// </summary>
			/// <returns>The available trigger conditions.</returns>
			TTriggerConditionSelector Any();

			/// <summary>
			/// Specifies any type of node (link node or definition node) corresponding to a given type of definition node.
			/// </summary>
			/// <typeparam name="TDefinitionNode">The type of definition node.</typeparam>
			/// <returns>The available trigger conditions.</returns>
			TTriggerConditionSelector Any<TDefinitionNode>() 
				where TDefinitionNode : DefinitionNode;

			/// <summary>
			/// Specifies any type of node (link node or definition node) corresponding to a given type of definition node with a 
			/// given type of target object.
			/// </summary>
			/// <typeparam name="TDefinitionNode">The type of definition node.</typeparam>
			/// <returns>The available trigger conditions.</returns>
			TTriggerConditionSelector Any<TDefinitionNode, TTargetObject>()
				where TDefinitionNode : DefinitionNode
				where TTargetObject : class;
		}

		/// <summary>
		/// Implementation of <see cref="INodeTypeSelector{TTriggerConditionSelector}"/>.
		/// </summary>
		/// <typeparam name="TTriggerConditionSelector">The type of trigger conditions that will be made available.</typeparam>
		private class NodeTypeSelector<TTriggerConditionSelector> : INodeTypeSelector<TTriggerConditionSelector>
		{
			/// <summary>
			/// The type of trigger condition selector that will be returned from each method in this class.
			/// </summary>
			private readonly Type _triggerConditionSelectorType;

			/// <summary>
			/// The trigger condition to be built.
			/// </summary>
			private readonly AtomicNodeValidationTriggerCondition _triggerCondition;

			/// <summary>
			/// Initialises a new instance of the <see cref="NodeTypeSelector{TTriggerConditionSelector}"/> class.
			/// </summary>
			/// <param name="triggerConditionSelectorType">The type of trigger condition selector that will be returned 
			/// from each method in this class.</param>
			/// <param name="triggerCondition">The trigger condition to be built.</param>
			public NodeTypeSelector(Type triggerConditionSelectorType, AtomicNodeValidationTriggerCondition triggerCondition)
			{
				_triggerConditionSelectorType = triggerConditionSelectorType;
				_triggerCondition = triggerCondition;
			}

			/// <summary>
			/// Creates an instance of the trigger conditions selector to be returned from methods in this class.
			/// </summary>
			/// <param name="nodeMetaType">The type of node to form part of the trigger conditions created by the trigger condition selector.</param>
			/// <returns>The newly created trigger condition selector.</returns>
			private TTriggerConditionSelector CreateTriggerConditionSelector(NodeMetaType nodeMetaType)
			{
				return (TTriggerConditionSelector)Activator.CreateInstance(_triggerConditionSelectorType, _triggerCondition.SetNodeMetaType(nodeMetaType));
			}

			/// <summary>
			/// Specifies a link node.
			/// </summary>
			/// <returns>The available trigger conditions.</returns>
			public TTriggerConditionSelector LinkNode()
			{
				return CreateTriggerConditionSelector(NodeMetaType.LinkNode());
			}

			/// <summary>
			/// Specifies a link node to a given type of definition node.
			/// </summary>
			/// <typeparam name="TDefinitionNode">The type of definition node.</typeparam>
			/// <returns>The available trigger conditions.</returns>
			public TTriggerConditionSelector LinkNodeTo<TDefinitionNode>() 
				where TDefinitionNode : DefinitionNode
			{
				return CreateTriggerConditionSelector(NodeMetaType.LinkNodeTo<TDefinitionNode>());
			}

			/// <summary>
			/// Specifies a link node to a given type of definition node with a given type of target object.
			/// </summary>
			/// <typeparam name="TDefinitionNode">The type of definition node.</typeparam>
			/// <typeparam name="TTargetObject">The type of target object.</typeparam>
			/// <returns>The available trigger conditions.</returns>
			public TTriggerConditionSelector LinkNodeTo<TDefinitionNode, TTargetObject>()
				where TDefinitionNode : DefinitionNode
				where TTargetObject : class
			{
				return CreateTriggerConditionSelector(NodeMetaType.LinkNodeTo<TDefinitionNode, TTargetObject>());
			}

			/// <summary>
			/// Specifies a definition node.
			/// </summary>
			/// <returns>The available trigger conditions.</returns>
			public TTriggerConditionSelector DefinitionNode()
			{
				return CreateTriggerConditionSelector( NodeMetaType.DefinitionNode());
			}

			/// <summary>
			/// Specifies a definition node of a given type.
			/// </summary>
			/// <typeparam name="TDefinitionNode">The type of definition node.</typeparam>
			/// <returns>The available trigger conditions.</returns>
			public TTriggerConditionSelector DefinitionNode<TDefinitionNode>() 
				where TDefinitionNode : DefinitionNode
			{
				return CreateTriggerConditionSelector(NodeMetaType.DefinitionNode<TDefinitionNode>());
			}

			/// <summary>
			/// Specifies a definition node of a given type with a given type of target object.
			/// </summary>
			/// <typeparam name="TDefinitionNode">The type of definition node.</typeparam>
			/// <typeparam name="TTargetObject">The type of target object.</typeparam>
			/// <returns>The available trigger conditions.</returns>
			public TTriggerConditionSelector DefinitionNode<TDefinitionNode, TTargetObject>()
				where TDefinitionNode : DefinitionNode
				where TTargetObject : class
			{
				return CreateTriggerConditionSelector( NodeMetaType.DefinitionNode<TDefinitionNode, TTargetObject>());
			}

			/// <summary>
			/// Specifies any type of node.
			/// </summary>
			/// <returns>The available trigger conditions.</returns>
			public TTriggerConditionSelector Any()
			{
				return CreateTriggerConditionSelector(NodeMetaType.Any());
			}

			/// <summary>
			/// Specifies any type of node (link node or definition node) corresponding to a given type of definition node.
			/// </summary>
			/// <typeparam name="TDefinitionNode">The type of definition node.</typeparam>
			/// <returns>The available trigger conditions.</returns>
			public TTriggerConditionSelector Any<TDefinitionNode>() 
				where TDefinitionNode : DefinitionNode
			{
				return CreateTriggerConditionSelector(NodeMetaType.Any<TDefinitionNode>());
			}

			/// <summary>
			/// Specifies any type of node (link node or definition node) corresponding to a given type of definition node with a 
			/// given type of target object.
			/// </summary>
			/// <typeparam name="TDefinitionNode">The type of definition node.</typeparam>
			/// <returns>The available trigger conditions.</returns>
			public TTriggerConditionSelector Any<TDefinitionNode, TTargetObject>()
				where TDefinitionNode : DefinitionNode
				where TTargetObject : class
			{
				return CreateTriggerConditionSelector(NodeMetaType.Any<TDefinitionNode, TTargetObject>());
			}
		}

		/// <summary>
		/// Trigger conditions for a node of a specified type.
		/// </summary>
		public interface INodeOfTypeTriggerConditions
		{
			/// <summary>
			/// Trigger when a node of the specified type is added as new.
			/// </summary>
			NodeValidationTriggerCondition IsAddedNew { get; }

			/// <summary>
			/// Trigger when the folder path of a node of the specified type changes.
			/// </summary>
			NodeValidationTriggerCondition PathChanged { get; }
		}

		/// <summary>
		/// Trigger conditions for a node of a given type.  Implementation of <see cref="INodeOfTypeTriggerConditions"/>.
		/// </summary>
		private class NodeOfTypeTriggerConditions : INodeOfTypeTriggerConditions 
		{
			/// <summary>
			/// Trigger when a node of the specified type is added as new.
			/// </summary>
			public NodeValidationTriggerCondition IsAddedNew => _triggerCondition.SetEventType(NodeValidationTriggerEventType.AddedNew);
			
			/// <summary>
			/// Trigger when the folder path of a node of the specified type changes.
			/// </summary>
			public NodeValidationTriggerCondition PathChanged => _triggerCondition.SetEventType(NodeValidationTriggerEventType.PathChanged);
			
			/// <summary>
			/// The trigger condition being built.
			/// </summary>
			private AtomicNodeValidationTriggerCondition _triggerCondition;

			/// <summary>
			/// Initialises a new instance of the <see cref="NodeOfTypeTriggerConditions"/> class.
			/// </summary>
			/// <param name="triggerCondition">The trigger condition being built.</param>
			public NodeOfTypeTriggerConditions(AtomicNodeValidationTriggerCondition triggerCondition)
			{
				_triggerCondition = triggerCondition;
			}
		}

		/// <summary>
		/// Trigger conditions for a descendant node of a specified type and depth.
		/// </summary>
		public interface IDescendantTriggerConditions
		{
			/// <summary>
			/// Trigger for an ascendant node when a descendant node of the specified type and depth is added.
			/// </summary>
			NodeValidationTriggerCondition IsAdded { get; }

			/// <summary>
			/// Trigger for an ascendant node when a descendant node of the specified type and depth is removed.
			/// </summary>
			NodeValidationTriggerCondition IsRemoved { get; }
		}

		/// <summary>
		/// Trigger conditions for a descendant node of a specified type and depth.  Implementation of <see cref="IDescendantTriggerConditions"/>.
		/// </summary>
		private class DescendantTriggerConditions : IDescendantTriggerConditions 
		{
			/// <summary>
			/// Trigger for an ascendant node when a descendant node of the specified type and depth is added.
			/// </summary>
			public NodeValidationTriggerCondition IsAdded => _triggerCondition.SetEventType(NodeValidationTriggerEventType.DescendantAdded);
			
			/// <summary>
			/// Trigger for an ascendant node when a descendant node of the specified type and depth is removed.
			/// </summary>
			public NodeValidationTriggerCondition IsRemoved => _triggerCondition.SetEventType(NodeValidationTriggerEventType.DescendantRemoved);
			
			/// <summary>
			/// The trigger condition being built.
			/// </summary>
			private AtomicNodeValidationTriggerCondition _triggerCondition;

			/// <summary>
			/// Initialises a new instance of the <see cref="DescendantTriggerConditions"/> class.
			/// </summary>
			/// <param name="triggerCondition">The trigger condition being built.</param>
			public DescendantTriggerConditions(AtomicNodeValidationTriggerCondition triggerCondition)
			{
				_triggerCondition = triggerCondition;
			}
		}
	}
}
