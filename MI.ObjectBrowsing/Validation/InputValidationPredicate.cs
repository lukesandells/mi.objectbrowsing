﻿namespace MI.Framework.ObjectBrowsing.Validation
{
	/// <summary>
	/// Predicate function for input validation.
	/// </summary>
	/// <typeparam name="TProperty">The type of the property to receive the input.</typeparam>
	/// <param name="validationMessage">The validation result message if the input is deemed invalid; otherwise <c>null</c>.</param>
	/// <returns><c>true</c> if the input is valid; otherwise <c>false</c>.</returns>
	public delegate bool InputValidationPredicate<TProperty>(TProperty input, out string validationMessage);
}
