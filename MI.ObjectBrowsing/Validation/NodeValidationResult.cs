﻿using System;
using System.Linq;
using System.Collections.Generic;
using MI.Core;
using MI.Core.Validation;

namespace MI.Framework.ObjectBrowsing.Validation
{
	/// <summary>
	/// A result of validating a <see cref="DefinitionNode"/> object.
	/// </summary>
	public class NodeValidationResult
	{
		/// <summary>
		/// Unique identifier for the validation result.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// The display message describing the validation result.
		/// </summary>
		public string DisplayMessage => _displayMessage;
		private string _displayMessage;

		/// <summary>
		/// The type of validation result.
		/// </summary>
		public NodeValidationResultType Type => _type;
		private readonly NodeValidationResultType _type;

		/// <summary>
		/// The node that is the source of the validation result.
		/// </summary>
		public BrowserNode SourceNode => _sourceNode;
		private readonly BrowserNode _sourceNode;

		/// <summary>
		/// The set of possible resolutions for the validation result.
		/// </summary>
		public IEnumerable<NodeValidationResultResolution> PossibleResolutions => _possibleResolutions;
		private readonly IEnumerable<NodeValidationResultResolution> _possibleResolutions;

		/// <summary>
		/// Initialises a new instance of the <see cref="NodeValidationResult"/> class.
		/// </summary>
		protected NodeValidationResult()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="NodeValidationResult"/>
		/// </summary>
		/// <param name="sourceNode">The node that is the source of the new validation result.</param>
		/// <param name="displayMessage">The display message for the new validation result.</param>
		/// <param name="resultType">The type of validation result.</param>
		internal NodeValidationResult(BrowserNode sourceNode, string displayMessage, NodeValidationResultType resultType)
		{
			_sourceNode = ArgumentValidation.AssertNotNull(sourceNode, nameof(sourceNode));
			_type = ArgumentValidation.AssertNotNull(resultType, nameof(resultType));
			_displayMessage = ArgumentValidation.AssertNotNullOrEmpty(displayMessage, nameof(displayMessage));
			_possibleResolutions = Type.SuggestResolutions(this);
			if (_type.Severity != NodeValidationResultSeverity.Error)
			{
				_possibleResolutions = _possibleResolutions.Union(new[] { NodeValidationResultResolution.IgnoreValidationResult });
			}
		}

		/// <summary>
		/// Resolves the validation result using the given resolution.
		/// </summary>
		/// <param name="resolution">The resolution to apply to resolve the validation result.</param>
		public void Resolve(NodeValidationResultResolution resolution)
		{
			resolution = ArgumentValidation.AssertNotNull(resolution, nameof(resolution));
			if (!PossibleResolutions.Contains(resolution))
			{
				// The given resolution is not a possible resolution for this validation result
				throw new ArgumentException(string.Format(ExceptionMessage.ResolutionIsNotPossibleForValidationResult, 
					resolution.DisplayMessage), nameof(resolution));
			}

			resolution.ApplyTo(SourceNode);
			new[] { SourceNode }.Union(SourceNode.Ascendants).ForEach(node => node.RemoveValidationResult(this));
		}
	}
}
