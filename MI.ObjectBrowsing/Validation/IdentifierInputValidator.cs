﻿namespace MI.Framework.ObjectBrowsing.Validation
{
	/// <summary>
	/// Validates an input as a <see cref="String"/> identifier.
	/// </summary>
	/// <remarks>
	/// Identifiers do not permit spaces anywhere within the input text.
	/// </remarks>
	public class IdentifierInputValidator : InputValidator<string, string>
	{
		/// <summary>
		/// Indicates whether leading and trailing spaces shall be automatically trimmed rather than deemed invalid.
		/// </summary>
		private bool _autoTrim;

		/// <summary>
		/// Initialises a new instance of the <see cref="IdentifierInputValidator"/> class.
		/// </summary>
		/// <param name="autoTrim">Stipulates whether leading and trailing spaces shall be automatically trimmed rather than deemed invalid.</param>
		public IdentifierInputValidator(bool autoTrim)
			: this(autoTrim, null)
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="IdentifierInputValidator"/> class.
		/// </summary>
		/// <param name="autoTrim">Stipulates whether leading and trailing spaces shall be automatically trimmed rather than deemed invalid.</param>
		/// <param name="predicate">A custom input validation predicate.</param>
		public IdentifierInputValidator(bool autoTrim, InputValidationPredicate<string> predicate)
			: base(predicate)
		{
			_autoTrim = autoTrim;
		}

		/// <summary>
		/// Validates the given input string. 
		/// </summary>
		/// <param name="property">The property to receive the input.</param>
		/// <param name="input">The input string to be validated.</param>
		/// <param name="validated">If the input is valid, the validated input; otherwise <c>null</c>.</param>
		/// <param name="displayMessage">If the input is invalid, a display message describing the reason.</param>
		/// <returns>The validated input cast to the appropriate type.</returns>
		protected override bool ValidateInput(NodeProperty property, string input, out string validated, out string displayMessage)
		{
			if (_autoTrim)
			{
				input = input?.Trim();
			}

			if (string.IsNullOrEmpty(input))
			{
				validated = null;
				displayMessage = ValidationMessage.InvalidIdentifier;
				return false;
			}

			validated = input;
			displayMessage = string.Empty;
			return true;
		}
	}
}
