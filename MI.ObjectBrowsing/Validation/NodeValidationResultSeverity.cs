﻿namespace MI.Framework.ObjectBrowsing.Validation
{
	/// <summary>
	/// Describes the severity of a node validation result.
	/// </summary>
	public enum NodeValidationResultSeverity
	{
		/// <summary>
		/// Represents an outcome of validation that requires the user's attention, but does not require any action.
		/// </summary>
		Information,

		/// <summary>
		/// Represents a violation of recommended practice.  A warning validation result may be ignored, but it is recommended that it is resolved.
		/// </summary>
		Warning,

		/// <summary>
		/// Represents an issue that cannot be ignored and must be resolved before the node may be published/released.
		/// </summary>
		Error
	}
}
