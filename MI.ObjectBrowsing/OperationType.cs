﻿namespace MI.Framework.ObjectBrowsing
{
	/// <summary>
	/// Defines a type of operation that can be performed against a node.
	/// </summary>
	public class OperationType
	{
		/// <summary>
		/// The name of the operation type.
		/// </summary>
		private readonly string _name;

		/// <summary>
		/// Initialises a new instance of the <see cref="OperationType"/> class.
		/// </summary>
		/// <param name="name"></param>
		protected OperationType(string name)
		{
			_name = name;
		}

		/// <summary>
		/// Returns a string representation of the operation type.
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return _name;
		}

		/// <summary>
		/// Addition of a new definition node to another definition node or folder.
		/// </summary>
		public static OperationType AddNew { get; } = new OperationType("Add New");

		/// <summary>
		/// Addition of a link to a definition not to another definition node or folder.
		/// </summary>
		public static OperationType AddTo { get; } = new OperationType("Add To");

		/// <summary>
		/// Move of a node to another node or folder.
		/// </summary>
		public static OperationType MoveTo { get; } = new OperationType("Move To");

		/// <summary>
		/// Copy of a node to another node or folder.
		/// </summary>
		public static OperationType CopyTo { get; } = new OperationType("Copy To");

		/// <summary>
		/// Deletion of a node.
		/// </summary>
		public static OperationType Delete { get; } = new OperationType("Delete");
	}
}
