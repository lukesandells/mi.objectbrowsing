﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MI.Core;
using MI.Core.Validation;

namespace MI.Framework.ObjectBrowsing
{
	internal class NodeMover
	{
		private readonly bool _assureDestinationIds;

		/// <summary>
		/// Initialises a new instance of the <see cref="NodeMover"/> class.
		/// </summary>
		/// <param name="assureDestinationIds">Stipulates whether to check the destinations for ID availability and
		/// replace unavailable IDs with available ones.</param>
		public NodeMover(bool assureDestinationIds)
		{
			_assureDestinationIds = assureDestinationIds;
		}

		internal void Move(BrowserNode sourceNode, IValidated<BrowserFolder> folder)
		{
			Move(new[] { sourceNode }, folder);
		}

		internal void Move(IEnumerable<BrowserNode> sourceNodes, IValidated<BrowserFolder> folder)
		{
			// Get the assured available destination IDs if not moving to the recycle bin or rolling back a transaction
			var assuredAvailableDestinationIds = !_assureDestinationIds ? null : DisplayIdAssurance.AssureDestinationIds(
				sourceNodes.OfType<DefinitionNode>(), FolderPath.To(folder.Value), true, 
				nodeType => nodeType.Configuration.DisplayId.IdClashOnMoveSuffix);

			// Execute the move
			sourceNodes.OfType<DefinitionNode>().ForEach(node => MoveDefinitionNode(node, folder));
			sourceNodes.OfType<LinkNode>().ForEach(node => MoveBrowserNode(node, folder));

			// Post-process properties of moved definition nodes
			foreach (var node in sourceNodes.Union(sourceNodes.SelectMany(n => n.DescendantsOrderedByDepth)).OfType<DefinitionNode>())
			{
				if (assuredAvailableDestinationIds != null)
				{
					node.SetPropertyValue(node.Type.Configuration.DisplayId.Property.MemberId, assuredAvailableDestinationIds[node],
						refreshLinkNodes: false, evaluatePropertyForumula: false);
				}

				// Property formulae must be evaluated for all descendants because a moved nodes ascendants have changed 
				// and the ascendants may feature in the property formulae.
				node.EvaluatePropertyFormulae();
			}

			// Invoke OnMoveTo actions for all moved nodes
			sourceNodes.OfType<DefinitionNode>().ForEach(node => node.Type.Configuration.OnMoveToActions
				.Where(kvp => kvp.Key.IsAssignableFrom(folder.Value.Parent.GetType()))
				.ForEach(kvp => kvp.Value?.Invoke(node, folder.Value.Parent)));
		}

		private void MoveDefinitionNode(DefinitionNode node, IValidated<BrowserFolder> folder)
		{
			// Is the destination folder not the property links folder or recycle bin with a link to this definition node?
			if (!folder.Value.SystemManaged)
			{
				var linkToThisNode = folder.Value.Nodes.OfType<LinkNode>().SingleOrDefault(link => link.LinkedDefinitionNode == node);
				if (linkToThisNode != null)
				{
					// If so, replace the link node with this definition node by first deleting the link node
					linkToThisNode.Delete();
				}
			}
			
			// Perform a standard move
			MoveBrowserNode(node, folder);
		}

		private void MoveBrowserNode(BrowserNode node, IValidated<BrowserFolder> folder)
		{
			var originalFolder = node.ParentFolder;
			node.ParentFolder.Remove(node, false);
			folder.Value.Add(node, true);
			if (!folder.Value.IsRecycleBin)
			{
				// Only want to log a move operation if not moving to the recycle bin because 
				// moving to the recycle bin is logged as a delete operation
				NodeActivityLog.LogNodeMoved(node, originalFolder, folder.Value);
			}
		}
	}
}
