﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using MI.Core;
using MI.Core.Validation;
using MI.Framework.ObjectBrowsing.Validation;

namespace MI.Framework.ObjectBrowsing
{
	/// <summary>
	/// A node that has folders and can contain other nodes.
	/// </summary>
	public abstract class ParentNode : BrowserNode
	{
        public virtual IEnumerable<BrowserFolder> AllFolders =>
            _allFolders = _allFolders ?? MI.Core.EnumerableExtensions.ToHashSet(NonEmptyNonInverseFolders
                .Union(Type.Configuration.Folders
                    .Where(fc => !NonEmptyNonInverseFolders.Any(bf => bf.MemberId == fc.MemberId))
                    .Select(fc => BrowserFolder.Create(fc.MemberId, this))));
				//.ToHashSet();
		private ISet<BrowserFolder> _allFolders;

		public virtual IEnumerable<BrowserFolder> NonEmptyNonInverseFolders => _nonEmptyNonInverseFolders;
		private ISet<BrowserFolder> _nonEmptyNonInverseFolders = new HashSet<BrowserFolder>();

        public virtual IEnumerable<BrowserFolder> NonSystemFolders =>
            _nonSystemFolders = _nonSystemFolders ?? MI.Core.EnumerableExtensions.ToHashSet(AllFolders.Where(f => !f.SystemManaged));//.ToHashSet();
		private ISet<BrowserFolder> _nonSystemFolders;

        public virtual IEnumerable<BrowserFolder> NonEmptyNonSystemFolders => MI.Core.EnumerableExtensions.ToHashSet(NonSystemFolders.Where(f => !f.IsEmpty));//.ToHashSet();

		public virtual BrowserFolder PrimaryFolder => // Will be null if no configured primary folder
			_primaryFolder = _primaryFolder ?? AllFolders.Where(f => f.IsPrimary).SingleOrDefault() ?? null;
		private BrowserFolder _primaryFolder;

		/// <summary>
		/// The set of folders that contains nodes.
		/// </summary>
		public virtual IEnumerable<BrowserFolder> NonEmptyFolders => NonEmptyNonInverseFolders
			.OrderBy(f => f.Configuration.Ordering);

		/// <summary>
		/// The descendants of the parent node.
		/// </summary>
		public override IEnumerable<BrowserNode> Descendants => _descendants;

		/// <summary>
		/// The protected read/write descendants of the parent node.
		/// </summary>
		/// <remarks>
		/// This property exists because direct access to the backing field between different instances of the <see cref="ParentNode"/> class
		/// must be avoided because a parent node instance may be a proxy, and proxy field accesses are not propagated to the proxied object.
		/// </remarks>
		protected virtual ICollection<BrowserNode> ProtectedDescendants => _descendants;
		private ICollection<BrowserNode> _descendants = new Collection<BrowserNode>();

		/// <summary>
		/// Initialises a new instance of the <see cref="ParentNode"/> class.
		/// </summary>
		protected ParentNode()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="ParentNode"/> class.
		/// </summary>
		/// <param name="nodeTypeId">Identifies the type of node to create.</param>
		protected ParentNode(Guid nodeTypeId)
			: base(nodeTypeId)
		{
		}

		public virtual BrowserFolder FolderWithMemberId(Guid memberId)
		{
			return AllFolders.Single(f => f.MemberId == memberId);
		}

		public override bool CanAddNew(out IEnumerable<NodeType> permittedNodeTypes)
		{
			permittedNodeTypes = Type.Configuration.Folders
				.Where(fc => fc.AllowsDefinitionNodes)
				.SelectMany(fc => fc.PermittedTypes);

			return permittedNodeTypes.Count() > 0;
		}

		public override bool CanAddNew(NodeType nodeType, object[] initialisationParameters = null)
		{
			return CanAddNew(nodeType, initialisationParameters, out _);
		}

		public override bool CanAddNew(NodeType nodeType, out IEnumerable<BrowserFolder> permittedFolders)
		{
			return CanAddNew(nodeType, null, out permittedFolders);
		}

		public override bool CanAddNew(NodeType nodeType, object[] initialisationParameters, out IEnumerable<BrowserFolder> permittedFolders)
		{
			permittedFolders = NonSystemFolders
			.Where(f => f.AllowsDefinitionNodes)
			.Where(f => f.Configuration.PermittedTypes.Contains(nodeType))
			.Where(f => (f.IsInverse ? f.Configuration.InverseFolderConfigurationIn(nodeType.Configuration).PermittedTypeConfigurations[Type.Id] 
				: f.Configuration.PermittedTypeConfigurations[nodeType.Id])
					.CanAddNewPredicate?.Invoke(TargetObject, initialisationParameters ?? new object[] { }) ?? true);

			// Not okay if more than one permitted folder including primary folder because no easy way to get the user to pick between them
			if (permittedFolders.Count() > 0 && !(permittedFolders.Count() > 1 && permittedFolders.Contains(PrimaryFolder)))
			{
				return true;
			}

			permittedFolders = new BrowserFolder[] { };
			return false;
		}

		public virtual DefinitionNode AddNew(NodeType nodeType, object[] initialisationParameters = null)
		{
			return AddNew(nodeType, (string)null, initialisationParameters);
		}

		public virtual DefinitionNode AddNew(NodeType nodeType, string displayId, object[] initialisationParameters = null)
		{
			if (!CanAddNew(nodeType, initialisationParameters, out var permittedFolders) || permittedFolders.Count() > 1)
			{
				// Cannot add new node of type '{0}' to {1} node '{2}'. It is possible that more than one receiving folder is 
				// permitted, in which case a specific folder must be specified.
				nodeType = ArgumentValidation.AssertNotNull(nodeType, nameof(nodeType));
				throw new ArgumentException(string.Format(ExceptionMessage.CannotAddNewNodeOfGivenType, nodeType.DisplayName, Type.DisplayName, DisplayId),
					nameof(nodeType));
			}

			return AddNew(nodeType, permittedFolders.Single(), displayId, initialisationParameters);
		}

		public virtual DefinitionNode AddNew(NodeType nodeType, BrowserFolder folder, object[] initialisationParameters = null)
		{
			return AddNew(nodeType, folder, (string)null, initialisationParameters);
		}

		public virtual DefinitionNode AddNew(NodeType nodeType, BrowserFolder folder, string displayId, object[] initialisationParameters = null)
		{
			return AddNew(nodeType, folder, Validatable.Unvalidated(displayId), initialisationParameters);
		}

		protected internal virtual DefinitionNode AddNew(NodeType nodeType, BrowserFolder folder, IValidatable<string> displayId,
			object[] initialisationParameters)
		{
			folder = ArgumentValidation.AssertNotNull(folder, nameof(folder));
			nodeType = ArgumentValidation.AssertNotNull(nodeType, nameof(nodeType));

			// Make sure the folder is in this node
			if (folder.Parent != this)
			{
				throw new ArgumentException(string.Format(ExceptionMessage.CannotAddNewNodeToFolderBelongingToDifferentNode, folder), nameof(folder));
			}
			
			// If no display ID provided, need to generate a display ID for the object that is unique within the scoping node
			var path = FolderPath.To(folder);
			if (string.IsNullOrEmpty(displayId?.Value))
			{
				displayId = Validatable.Validated(path.NextAvailableId(nodeType, initialisationParameters));
			}
			else
			{
				// If a display ID requiring validation is provided, check that the ID is available in the new folder
				if (!displayId.IsValidated() && !path.IsIdAvailable(nodeType, displayId.Value))
				{
					// Cannot add new {0} node with display ID '{1}' to folder '{2}' of {3} node '{4}' because the ID is not available in that folder.
					throw new ArgumentException(string.Format(ExceptionMessage.DisplayIdNotAvailableForNewNode, nodeType.DisplayName, displayId,
						folder.DisplayName, Type.DisplayName, DisplayId), nameof(displayId));
				}
			}

			var newNode = nodeType.NewDefinitionNode(displayId.Value, initialisationParameters);
			AddNew(Validatable.Validated(newNode, ValidatableCondition.DisplayIdAvailable), folder);
			return newNode;
		}

		public virtual void AddNew(BrowserNode newNode, BrowserFolder folder)
		{
			AddNew(Validatable.Unvalidated(newNode), folder);
		}

		private void AddNew(IValidatable<BrowserNode> newNode, BrowserFolder folder)
		{
			// Check that the new node's display ID is available in the new folder
			newNode = ArgumentValidation.AssertNotNull(newNode, nameof(newNode));
			var path = FolderPath.To(folder);
			if (!newNode.IsValidated(ValidatableCondition.DisplayIdAvailable) && !path.IsIdAvailable(newNode.Value.Type, newNode.Value.DisplayId))
			{
				// Cannot add new {0} node with display ID '{1}' to folder '{2}' of {3} node '{4}' because the ID is not available in that folder.
				throw new ArgumentException(string.Format(ExceptionMessage.DisplayIdNotAvailableForNewNode, newNode.Value.Type.DisplayName, 
					newNode.Value.DisplayId, folder.DisplayName, Type.DisplayName, DisplayId), nameof(newNode));
			}
			
			// Add the node to the given folder.  This will also check that the new node is in fact new and doesn't
			// already belong to another folder/node.
			folder.Add(newNode.Value, true);
			// Log that the node has been added to it's parent
			NodeActivityLog.LogNodeAddedNew(newNode.Value);
			// Invoke OnInitialise event
			newNode.Value.Type.Configuration.OnInitialiseAction?.Invoke(newNode, path);
		}

		protected internal virtual void OnNodeAdded(BrowserFolder folder, BrowserNode child)
		{
			// Folder has to be added as a non-empty non-inverse folder before adding descendants because
			// the persistence framework may decide to auto-flush on adding the descendants, which means that 
			// because the child already has its parent folder set at this point, the folder must be added before 
			// the flush occurs or otherwise a foreign key constraint violation occurs.
			if (!_nonEmptyNonInverseFolders.Contains(folder))
			{
				_nonEmptyNonInverseFolders.Add(folder);
			}

			// The new child has to be added as a descendant of this node and each of this node's ascendants
			new[] { this }.Union(Ascendants).ForEach(ascendant => ascendant.ProtectedDescendants.Add(child));

			if (child.Is<ParentNode>(out var childAsParent))
			{
				// Each of the child's descendants have to be added as descendants of this node and each of this node's ascendants
				new[] { this }.Union(Ascendants)
					.ForEach(ascendant => childAsParent.Descendants
						.ForEach(descendant => ascendant.ProtectedDescendants.Add(descendant)));
			}
		}

		protected internal virtual void OnNodeRemoved(BrowserFolder folder, BrowserNode child)
		{
			// The new child has to be removed as a descendant of this node and each of this node's ascendants
			new[] { this }.Union(Ascendants).ForEach(ascendant => ascendant.ProtectedDescendants.Remove(child));
			
			if (child.Is<ParentNode>(out var childAsParent))
			{
				// Each of the child's descendants have to be removed as descendants of this node and each of this node's ascendants
				new[] { this }.Union(Ascendants)
					.ForEach(ascendant => childAsParent.Descendants
						.ForEach(descendant => ascendant.ProtectedDescendants.Remove(descendant)));
			}

			// Folder has to be removed from the non-empty non-inverse folders after adding descendants because
			// the persistence framework may decide to auto-flush on removing the descendants, which means that 
			// because the child still has its parent folder set at this point, the folder must not be removed from 
			// the non-empty non-inverse folders collection until after this flush occurs or otherwise a foreign key 
			// constraint violation occurs.
			if (folder.IsEmpty && _nonEmptyNonInverseFolders.Contains(folder))
			{
				_nonEmptyNonInverseFolders.Remove(folder);
			}
		}
	}
}
