﻿using MI.Core;
using System;
using System.Collections.Generic;

namespace MI.Framework.ObjectBrowsing
{
	public static class NodeActivityLog
	{
		private static IList<NodeActivityListener> _listeners = new List<NodeActivityListener>();

		public static void AddListener<TListener>(TListener listener)
			where TListener : NodeActivityListener
		{
			_listeners.Add(listener);
		}

		public static TListener AddListener<TListener>()
			where TListener : NodeActivityListener, new()
		{
			var listener = new TListener();
			_listeners.Add(listener);
			return listener;
		}

		public static void RemoveListener(NodeActivityListener listener)
		{
			_listeners.Remove(listener);
		}

		public static void LogNodeCreated(BrowserNode newNode)
		{
			_listeners.ForEach(listener => listener.NodeCreated(newNode));
		}

		public static void LogNodeAddedNew(BrowserNode newNode)
		{
			_listeners.ForEach(listener => listener.NodeAddedNew(newNode));
		}

		public static void LogNodeDeleted(BrowserNode deletedNode, BrowserFolder deletedFrom, bool permanentlyDeleted)
		{
			_listeners.ForEach(listener => listener.NodeDeleted(deletedNode, deletedFrom, permanentlyDeleted));
		}

		public static void LogNodeMoved(BrowserNode movedNode, BrowserFolder movedFrom, BrowserFolder movedTo)
		{
			_listeners.ForEach(listener => listener.NodeMoved(movedNode, movedFrom, movedTo));
		}

		public static void LogNodePropertySet(DefinitionNode node, Guid memberId, object formerValue)
		{
			_listeners.ForEach(listener => listener.NodePropertySet(node, memberId, formerValue));
		}
	}
}
