﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework.ObjectBrowsing.Configuration;
using MI.Core.Validation;
using MI.Core;

namespace MI.Framework.ObjectBrowsing
{
	/// <summary>
	/// A node representing the definition of an object.
	/// </summary>
	public abstract class DefinitionNode : ParentNode
	{
		public override bool IsLinkNode => false;
		public override bool IsDefinitionNode => true;
		public override bool IsRootNode => false;

		/// <summary>
		/// Gets the target object for this definition node.
		/// </summary>
		/// <remarks>
		/// If the definition node is self-referencing, then the target object is the definition node itself.
		/// </remarks>
		public override object TargetObject => _targetObject ?? this;
		private object _targetObject;

		public override string DisplayId
		{
			get
			{
				if (Type.Configuration.IsSelfReferencing)
				{
					return base.DisplayId;
				}

				if (Type.Configuration.DisplayId == null)
				{
					// No display ID configured for node type "{0}".
					throw new FrameworkException();
				}

				return (string)Properties[Type.Configuration.DisplayId.Property.MemberId].Value;
			}
			set
			{
				if (Type.Configuration.IsSelfReferencing)
				{
					base.DisplayId = ArgumentValidation.AssertNotNullOrEmpty(value, nameof(value));
					return;
				}

				if (Type.Configuration.DisplayId == null)
				{
					// No display ID configured for node type "{0}".
					throw new FrameworkException();
				}

				Properties[Type.Configuration.DisplayId.Property.MemberId].Value = value;
			}
		}

		public override string Description
		{
			get
			{
				if (Type.Configuration.IsSelfReferencing)
				{
					return base.Description;
				}

				return Type.Configuration.DescriptionProperty == null ? string.Empty
					: (string)Properties[Type.Configuration.DescriptionProperty.MemberId].Value;
			}
			set
			{
				if (Type.Configuration.IsSelfReferencing)
				{
					base.Description = ArgumentValidation.AssertNotNullOrEmpty(value, nameof(value));
					return;
				}

				if (Type.Configuration.DescriptionProperty == null)
				{
					// No description property configured for node type "{0}".
					throw new FrameworkException();
				}

				Properties[Type.Configuration.DescriptionProperty.MemberId].Value = value;
			}
		}

        public virtual IEnumerable<BrowserFolder> InverseFolders =>
            _inverseFolders = _inverseFolders ?? MI.Core.EnumerableExtensions.ToHashSet(AllFolders.Where(f => f.IsInverse));//.ToHashSet();
		private ISet<BrowserFolder> _inverseFolders;

		/// <summary>
		/// The set of folders that contains nodes.
		/// </summary>
		public override IEnumerable<BrowserFolder> NonEmptyFolders => NonEmptyNonInverseFolders
			.Union(InverseFolders.Where(invf => !invf.IsEmpty))
			.OrderBy(f => f.Configuration.Ordering);

		public virtual PropertyLinksFolder PropertyLinksFolder => _propertyLinksFolder = _propertyLinksFolder
			?? AllFolders.Where(bf => bf.IsPropertyLinksFolder).SingleOrDefault().Cast<PropertyLinksFolder>();
		private PropertyLinksFolder _propertyLinksFolder;

		public virtual IEnumerable<BrowserFolder> NonEmptyNonPropertyLinksFolders => AllFolders.Except(PropertyLinksFolder);

		public virtual IEnumerable<LinkNode> InboundLinks => _inboundLinks;
		private ISet<LinkNode> _inboundLinks = new HashSet<LinkNode>();

		public virtual IEnumerable<LinkNode> InboundInverseLinks => NonEmptyFolders.SelectMany(f => f.Nodes)
			.OfType<LinkNode>().Select(link => link.InverseLink);

		public virtual PropertyCollection Properties => _properties;
		private PropertyCollection _properties;

		/// <summary>
		/// Initialises a new instance of the <see cref="DefinitionNode"/> class.
		/// </summary>
		protected DefinitionNode()
		{
			if (NodeType.IsConfiguredFor(GetType()))
			{
				SetNodeTypeId(NodeType.For(GetType()).Id);
				_properties = new PropertyCollection(this);
			}
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="DefinitionNode"/> class.
		/// </summary>
		/// <param name="targetObject">The underlying target object for the new node.</param>
		protected DefinitionNode(object targetObject)
		{
			_targetObject = ArgumentValidation.AssertNotNull(targetObject, nameof(targetObject));
			SetNodeTypeId(NodeType.For(_targetObject.GetType()).Id);
			_properties = new PropertyCollection(this);
		}

		/// <summary>
		/// Sets the target object.
		/// </summary>
		/// <param name="targetObject">The target object to set.</param>
		/// <remarks>
		/// Can't provide a protected setter for the <see cref="TargetObject"/> property because it's overridden from 
		/// <see cref="BrowserNode"/>.  Can't define a protected setter for that property in the browser node because 
		/// not all concrete classes are able to set the property (e.g. <see cref="LinkNode"/>).
		/// </remarks>
		protected internal virtual void SetTargetObject(object targetObject)
		{
			_targetObject = ArgumentValidation.AssertNotNull(targetObject, nameof(targetObject));
			SetNodeTypeId(NodeType.For(_targetObject.GetType()).Id);
			_properties = new PropertyCollection(this);
		}

		protected internal virtual void EvaluatePropertyFormulae()
		{
			foreach (var property in Type.Configuration.Properties.Values.Where(p => p.HasFormula))
			{
				SetPropertyValue(property.MemberId, property.FormulaFunc(this), refreshLinkNodes: true, evaluatePropertyForumula: false);
			}
		}

		protected internal virtual object GetPropertyValue(Guid memberId)
		{
			// Check if there is a node mapped for the property type
			var property = Type.Configuration.Properties[memberId];
			object underlyingValue = property.Accessor.GetValue(TargetObject);
			if (property.IsNodeType)
			{
				// Yes, so there needs to be a property link node present if the property value is not null
				if (underlyingValue != null)
				{
					var propertyLinkNode = PropertyLinksFolder.Nodes.SingleOrDefault(linkNode => linkNode.FromMemberId == memberId);
					if (propertyLinkNode == null || propertyLinkNode.LinkedDefinitionNode.TargetObject != underlyingValue)
					{
						// Something gone wrong in the framework.  Expected corresponding link node to be in the property links folder.
						throw new FrameworkException();
					}

					return propertyLinkNode.LinkedDefinitionNode;
				}
			}

			return underlyingValue;
		}

		protected internal virtual void SetPropertyValue(Guid memberId, object value)
		{
			SetPropertyValue(memberId, value, refreshLinkNodes: true, evaluatePropertyForumula: true);
		}

		protected internal virtual void SetPropertyValue(Guid memberId, object value, bool refreshLinkNodes, bool evaluatePropertyForumula)
		{
			// If there is a node type configured for the type of the new value, then a 
			// definition or link node must be passed rather than the node's target object.
			DefinitionNode nodeValue = (value is BrowserNode browserNode ? 
				(browserNode.Is<LinkNode>(out var linkNode) ? linkNode.LinkedDefinitionNode : browserNode.Cast<DefinitionNode>()) : null);
			if (value != null && nodeValue == null && NodeType.IsConfiguredFor(value.GetType()))
			{
				// The given value is of a type for which there is configured type of definition node that has the type of the given value
				// as a target object. The definition node must be passed as the property value rather than the node's target object.
				throw new ArgumentException();
			}

			// Abort if no change in value
			if (Properties[memberId].Value == (nodeValue ?? value))
			{
				return;
			}

			// Are we refreshing link nodes resulting from the change in property value?
			if (refreshLinkNodes)
			{
				// We need to delete the current link node if there is one and:
				// - it points to a different node than the new one; and/or
				// - a new one wasn't provided.
				var currentLinkNode = PropertyLinksFolder.Nodes.SingleOrDefault(ln => ln.FromMemberId == memberId);
				if (currentLinkNode != null && currentLinkNode.LinkedDefinitionNode != nodeValue)
				{
					// Delete the link node, putting it in the recycle bin, and if it was the last link in the property links folder, then
					// the folder will be automatically notified that the folder is empty and it will be removed from the non-empty collection
					// If the link node corresponds to a property that has a value derived by evaluating a formula, the link node should be deleted permanently because there 
					// will never be any need to restore the link node because the property value will be automatically recalculated.
					currentLinkNode.Delete(deletePermanantly: currentLinkNode.IsPropertyLink && currentLinkNode.CorrespondingPropertyConfiguration.HasFormula && currentLinkNode.CorrespondingPropertyConfiguration.IsReadOnly, clearPropertyValue: currentLinkNode.IsPropertyLink);
				}

				// We need to add a new link node if:
				// - a definition node was provided; and
				// - it is different than the current link or there is no current link.
				if (nodeValue != null && (currentLinkNode == null || currentLinkNode.LinkedDefinitionNode != nodeValue))
				{
					// Create the new link node and add it to this node's property links folder
					var newLinkNode = nodeValue.CreateLink(PropertyLinksFolder, memberId);
				}
			}

			// Convert the given value to the target property type
			if (Type.Configuration.Properties[memberId].NodeType == null)
			{
				value = TypeConverter.Convert(value, Type.Configuration.Properties[memberId].NativeType);
			}

			Type.Configuration.Properties[memberId].Accessor.SetValue(TargetObject, nodeValue?.TargetObject ?? value);
			NodeActivityLog.LogNodePropertySet(this, memberId, nodeValue ?? value);
			if (evaluatePropertyForumula)
			{
				EvaluatePropertyFormulae();
			}
		}

		private LinkNode CreateLink(BrowserFolder folder, Guid fromMemberId)
		{
			// Check if the node should be linked as something other than itself
			var linkNodeTypeId = Type.Configuration.LinkAs?.LinkNodeType.Id ?? TypeId;
			
			// Create the link node and add it to the target node's inbound links collection
			var newLinkNode = new LinkNode(linkNodeTypeId, fromMemberId, this);
			NodeActivityLog.LogNodeCreated(newLinkNode);
			folder.Add(newLinkNode);
			_inboundLinks.Add(newLinkNode);
			NodeActivityLog.LogNodeAddedNew(newLinkNode);

			// Create the new link node and add it to the folder
			return newLinkNode;
		}

		protected internal virtual void AddInboundLink(LinkNode linkNode)
		{
			_inboundLinks.Add(linkNode);
		}

		protected internal virtual void RemoveInboundLink(LinkNode linkNode)
		{
			_inboundLinks.Remove(linkNode);
		}

		protected internal override LinkNode AddTo(IValidated<BrowserFolder> folder)
		{
			folder = ArgumentValidation.AssertNotNull(folder, nameof(folder));
			if (folder.Value.IsPropertyLinksFolder)
			{
				throw new Exception();
			}
			LinkNode linkNode;
			if (folder.Value.IsInverse)
			{
				// If adding to an inverse folder, the link node needs to be added to this definition node
				// Note: Can cast the folder's parent node to DefinitionNode because only definition nodes permit inverse folders
				linkNode = folder.Value.Parent.Cast<DefinitionNode>().AddTo(Validatable.Validated(folder.Value.InverseFolderIn(this)));
			}
			else
			{
				// If not adding to an inverse folder, the link node must be created in that folder
				linkNode = CreateLink(folder.Value, folder.Value.Configuration.MemberId);
			}

			// Invoke OnAddTo actions
			Type.Configuration.OnAddToActions.Where(kvp => kvp.Key.IsAssignableFrom(linkNode.TargetObject.GetType()))
				.ForEach(kvp => kvp.Value?.Invoke(TargetObject, linkNode.TargetObject));
			
			return linkNode;
		}

		public override bool CanCopyTo(ParentNode node, out IEnumerable<BrowserFolder> permittedFolders)
		{
			node = ArgumentValidation.AssertNotNull(node, nameof(node));
			if (base.CanCopyTo(node, out permittedFolders))
			{
				// Copying a node is equivalent to adding a new node at the destination.  Need to check whether the 
				// destination permits a new node that is initialised with the same initialisation parameters as the 
				// node being copied.
				permittedFolders = permittedFolders.Where(f => f.Configuration.PermittedTypeConfigurations[Type.Id]
					.CanAddNewPredicate?.Invoke(node.TargetObject, Type.Configuration.Initialiser.Properties
						.Select(p => Properties[p.MemberId].Value).ToArray()) ?? true);
				return permittedFolders.Count() > 0;
			}

			return false;
		}

		public override void Delete()
		{
			// If node is already deleted, then nothing to do
			if (IsDeleted)
			{
				return;
			}

			// Delete all inbound links first because links to a definition node must be deleted before the definition node can be deleted.
			// Property link nodes corresponding to read-only properties are not deleted here because the property formulae could evaluate 
			// to anything and if they evaluate to a node value, there must be a corresponding property link node in the property links folder 
			// or otherwise the property getter will fail.  Users of the object browsing library must ensure that any property formula 
			// evaluating to a node value returns null when the node is in the recycle bin.  Otherwise undeleted nodes may have inbound 
			// links in the recycle bin.  Need to invoke from a copy (ToHashSet) because deleting updates the collection being enumerated.
			MI.Core.EnumerableExtensions.ToHashSet(InboundLinks.Where(link => !link.IsPropertyLink || !link.CorrespondingPropertyConfiguration.HasFormula))//.ToHashSet()
				.ForEach(link => link.Delete());

			// Delete all child nodes.  The child nodes have to be explicitly deleted to ensure the inbound links are deleted for all
			// child definition nodes and the inverse properties/collections of linked definition nodes are cleared of their references
			// to the deleted definition nodes.  The child nodes must be deleted before their parent node so that if the node transaction
			// is rolled back, the nodes will be restored in the correct order.  Property link nodes corresponding to read-only properties
			// are not deleted here for the reason described above.  Need to invoke from a copy (ToHashSet) because deleting updates the 
			// collection being enumerated.
			MI.Core.EnumerableExtensions.ToHashSet(NonEmptyNonInverseFolders.SelectMany(folder => folder.Nodes)
				.Where(node => !node.IsLinkNode || node.Is<LinkNode>(out var link) 
					&& (!link.IsPropertyLink || !link.CorrespondingPropertyConfiguration.IsReadOnly)))
				/*.ToHashSet()*/.ForEach(node => node.Delete());

			// Delete the definition node
			base.Delete();

			// Invoke OnDelete action
			Type.Configuration.OnDeleteAction?.Invoke(this);
		}
		
		/// <summary>
		/// Returns a string representation of the node.
		/// </summary>
		/// <returns>A string representation of the node.</returns>
		public override string ToString()
		{
			return Type.DisplayName + ": " + DisplayId;
		}
	}
}
