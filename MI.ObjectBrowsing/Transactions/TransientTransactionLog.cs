﻿using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using MI.Core;

namespace MI.Framework.ObjectBrowsing.Transactions
{
	/// <summary>
	/// Log of uncommitted node transactions. See <see cref="NodeTransaction"/>.
	/// </summary>
	/// <remarks>
	/// Uncommitted node transactions are those that have been executed, but are not yet committed.  When a transaction is 
	/// rolled back or committed it is removed from the log.  If a rolled back transaction is itself rolled back, then
	/// a new uncommitted transaction representative of the original transaction is once again added to the log.
	/// </remarks>
	public class TransientTransactionLog : ITransactionLog
	{
		public virtual IQueryable<NodeTransaction> Transactions => _transactions.AsQueryable();
		private ICollection<NodeTransaction> _transactions = new List<NodeTransaction>();

		public Task Add(NodeTransaction transaction)
		{
			_transactions.Add(transaction);
            return Task.FromResult(0);
		}

		public Task ClearFinalised()
		{
			Transactions.Where(transaction => transaction.IsFinalised).ForEach(transaction => _transactions.Remove(transaction));
            return Task.FromResult(0);
		}
	}
}
