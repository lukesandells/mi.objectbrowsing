﻿using System.Linq;
using System.Threading.Tasks;

namespace MI.Framework.ObjectBrowsing.Transactions
{
	/// <summary>
	/// Log of node transactions.
	/// </summary>
	public interface ITransactionLog
	{
		/// <summary>
		/// Adds a transaction to the log.
		/// </summary>
		/// <param name="transaction">The transaction to add to the log.</param>
		Task Add(NodeTransaction transaction);

		/// <summary>
		/// Returns all node transactions in the log.
		/// </summary>
		/// <returns>All node transactions in the log.</returns>
		IQueryable<NodeTransaction> Transactions { get; }

		/// <summary>
		/// Clears all finalised transactions from the log.
		/// </summary>
		Task ClearFinalised();
	}
}
