﻿namespace MI.Framework.ObjectBrowsing.Transactions
{
	/// <summary>
	/// Node transaction log entry created to record the creation of a new node.
	/// </summary>
	public class NewNodeTransactionLogEntry : TransactionLogEntry
	{
		/// <summary>
		/// The newly created node.
		/// </summary>
		public BrowserNode NewNode => _newNode;
		private BrowserNode _newNode;

		/// <summary>
		/// Initialises a new instance of the <see cref="NewNodeTransactionLogEntry"/> class.
		/// </summary>
		protected NewNodeTransactionLogEntry()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="NewNodeTransactionLogEntry"/> class.
		/// </summary>
		/// <param name="newNode">The newly created node.</param>
		internal NewNodeTransactionLogEntry(BrowserNode newNode)
		{
			_newNode = newNode;
		}

		/// <summary>
		/// Rolls back the node operation corresponding to the log entry.
		/// </summary>
		/// <returns>The transaction log entry recording the rollback operation.</returns>
		public override TransactionLogEntry RollBack()
		{
			var rollbackLogEntry = new DeleteNodeTransactionLogEntry(_newNode, _newNode.ParentFolder);
			_newNode.Delete();
			return rollbackLogEntry;
		}
	}
}
