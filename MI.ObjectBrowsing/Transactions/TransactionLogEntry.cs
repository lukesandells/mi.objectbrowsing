﻿namespace MI.Framework.ObjectBrowsing.Transactions
{
	/// <summary>
	/// Node transaction log entry created to record an operation against a node.
	/// </summary>
	public abstract class TransactionLogEntry
	{
		/// <summary>
		/// Unique identifier for the log entry.
		/// </summary>
		public long Id { get; set; }

		/// <summary>
		/// Unique monotonically increasing number that describes the sequence of log entries within a node transaction.
		/// </summary>
		public int LogEntryNumber { get => _logEntryNumber; internal set => _logEntryNumber = value; }
		private int _logEntryNumber;

		/// <summary>
		/// The node transaction to which the log entry belongs.
		/// </summary>
		public NodeTransaction Transaction { get => _transaction; protected internal set => _transaction = value; }
		private NodeTransaction _transaction;

		/// <summary>
		/// Initialises a new instance of the <see cref="TransactionLogEntry"/> class.
		/// </summary>
		protected TransactionLogEntry()
		{
		}

		/// <summary>
		/// Rolls back the node operation corresponding to the log entry.
		/// </summary>
		/// <returns>The transaction log entry recording the rollback operation.</returns>
		public abstract TransactionLogEntry RollBack();
	}
}
