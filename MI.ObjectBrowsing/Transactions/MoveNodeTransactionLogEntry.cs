﻿using System;
using MI.Core.Validation;

namespace MI.Framework.ObjectBrowsing.Transactions
{
	/// <summary>
	/// Node transaction log entry created to record the moving of a node between two folders.
	/// </summary>
	public class MoveNodeTransactionLogEntry : TransactionLogEntry
	{
		/// <summary>
		/// The moved node.
		/// </summary>
		public BrowserNode MovedNode => _movedNode;
		private BrowserNode _movedNode;

		/// <summary>
		/// The former parent node of the moved node.
		/// </summary>
		public ParentNode SourceNode => _sourceNode;
		private ParentNode _sourceNode;

		/// <summary>
		/// Identifies the folder within the former parent node from which the node was moved.
		/// </summary>
		/// <remarks>
		/// The folder from which the node was moved is identified with the combination of the <see cref="SourceNode"/> and 
		/// <see cref="SourceFolderMemberId"/> properties rather than with a reference to the <see cref="BrowserFolder"/> object
		/// itself because empty folders are deleted from the database.  Therefore, if the log entry were to reference the browser 
		/// folder directly, a foreign key constraint would be violated in the database.
		/// </remarks>
		public Guid SourceFolderMemberId => _sourceFolderMemberId;
		private Guid _sourceFolderMemberId;

		/// <summary>
		/// The node to which the node was moved.
		/// </summary>
		public ParentNode DestinationNode => _destinationNode;
		private ParentNode _destinationNode;

		/// <summary>
		/// Identifies the folder within the node to which the node was moved.
		/// </summary>
		public Guid DestinationFolderMemberId => _destinationFolderMemberId;
		private Guid _destinationFolderMemberId;

		/// <summary>
		/// Initialises a new instance of the <see cref="MoveNodeTransactionLogEntry"/> class.
		/// </summary>
		protected MoveNodeTransactionLogEntry()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="MoveNodeTransactionLogEntry"/> class.
		/// </summary>
		/// <param name="movedNode">The moved node.</param>
		/// <param name="movedFrom">The folder from which the node was moved.</param>
		/// <param name="movedTo">The folder to which the node was moved.</param>
		internal MoveNodeTransactionLogEntry(BrowserNode movedNode, BrowserFolder movedFrom, BrowserFolder movedTo)
		{
			_movedNode = movedNode;
			_sourceNode = movedFrom.Parent;
			_sourceFolderMemberId = movedFrom.MemberId;
			_destinationNode = movedTo.Parent;
			_destinationFolderMemberId = movedTo.MemberId;
		}

		/// <summary>
		/// Rolls back the node operation corresponding to the log entry.
		/// </summary>
		/// <returns>The transaction log entry recording the rollback operation.</returns>
		public override TransactionLogEntry RollBack()
		{
			var sourceFolder = _destinationNode.FolderWithMemberId(_destinationFolderMemberId);
			var destinationFolder = _sourceNode.FolderWithMemberId(_sourceFolderMemberId);
			new NodeMover(assureDestinationIds: false).Move(_movedNode, Validatable.Validated(destinationFolder));
			return new MoveNodeTransactionLogEntry(_movedNode, sourceFolder, destinationFolder);
		}
	}
}
