﻿using System;
using System.Linq;
using MI.Core;

namespace MI.Framework.ObjectBrowsing.Transactions
{
	// Log entry for setting a node property to a primitive value.  Node values are logged through logging creation/deletion of 
	// property link nodes.
	public class SetNodePropertyTransactionLogEntry : TransactionLogEntry
	{
		public DefinitionNode Node => _node;
		private DefinitionNode _node;

		public Guid MemberId => _memberId;
		private Guid _memberId;

		public string FormerValue => _formerValue;
		private string _formerValue;

		protected SetNodePropertyTransactionLogEntry()
		{
		}

		/// <summary>
		/// Rolls back the node operation corresponding to the log entry.
		/// </summary>
		/// <returns>The transaction log entry recording the rollback operation.</returns>
		internal SetNodePropertyTransactionLogEntry(DefinitionNode node, Guid memberId, object formerValue)
		{
			_node = node;
			_memberId = memberId;

			// Check if the property has a node type.  If it's a node type, then its former value is stored in the transaction
			// using its corresponding property link node using a "new node" transaction log entry.
			if (node.Properties[memberId].Configuration.NodeType == null)
			{
				// Not a node type, so check if there is a registered property value serialiser
				if (PropertyValueSerialiser.CanSerialise(node.Properties[memberId].Configuration.NativeType))
				{
					// There is a registered serialiser, so use that to store the former value
					_formerValue = PropertyValueSerialiser.SerialiseValue(node.Properties[memberId].Value);
				}
				else
				{
					// No registered serialised, so just store the result of ToString
					_formerValue = formerValue?.ToString();
				}
			}
		}

		public override TransactionLogEntry RollBack()
		{
			var formerValue = _node.Properties[_memberId].Value;
			var propertyType = _node.Properties[_memberId].Configuration.NativeType;

			// Check if the property has a node type.  If it's a node type, then its former value is restored by rolling
			// back the "new node" transaction corresponding to the creation of the property's corresponding property link node.
			if (_node.Properties[_memberId].Configuration.NodeType == null)
			{
				// Not a node type, so check if there is a registered property value serialiser
				if (PropertyValueSerialiser.CanSerialise(_node.Properties[_memberId].Configuration.NativeType))
				{
					// There is a registered serialiser, so use that to restore the former value
					_node.Properties[_memberId].Value = PropertyValueSerialiser.DeserialiseValue(
						_node.Properties[_memberId].Configuration.NativeType, _formerValue);
				}
				else
				{
					// No registered serialised, so restore the value using the TypeConverter
					_node.Properties[_memberId].Value = TypeConverter.Convert(_formerValue, _node.Properties[_memberId].Configuration.NativeType);
				}
			}

			return new SetNodePropertyTransactionLogEntry(_node, _memberId, formerValue);
		}
	}
}
