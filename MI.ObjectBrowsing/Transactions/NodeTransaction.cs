﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Core;
using MI.Framework.ObjectBrowsing.Persistence;

namespace MI.Framework.ObjectBrowsing.Transactions
{
	/// <summary>
	/// A record of a transaction performed against a node.
	/// </summary>
	public class NodeTransaction : IDisposable
	{
		/// <summary>
		/// Node activity listener for appending transaction log entries.
		/// </summary>
		private class NodeActivityListener : ObjectBrowsing.NodeActivityListener
		{
			/// <summary>
			/// A running list of log entries by logged node.
			/// </summary>
			public IEnumerable<TransactionLogEntry> LogEntries => _logEntriesByNode.Values.SelectMany(set => set);
			private Dictionary<BrowserNode, ISet<TransactionLogEntry>> _logEntriesByNode = new Dictionary<BrowserNode, ISet<TransactionLogEntry>>();

			/// <summary>
			/// The last log entry number.
			/// </summary>
			private int lastLogEntryNumber;

			/// <summary>
			/// Adds a transaction log entry to the node activity listener.
			/// </summary>
			/// <param name="logEntry">The log entry to add.</param>
			private void AddEntry(TransactionLogEntry logEntry)
			{
				if (logEntry == null)
				{
					return;
				}

				logEntry.LogEntryNumber = lastLogEntryNumber++;
				if (!_logEntriesByNode.TryGetValue(NodeFor(logEntry), out var set))
				{
					set = new HashSet<TransactionLogEntry>();
					_logEntriesByNode.Add(NodeFor(logEntry), set);
				}

				set.Add(logEntry);
			}

			/// <summary>
			/// Removes a transaction log entry from the node activity listener.
			/// </summary>
			/// <param name="logEntry">The log entry to remove.</param>
			private void RemoveEntry(TransactionLogEntry logEntry)
			{
				_logEntriesByNode.Remove(NodeFor(logEntry));
			}

			/// <summary>
			/// Returns the logged node from a transaction log entry.
			/// </summary>
			/// <param name="logEntry">The log entry for which to return the logged node.</param>
			/// <returns>The logged node from a transaction log entry.</returns>
			private BrowserNode NodeFor(TransactionLogEntry logEntry)
			{
				return (logEntry as NewNodeTransactionLogEntry)?.NewNode
					?? (logEntry as DeleteNodeTransactionLogEntry)?.DeletedNode
					?? (logEntry as MoveNodeTransactionLogEntry)?.MovedNode
					?? (logEntry as SetNodePropertyTransactionLogEntry)?.Node;
			}

			/// <summary>
			/// Returns whether the given node has a corresponding log entry of the given type.
			/// </summary>
			/// <typeparam name="TLogEntry">The type of log entry.</typeparam>
			/// <param name="node">The logged node.</param>
			/// <returns><c>true</c> if the given node has a corresponding log entry of the given type; otherwise <c>false</c>.</returns>
			private bool NodeHasLogEntry<TLogEntry>(BrowserNode node)
				where TLogEntry : TransactionLogEntry
			{
				if (_logEntriesByNode.TryGetValue(node, out var set))
				{
					return set.OfType<TLogEntry>().Count() > 0;
				}

				return false;
			}

			/// <summary>
			/// Returns the log entries of the given type for the given node.
			/// </summary>
			/// <typeparam name="TLogEntry">The type of log entry.</typeparam>
			/// <param name="node">The logged node.</param>
			/// <returns>The log entries of the given type for the given node.</returns>
			private IEnumerable<TLogEntry> NodeLogEntries<TLogEntry>(BrowserNode node)
				where TLogEntry : TransactionLogEntry
			{
				if (_logEntriesByNode.TryGetValue(node, out var set))
				{
					return set.OfType<TLogEntry>();
				}

				return new TLogEntry[] { };
			}

			/// <summary>
			/// Invoked when a new node is added.
			/// </summary>
			/// <param name="newNode">The newly added node.</param>
			public override void NodeAddedNew(BrowserNode newNode)
			{
				// Don't want to log new property link nodes corresponding to properties defined by a formula because those link nodes
				// are automatically created and deleted in accordance with the property formula
				if (!(newNode.Is<LinkNode>(out var linkNode) && linkNode.IsPropertyLink && linkNode.CorrespondingPropertyConfiguration.HasFormula))
				{
					AddEntry(new NewNodeTransactionLogEntry(newNode));
				}
			}

			/// <summary>
			/// Invoked when a node is deleted.
			/// </summary>
			/// <param name="deletedNode">The deleted node.</param>
			/// <param name="deletedFrom">The folder from which the node was deleted.</param>
			public override void NodeDeleted(BrowserNode deletedNode, BrowserFolder deletedFrom, bool permanentlyDeleted)
			{
				// Don't want to log permanently deleted nodes.  Also don't want to log deleted property link nodes corresponding to properties 
				// defined by a formula because those link nodes are automatically created and deleted in accordance with the property formula
				if (!permanentlyDeleted && !(deletedNode.Is<LinkNode>(out var linkNode) && linkNode.IsPropertyLink 
					&& linkNode.CorrespondingPropertyConfiguration.HasFormula))
				{
					AddEntry(new DeleteNodeTransactionLogEntry(deletedNode, deletedFrom));
				}

				// If the node has been permanently deleted, we need to remove all previously logged log entries for that node
				if (permanentlyDeleted)
				{
					NodeLogEntries<TransactionLogEntry>(deletedNode).ForEach(logEntry => Current.RemoveEntry(logEntry));
				}
			}

			/// <summary>
			/// Invoked when a node is moved between folders.
			/// </summary>
			/// <param name="movedNode">The moved node.</param>
			/// <param name="movedFrom">The folder from which the node was moved.</param>
			/// <param name="movedTo">The foler to which the node was moved.</param>
			public override void NodeMoved(BrowserNode movedNode, BrowserFolder movedFrom, BrowserFolder movedTo)
			{
				AddEntry(new MoveNodeTransactionLogEntry(movedNode, movedFrom, movedTo));
			}

			/// <summary>
			/// Invoked when a property value is set on a definition node.
			/// </summary>
			/// <param name="node">The node that had its property value set.</param>
			/// <param name="memberId">The node type member ID of the set property.</param>
			/// <param name="formerValue">The former value of the property.</param>
			public override void NodePropertySet(DefinitionNode node, Guid memberId, object formerValue)
			{
				// Check if the in progress transaction has an entry for the creation of the given node.  Don't need to log 
				// property updates of a node if the transaction contains the creation of that node because if the transaction 
				// is rolled back, the node will be deleted anyway.  The propery updates can't be rolled back separately from 
				// the node creation.
				if (!NodeHasLogEntry<NewNodeTransactionLogEntry>(node))
				{
					// Only want to log property values that are not node values because setting a node value creates or deletes
					// a link node, which is logged in the transaction via the NodeCreated or NodeDeleted methods.
					if (node.Properties[memberId].Configuration.NodeType == null)
					{
						AddEntry(new SetNodePropertyTransactionLogEntry(node, memberId, formerValue));
					}
				}
			}
		}

		/// <summary>
		/// Thread synchronisation object for forcing global serialisation of node transactions.
		/// </summary>
		private static object _syncRoot = new object();

		/// <summary>
		/// The transaction log to which node transactions are logged.
		/// </summary>
		public static ITransactionLog Log { get; set; } = new TransientTransactionLog();

		/// <summary>
		/// Listener for the changes within this transaction
		/// </summary>
		public NodeChangesListener NodeChanges { get; private set; }

		/// <summary>
		/// The current in progress transaction if a transaction is in progress; otherwise <c>null</c>.
		/// </summary>
		public static NodeTransaction Current => _current;
		private static NodeTransaction _current;

		/// <summary>
		/// The last completed transaction that has not been rolled back.
		/// </summary>
		public static NodeTransaction LastCompleted => Log.Transactions
			.Where(transaction => transaction.State == NodeTransactionState.Complete || transaction.State == NodeTransactionState.Committed)
			.OrderByDescending(transaction => transaction.TransactionNumber)
			.FirstOrDefault();
		
		/// <summary>
		/// The collection of uncommitted transactions that have not been rolled back.
		/// </summary>
		public static IQueryable<NodeTransaction> Uncommitted => Log.Transactions
			.Where(transaction => transaction.State != NodeTransactionState.Committed);

		/// <summary>
		/// The list of node transaction listeners (see <see cref="NodeTransactionListener"/>).
		/// </summary>
		private static IList<NodeTransactionListener> _listeners = new List<NodeTransactionListener>();

		/// <summary>
		/// The unique identifier for the node transaction.
		/// </summary>
		public long Id { get; set; }

		/// <summary>
		/// The unique monotonically increasing transaction number for the node transaction.
		/// </summary>
		public int TransactionNumber => _transactionNumber;
		private int _transactionNumber;

		/// <summary>
		/// The list of log entries contained within the transaction.
		/// </summary>
		public IEnumerable<TransactionLogEntry> LogEntries => _logEntries.OrderBy(logEntry => logEntry.LogEntryNumber);
		private ICollection<TransactionLogEntry> _logEntries = new List<TransactionLogEntry>();

		/// <summary>
		/// The last log entry logged within the transaction.
		/// </summary>
		public TransactionLogEntry LastLogEntry => LogEntries.Last();

		/// <summary>
		/// The state of the node transaction.
		/// </summary>
		/// <remarks>
		/// All node transactions start in the <see cref="NodeTransactionState.InProgress"/> state and are marked as
		/// <see cref="NodeTransactionState.Complete"/> once the transaction is complete by invoking the 
		/// <see cref="Complete"/> method.  From this point forward, no log entries (see <see cref="TransactionLogEntry"/>) 
		/// can be added to the transaction.  A complete transaction may then be committed by invoking the 
		/// <see cref="Commit"/> or <see cref="CommitUncommitted"/> method, placing the transaction into the 
		/// <see cref="NodeTransactionState.Committed"/> state.  A transaction in any state other than 
		/// <see cref="NodeTransactionState.RolledBack"/> may be rolled back by invoking the 
		/// <see cref="RollBack"/> or <see cref="RollBackUncommitted"/> method, placing the transaction into the
		/// <see cref="NodeTransactionState.RolledBack"/> state.
		/// </remarks>
		public NodeTransactionState State => _state;
		private NodeTransactionState _state;

		/// <summary>
		/// Indicates whether the transaction was created by rolling back a transaction.
		/// </summary>
		public bool IsRollbackTransaction => _isRollbackTransaction;
		private bool _isRollbackTransaction;

		/// <summary>
		/// Indicates whether the transaction is <see cref="NodeTransactionState.Committed"/> or <see cref="NodeTransactionState.RolledBack"/>.
		/// </summary>
		public bool IsFinalised => State == NodeTransactionState.Committed || State == NodeTransactionState.RolledBack;

		/// <summary>
		/// Node activity listener for appending transaction log entries.
		/// </summary>
		private NodeActivityListener _nodeActivityListener;
		
		/// <summary>
		/// Initialises a new instance of the <see cref="NodeTransaction"/> class.
		/// </summary>
		protected NodeTransaction()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="NodeTransaction"/> class.
		/// </summary>
		/// <param name="transactionNumber">The monotonically increasing transaction number.</param>
		private NodeTransaction(int transactionNumber)
		{
			_transactionNumber = transactionNumber;
		}

		/// <summary>
		/// Begins a new transaction.
		/// </summary>
		/// <returns>The new transaction.</returns>
		public static NodeTransaction Begin()
		{
			return Begin(false);
		}

		/// <summary>
		/// Begins a new transaction.
		/// </summary>
		/// <param name="rollbackTransaction">Stipulates whether the new transaction is a rollback transaction.</param>
		/// <returns>The new transaction.</returns>
		private static NodeTransaction Begin(bool rollbackTransaction)
		{
			lock (_syncRoot)
			{
				if (Current != null)
				{
					// Node transaction already in progress.  Only one node transaction is permitted at a time.
					throw new InvalidOperationException(ExceptionMessage.NodeTransactionAlreadyInProgress);
				}

				_current = new NodeTransaction((LastCompleted?.TransactionNumber ?? 0) + 1);
			}

			if (!rollbackTransaction)
			{
				_current._nodeActivityListener = NodeActivityLog.AddListener<NodeActivityListener>();
			}

			_current._isRollbackTransaction = rollbackTransaction;
			_listeners.ForEach(listener => listener.TransactionBegun(_current));
			_current.NodeChanges = NodeActivityLog.AddListener<NodeChangesListener>();
			return _current;
		}

		/// <summary>
		/// Logs the transaction and sets the transaction as complete.
		/// </summary>
		public virtual void Complete()
		{
			if (State == NodeTransactionState.InProgress)
			{
				if (!IsRollbackTransaction)
				{
					_nodeActivityListener.LogEntries.ForEach(logEntry => AppendEntry(logEntry));
					_nodeActivityListener.Dispose();
					NodeChanges.Dispose();
					NodeChanges = null;
					_nodeActivityListener = null;
				}

				_state = NodeTransactionState.Complete;
				_current = null;
				Log.Add(this);
				NodeActivityLog.RemoveListener(_nodeActivityListener);

				_listeners.ForEach(listener => listener.TransactionCompleted(this));
			}
		}
		
		/// <summary>
		/// Commits the transaction.
		/// </summary>
		public virtual void Commit()
		{
			if (State == NodeTransactionState.InProgress)
			{
				Complete();
			}

			if (State != NodeTransactionState.Complete)
			{
				// Transaction must be completed before it can be committed
				throw new InvalidOperationException(ExceptionMessage.NodeTransactionMustBeCompletedBeforeCommitted);
			}

			_state = NodeTransactionState.Committed;
			_listeners.ForEach(listener => listener.TransactionCommitted(this));
		}

		/// <summary>
		/// Rolls back the transaction.
		/// </summary>
		/// <returns>A transaction representing the rollback, which can itself be rolled back in order to undo the rollback.</returns>
		public virtual NodeTransaction RollBack()
		{
			if (State == NodeTransactionState.RolledBack)
			{
				// Transaction already rolled back
				throw new InvalidOperationException(ExceptionMessage.NodeTransactionAlreadyRolledBack);
			}

			if (this == Current)
			{
				Complete();
			}

			if (this != LastCompleted)
			{
				// Can only roll back the in progress transaction or the last completed transaction while no other transaction is in progress
				throw new InvalidOperationException(ExceptionMessage.CannotRollBackNodeTransactionThatIsNotLastCompleted);
			}

			var lastLogEntryNumber = 0;
			var rollbackTransaction = Begin(true);
			foreach (var logEntry in LogEntries.Reverse())
			{
				var rollbackLogEntry = logEntry.RollBack();
				if (rollbackLogEntry != null)
				{
					rollbackLogEntry.LogEntryNumber = lastLogEntryNumber++;
					rollbackTransaction.AppendEntry(rollbackLogEntry);
				}
			}
			
			_state = NodeTransactionState.RolledBack;
			rollbackTransaction.Complete();
			_listeners.ForEach(listener => listener.TransactionRolledBack(this));
			return rollbackTransaction;
		}

		/// <summary>
		/// Commits all uncommitted transactions.
		/// </summary>
		public static void CommitUncommitted()
		{
			var uncommitted = Uncommitted.ToList();
			uncommitted.ForEach(transaction => transaction.Commit());
			_listeners.ForEach(listener => listener.UncommittedTransactionsCommitted(uncommitted));
		}

		/// <summary>
		/// Rolls back all uncommitted transactions.
		/// </summary>
		public static void RollBackUncommitted()
		{
			var uncommitted = Uncommitted.ToList();
			uncommitted.ForEach(transaction => transaction.RollBack());
			_listeners.ForEach(listener => listener.UncommittedTransactionsRolledBack(uncommitted));
		}

		/// <summary>
		/// Appends a log entry to the transaction.
		/// </summary>
		/// <param name="logEntry">The log entry to append.</param>
		/// <returns>The appended log entry or <c>null</c> if the given log entry did not need to be appended.</returns>
		private TransactionLogEntry AppendEntry(TransactionLogEntry logEntry)
		{
			if (State != NodeTransactionState.InProgress)
			{
				// Transaction is not in progress
				throw new InvalidOperationException(ExceptionMessage.NodeTransactionNotInProgress);
			}

			if (logEntry == null)
			{
				return null;
			}

			logEntry.Transaction = this;
			_logEntries.Add(logEntry);
			return logEntry;
		}

		/// <summary>
		/// Removes a log entry from the transaction.
		/// </summary>
		/// <param name="logEntry">The log entry to remove.</param>
		private void RemoveEntry(TransactionLogEntry logEntry)
		{
			if (State != NodeTransactionState.InProgress)
			{
				// Transaction is not in progress
				throw new InvalidOperationException(ExceptionMessage.NodeTransactionNotInProgress);
			}

			logEntry.Transaction = null;
			_logEntries.Remove(logEntry);
		}

		/// <summary>
		/// Adds a new listener to the transaction.
		/// </summary>
		/// <typeparam name="TListener">The type of listener to add.</typeparam>
		/// <returns>The new listener.</returns>
		public static TListener AddListener<TListener>()
			where TListener : NodeTransactionListener, new()
		{
			var listener = new TListener();
			_listeners.Add(listener);
			return listener;
		}

		/// <summary>
		/// Removes a listener from the transaction.
		/// </summary>
		/// <param name="listener">The listener to remove.</param>
		public static void RemoveListener(NodeTransactionListener listener)
		{
			_listeners.Remove(listener);
		}

		/// <summary>
		/// Disposes the transaction.
		/// </summary>
		/// <param name="disposing"><c>true</c> if invoked from the <see cref="IDisposable.Dispose"/> method; otherwise <c>false</c>.</param>
		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				_nodeActivityListener?.Dispose();

				NodeChanges?.Dispose();
				NodeChanges = null;

				if (State == NodeTransactionState.InProgress)
				{
					RollBack();
				}
			}

			_current = null;
		}

		/// <summary>
		/// Disposes of the transaction.  Rolls back the transaction if not complete (see <see cref="Done"/> method and <see cref="State"/> property).
		/// </summary>
		void IDisposable.Dispose()
		{
			Dispose(true);
		}
	}
}
