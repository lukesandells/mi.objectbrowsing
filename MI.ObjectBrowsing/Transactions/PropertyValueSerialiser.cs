﻿using System;
using System.Collections.Generic;
using MI.Core.Validation;

namespace MI.Framework.ObjectBrowsing.Transactions
{
	/// <summary>
	/// Serialiser for property values that are not .NET primitive types or strings.
	/// </summary>
	/// <remarks>
	/// <para>When a node property value is updated as part of a node transaction, a record of its former value must be stored in a 
	/// log entry as a <see cref="String"/> so that it may be restored if the transaction is rolled back.  The .NET Framework provides
	/// functionality to convert values of primitive types to and from strings.  However, where the type of value held by a property
	/// is not a primitive .NET type or a definition node, there must be a way to serialise the value to and from a string.</para>
	/// 
	/// <para>When creating a <see cref="SetNodePropertyTransactionLogEntry"/>, the framework checks if a serialiser has been previously
	/// registered using the <see cref="Add"/> method overloads.  If so, it uses that serialiser to store the former property value.
	/// If not, it stores the former property value by invoking the <see cref="Object.ToString"/> method.  Upon rolling back the 
	/// transaction, if a serialiser has been previously registered then that is used to deserialise the stored former value.  If 
	/// no serialiser has been registered, the framework attempts to convert the stored string value to the type configured for
	/// the property using the <see cref="TypeConverter"/> class.</para>
	/// </remarks>
	public class PropertyValueSerialiser
	{
		/// <summary>
		/// The collection of registered serialisers.
		/// </summary>
		private static IDictionary<Type, PropertyValueSerialiser> _serialisers = new Dictionary<Type, PropertyValueSerialiser>();

		/// <summary>
		/// The type of object serialised/deserialised by the serialiser.
		/// </summary>
		private Type _objectType;

		/// <summary>
		/// The serialisation function.
		/// </summary>
		private Func<object, string> _serialisationFunc;

		/// <summary>
		/// The deserialisation function.
		/// </summary>
		private Func<string, object> _deserialisationFunc;

		/// <summary>
		/// Initialises a new instance of the <see cref="PropertyValueSerialiser"/> class.
		/// </summary>
		/// <param name="type">The type of object serialised by the serialiser.</param>
		/// <param name="serialisationFunc">The function to serialise the object.</param>
		/// <param name="deserialisationFunc">The function to deserialise the object.</param>
		protected PropertyValueSerialiser(Type type, Func<object, string> serialisationFunc, Func<string, object> deserialisationFunc)
		{
			_objectType = ArgumentValidation.AssertNotNull(type, nameof(type));
			_serialisationFunc = ArgumentValidation.AssertNotNull(serialisationFunc, nameof(serialisationFunc));
			_deserialisationFunc = ArgumentValidation.AssertNotNull(deserialisationFunc, nameof(deserialisationFunc));
		}

		/// <summary>
		/// Adds a new serialiser.
		/// </summary>
		/// <param name="serialiser">The serialiser to add.</param>
		public static void Add(PropertyValueSerialiser serialiser)
		{
			serialiser = ArgumentValidation.AssertNotNull(serialiser, nameof(serialiser));
			_serialisers.Add(serialiser._objectType, serialiser);
		}

		/// <summary>
		/// Adds a new serialiser.
		/// </summary>
		/// <typeparam name="TPropertyValue">The type of object serialised by the new serialiser.</typeparam>
		/// <param name="serialisationFunc">The serialisation function.</param>
		/// <param name="deserialisationFunc">The deserialisation function.</param>
		public static void Add<TPropertyValue>(Func<TPropertyValue, string> serialisationFunc, Func<string, TPropertyValue> deserialisationFunc)
		{
			_serialisers.Add(typeof(TPropertyValue), new PropertyValueSerialiser(typeof(TPropertyValue), 
				value => serialisationFunc((TPropertyValue)value),
				value => deserialisationFunc(value)));
		}

		/// <summary>
		/// Determines whether a serialiser has been registered to serialise an object of the given type.
		/// </summary>
		/// <param name="type">The type of object for which to determine whether a serialiser has been registered.</param>
		/// <returns><c>true</c> if a serialiser has been registered for the given type; otherwise <c>false</c>.</returns>
		public static bool CanSerialise(Type type)
		{
			return _serialisers.ContainsKey(type);
		}

		/// <summary>
		/// Serialises the given property value to a <see cref="String"/>.
		/// </summary>
		/// <param name="value">The property value to serialise.</param>
		/// <returns>The serialised property value.</returns>
		protected virtual string Serialise(object value)
		{
			return _serialisers[_objectType]._serialisationFunc(value);
		}

		/// <summary>
		/// Deserialises a serialised property value.
		/// </summary>
		/// <param name="value">The serialised property value.</param>
		/// <returns>The deserialised property value.</returns>
		protected virtual object Deserialise(string value)
		{
			return _serialisers[_objectType]._deserialisationFunc(value);
		}

		/// <summary>
		/// Serialises a property value to a string.
		/// </summary>
		/// <param name="value">The property value to serialise.</param>
		/// <returns>The serialised property value.</returns>
		public static string SerialiseValue(object value)
		{
			value = ArgumentValidation.AssertNotNull(value, nameof(value));
			return _serialisers[value.GetType()].Serialise(value);
		}

		/// <summary>
		/// Deserialises a property value.
		/// </summary>
		/// <param name="objectType">The type of property value to deserialise.</param>
		/// <param name="value">The serialised property value.</param>
		/// <returns>The deserialised property value.</returns>
		public static object DeserialiseValue(Type objectType, string value)
		{
			return _serialisers[objectType].Deserialise(value);
		}

		/// <summary>
		/// Deeserialises a property value.
		/// </summary>
		/// <typeparam name="TPropertyValue">The type of property value to deserialise.</typeparam>
		/// <param name="value">The serialised property value.</param>
		/// <returns>The deserialised property value.</returns>
		public static TPropertyValue DeserialiseValue<TPropertyValue>(string value)
		{
			return (TPropertyValue)_serialisers[typeof(TPropertyValue)].Deserialise(value);
		}
	}
}
