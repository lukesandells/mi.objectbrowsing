﻿namespace MI.Framework.ObjectBrowsing.Transactions
{
	/// <summary>
	/// The state of a node transaction.
	/// </summary>
	public enum NodeTransactionState
	{
		/// <summary>
		/// Transaction is in progress.
		/// </summary>
		InProgress,

		/// <summary>
		/// Transaction is complete.
		/// </summary>
		Complete,

		/// <summary>
		/// Transaction is committed.
		/// </summary>
		Committed,

		/// <summary>
		/// Transaction is rolled back.
		/// </summary>
		RolledBack
	}
}
