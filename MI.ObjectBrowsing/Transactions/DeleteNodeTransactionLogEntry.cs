﻿using System;
using MI.Core.Validation;

namespace MI.Framework.ObjectBrowsing.Transactions
{
	/// <summary>
	/// Node transaction log entry created to record the deletion of a node.
	/// </summary>
	public class DeleteNodeTransactionLogEntry : TransactionLogEntry
	{
		/// <summary>
		/// The deleted node.
		/// </summary>
		public BrowserNode DeletedNode => _deletedNode;
		private BrowserNode _deletedNode;

		/// <summary>
		/// The parent node from which the node was deleted.
		/// </summary>
		public ParentNode DeletedFromParentNode => _deletedFromParentNode;
		private ParentNode _deletedFromParentNode;

		/// <summary>
		/// Identififes the folder within the former parent from which the node was deleted.
		/// </summary>
		/// <remarks>
		/// The folder from which the node was deleted is identified with the combination of the <see cref="DeletedFromParentNode"/>
		/// and <see cref="DeletedFromFolderMemberId"/> properties rather than with a reference to the <see cref="BrowserFolder"/> object
		/// itself because empty folders are deleted from the database.  Therefore, if the log entry were to reference the browser folder
		/// directly, a foreign key constraint would be violated in the database.
		/// </remarks>
		public Guid DeletedFromFolderMemberId => _deletedFromFolderMemberId;
		private Guid _deletedFromFolderMemberId;

		/// <summary>
		/// Initialises a new instance of the <see cref="DeleteNodeTransactionLogEntry"/> class.
		/// </summary>
		protected DeleteNodeTransactionLogEntry()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="DeleteNodeTransactionLogEntry"/> class.
		/// </summary>
		/// <param name="deletedNode">The deleted node.</param>
		/// <param name="deletedFrom">The folder from which the node was deleted.</param>
		internal DeleteNodeTransactionLogEntry(BrowserNode deletedNode, BrowserFolder deletedFrom)
		{
			_deletedNode = deletedNode;
			_deletedFromParentNode = deletedFrom.Parent;
			_deletedFromFolderMemberId = deletedFrom.MemberId;
		}

		/// <summary>
		/// Rolls back the node operation corresponding to the log entry.
		/// </summary>
		/// <returns>The transaction log entry recording the rollback operation.</returns>
		public override TransactionLogEntry RollBack()
		{
			var restoreFolder = _deletedFromParentNode.FolderWithMemberId(_deletedFromFolderMemberId);
			if (restoreFolder.IsPropertyLinksFolder)
			{
				var deletedLinkNode = (LinkNode)_deletedNode;
				_deletedFromParentNode.Cast<DefinitionNode>().Properties[deletedLinkNode.FromMemberId].Value = deletedLinkNode.LinkedDefinitionNode;
				_deletedNode.RootNode.RecycleBin.PermanentlyDelete(_deletedNode);
				return null;
			}
			else
			{
				new NodeMover(assureDestinationIds: false).Move(_deletedNode, Validatable.Validated(restoreFolder));
				return new NewNodeTransactionLogEntry(_deletedNode);
			}
		}
	}
}
