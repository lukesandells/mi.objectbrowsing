﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Core.Validation;
using MI.Framework.ObjectBrowsing.Configuration;

namespace MI.Framework.ObjectBrowsing
{
	public class LinkNode : BrowserNode
	{
		/// <summary>
		/// The definition node containing the link node.
		/// </summary>
		/// <remarks>
		/// <para>The definition node containing the link node is the link node's parent node.  However, unlike the 
		/// <see cref="BrowserNode.ParentNode"/> property of the <see cref="BrowserNode"/> class, any node containing a
		/// link node must be specifically an instance of the <see cref="DefinitionNode"/> class, as opposed to the more
		/// abstract <see cref="ParentNode"/> class.</para>
		/// </remarks>
		public virtual DefinitionNode FromDefinitionNode => IsDeleted ? null : base.ParentNode?.Cast<DefinitionNode>();
		
		/// <summary>
		/// Identifies the member in the <see cref="FromDefinitionNode"/> that contains the link.
		/// </summary>
		/// <remarks>
		/// If a primary or secondary folder holds the link, then this property identifies the folder member of the source 
		/// definition node.  However if a property links folder holds the link, this property identifies the corresponding 
		/// property member of the source definition that holds the reference to the linked definition node.
		/// </remarks>
		public virtual Guid FromMemberId => _fromMemberId;
		private Guid _fromMemberId;

		public override RootNode RootNode { get => _rootNode; protected internal set => _rootNode = value; }
		private RootNode _rootNode;

		public override bool IsLinkNode => true;
		public override bool IsDefinitionNode => false;
		public override bool IsRootNode => false;

		public virtual DefinitionNode LinkedDefinitionNode => _linkedDefinitionNode;
		private DefinitionNode _linkedDefinitionNode;

		public virtual bool IsPropertyLink => FromDefinitionNode?.Type.Configuration.Properties.ContainsKey(_fromMemberId) ?? false;

		public virtual PropertyConfiguration CorrespondingPropertyConfiguration => !IsPropertyLink ? null : FromDefinitionNode.Type.Configuration.Properties[_fromMemberId];

		public override object TargetObject => 
			LinkedDefinitionNode.Type.Configuration.LinkAs?.LinkTargetObjectFunc?.Invoke(LinkedDefinitionNode.TargetObject) 
				?? LinkedDefinitionNode.TargetObject;

		public override NodeType Type => base.Type.Configuration.LinkAs?.LinkNodeType ?? base.Type;

		public override string DisplayId { get => LinkedDefinitionNode.DisplayId; set => LinkedDefinitionNode.DisplayId = value; }

		public override string Description { get => LinkedDefinitionNode.Description; set => LinkedDefinitionNode.Description = value; }

		public virtual LinkNode InverseLink
		{
			get
			{
				if (_inverseLink == null)
				{
					if (ParentFolder == null)
					{
						// Cannot determine inverse link because the link node has no parent node
						throw new InvalidOperationException(string.Format(ExceptionMessage.CannotDetermineInverseLinkBecauseTheLinkNodeHasNoParent,
							Type.DisplayName, DisplayId));
					}

					// Attempt to find the folder on the linked definition node that has an inverse folder member ID of this link node's parent folder
					var inverseFolder = ParentFolder.InverseFolderIn(LinkedDefinitionNode);
					if (inverseFolder != null)
					{
						_inverseLink = new LinkNode(TypeId, inverseFolder.MemberId, FromDefinitionNode)
						{
							// The ParentFolder property is set (which sets the ascendants of the inverse link node),
							// but the inverse link node isn't added to the inverse folder because inverse folder
							// contents are queried/generated on access (i.e. they don't actually hold the inverse
							// link nodes.  We also don't want the inverse link added to the descendants collections
							// of the ascendant nodes because inverse links are transient.
							ParentFolder = inverseFolder,
							_inverseLink = this
						};
					}
				}

				return _inverseLink;
			}
		}
		private LinkNode _inverseLink;

		protected LinkNode()
		{
		}

		internal LinkNode(Guid nodeTypeId, Guid fromMemberId, DefinitionNode targetNode)
			: base(nodeTypeId)
		{
			targetNode = ArgumentValidation.AssertNotNull(targetNode, nameof(targetNode));
			_fromMemberId = fromMemberId;
			_linkedDefinitionNode = targetNode;
			_rootNode = _linkedDefinitionNode.RootNode;
		}

		public override bool CanAddNew(out IEnumerable<NodeType> permittedTypes)
		{
			// Cannot add new nodes to a link node because link nodes do not have folders or child nodes
			permittedTypes = new NodeType[] { };
			return false;
		}

		public override bool CanAddNew(NodeType nodeType, object[] initialisationParameters = null)
		{
			// Cannot add new nodes to a link node because link nodes do not have folders or child nodes
			return false;
		}

		public override bool CanAddNew(NodeType nodeType, out IEnumerable<BrowserFolder> permittedFolders)
		{
			// Cannot add new nodes to a link node because link nodes do not have folders or child nodes
			permittedFolders = new BrowserFolder[] { };
			return false;
		}

		public override bool CanAddNew(NodeType nodeType, object[] initialisationParameters, out IEnumerable<BrowserFolder> permittedFolders)
		{
			// Cannot add new nodes to a link node because link nodes do not have folders or child nodes
			permittedFolders = new BrowserFolder[] { };
			return false;
		}

		protected internal override LinkNode AddTo(IValidated<BrowserFolder> folder)
		{
			// Adding a link node to a folder is actually adding the linked definition node to that folder
			return LinkedDefinitionNode.AddTo(folder);
		}

		public override bool CanCopyTo(ParentNode node, out IEnumerable<BrowserFolder> permittedFolders)
		{
			if (base.CanCopyTo(node, out permittedFolders))
			{
				// Not allowed to copy a link node to any folder containing the linked definition node
				permittedFolders = permittedFolders.Where(f => !f.Nodes.OfType<DefinitionNode>().Contains(LinkedDefinitionNode));
				return permittedFolders.Count() > 0;
			}

			return false;
		}

		public override void MoveTo(BrowserFolder folder)
		{
			folder = ArgumentValidation.AssertNotNull(folder, nameof(folder));
			if (!CanMoveTo(folder))
			{
				// Can't copy to the given folder
				throw new ArgumentException(string.Format(ExceptionMessage.CannotMoveToDestinationFolder, 
					Type.DisplayName, DisplayId, folder), nameof(folder));
			}

			// Does the destination folder contain the linked definition node?
			if (folder == LinkedDefinitionNode.ParentFolder)
			{
				// Just delete the link
				Delete();
				return;
			}
			
			// Back to normal move operation
			new NodeMover(assureDestinationIds: true).Move(this, Validatable.Validated(folder));
		}

		public override void Delete()
		{
			// If the link node corresponds to a property that has a value derived by evaluating a formula, the link node should be deleted permanently because there 
			// will never be any need to restore the link node because the property value will be automatically recalculated.
			Delete(deletePermanantly: IsPropertyLink && CorrespondingPropertyConfiguration.HasFormula, clearPropertyValue:IsPropertyLink);
		}

		protected internal override void Delete(bool deletePermanantly)
		{
			Delete(deletePermanantly, clearPropertyValue: true);
		}

		protected internal virtual void Delete(bool deletePermanantly, bool clearPropertyValue)
		{
			// If node is already deleted, then nothing to do
			if (IsDeleted)
			{
				return;
			}

			// If the link node is defined by an inverse link then it's deleted by deleting the inverse link
			if (_inverseLink != null)
			{
				_inverseLink.Delete(deletePermanantly, clearPropertyValue);
				return;
			}

			if (IsPropertyLink && clearPropertyValue && CorrespondingPropertyConfiguration.Accessor.AccessorType != AccessorType.ReadOnlyProperty)
			{
				// Set the source member back to null.  Null is always supported because the 
				// node mapping class guarantees only reference types can be mapped as node and 
				// target object types.
				FromDefinitionNode.SetPropertyValue(_fromMemberId, null, refreshLinkNodes: false, evaluatePropertyForumula: false);
			}

			// Note: Propery value must be cleared before the base Delete method is invoked because the DefinitionNode.SetPropertyValue
			// method checks whether the value has changed, which causes a get of the value, which would fail for a node value if the
			// corresponding property link node has already been deleted from the property links folder.
			base.Delete(deletePermanantly);
			LinkedDefinitionNode.RemoveInboundLink(this);
		}

		/// <summary>
		/// Returns a string representation of the link node.
		/// </summary>
		/// <returns>A string representation of the link node.</returns>
		public override string ToString()
		{
			return string.Format("Link To: {0}", LinkedDefinitionNode.ToString());
		}
	}
}
