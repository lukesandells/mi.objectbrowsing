﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Core;
using MI.Core.Validation;

namespace MI.Framework.ObjectBrowsing
{
	/// <summary>
	/// Extension methods for the <see cref="IEnumerable{BrowserNode}"/> class.
	/// </summary>
	public static class BrowserNodeEnumerableExtensions
	{
		/// <summary>
		/// Selects the top-level non-inverse folder nodes.
		/// </summary>
		/// <typeparam name="TNode">The type of node.</typeparam>
		/// <param name="sourceNodes">The nodes from which to select the nodes.</param>
		/// <returns>The top-level non-inverse folder nodes.</returns>
		public static IEnumerable<TNode> WhereTopLevelAndNotInverseLinks<TNode>(this IEnumerable<TNode> sourceNodes)
			where TNode : BrowserNode
		{
			return sourceNodes.Where(node => node.ParentNode == null || !sourceNodes.Contains((BrowserNode)node.ParentNode) && !node.ParentFolder.IsInverse);
		}

		/// <summary>
		/// Gets the operations permitted to be performed on the collection of nodes.
		/// </summary>
		/// <param name="nodes">The nodes to evaluate.</param>
		/// <returns>The operations permitted to be performed on the collection of nodes.</returns>
		public static IEnumerable<OperationType> PermittedOperations(this IEnumerable<BrowserNode> nodes)
		{
			return new[] { OperationType.Delete };
		}

		/// <summary>
		/// Returns the set of operations permitted to be performed between the nodes and the given node, represented as a 
		/// dictionary of permitted folders indexed by permitted node operation type.
		/// </summary>
		/// <param name="sourceNodes">The nodes to evaluate.</param>
		/// <param name="destinationNode">The node to which permitted operations shall be evaluated.</param>
		/// <returns>The set of operations permitted to be performed between the nodes and the given node.</returns>
		public static IReadOnlyDictionary<OperationType, IEnumerable<BrowserFolder>> PermittedOperationsTo(this IEnumerable<BrowserNode> sourceNodes,
			BrowserNode destinationNode)
		{
			sourceNodes = ArgumentValidation.AssertNotNull(sourceNodes, nameof(sourceNodes));
			var permittedOperations = new Dictionary<OperationType, IEnumerable<BrowserFolder>>();
			if (destinationNode is ParentNode nodeAsParent)
			{
				var sourceDefinitionNodes = sourceNodes.OfType<DefinitionNode>();
				var destinationDefinitionNode = destinationNode as DefinitionNode;
				if (sourceDefinitionNodes.Count() == sourceNodes.Count() && destinationDefinitionNode != null
					&& sourceDefinitionNodes.CanAddTo(destinationDefinitionNode, out var permittedFolders))
				{
					permittedOperations.Add(OperationType.AddTo, permittedFolders);
				}

				if (sourceNodes.CanMoveTo(nodeAsParent, out permittedFolders))
				{
					permittedOperations.Add(OperationType.MoveTo, permittedFolders);
				}

				if (sourceNodes.CanCopyTo(nodeAsParent, out permittedFolders))
				{
					permittedOperations.Add(OperationType.CopyTo, permittedFolders);
				}
			}

			return permittedOperations;
		}

		/// <summary>
		/// Returns the set of operations permitted to be performed between the nodes and the given folder.
		/// </summary>
		/// <param name="nodes">The nodes to evaluate.</param>
		/// <param name="folder">The folder to which permitted operations shall be evaluated.</param>
		/// <returns>The set of operations permitted to be performed between the nodes and the given folder.</returns>
		public static IEnumerable<OperationType> PermittedOperationsTo(this IEnumerable<BrowserNode> nodes, BrowserFolder folder)
		{
			nodes = ArgumentValidation.AssertNotNull(nodes, nameof(nodes));
			var permittedOperations = new List<OperationType>();
			var definitionNodes = nodes.OfType<DefinitionNode>();
			if (definitionNodes.Count() == nodes.Count() && definitionNodes.CanAddTo(folder))
			{
				permittedOperations.Add(OperationType.AddTo);
			}

			if (nodes.CanMoveTo(folder))
			{
				permittedOperations.Add(OperationType.MoveTo);
			}

			if (nodes.CanCopyTo(folder))
			{
				permittedOperations.Add(OperationType.CopyTo);
			}

			return permittedOperations;
		}

		public static bool CanMoveTo(this IEnumerable<BrowserNode> sourceNodes, ParentNode destinationNode, out IEnumerable<BrowserFolder> permittedFolders)
		{
			permittedFolders = null;
			sourceNodes = ArgumentValidation.AssertNotNull(sourceNodes, nameof(sourceNodes));
			foreach (var node in sourceNodes.WhereTopLevelAndNotInverseLinks())
			{
				if (node.CanMoveTo(destinationNode, out var folders) && (permittedFolders == null || permittedFolders.Count() > 0))
				{
					permittedFolders = permittedFolders == null ? folders : permittedFolders.Intersect(folders);
				}
				else
				{
					permittedFolders = new BrowserFolder[] { };
					return false;
				}
			}

			return true;
		}

		public static bool CanMoveTo(this IEnumerable<BrowserNode> sourceNodes, ParentNode destinationNode)
		{
			sourceNodes = ArgumentValidation.AssertNotNull(sourceNodes, nameof(sourceNodes));
			return sourceNodes.WhereTopLevelAndNotInverseLinks().All(node => node.CanMoveTo(destinationNode));
		}

		public static bool CanMoveTo(this IEnumerable<BrowserNode> sourceNodes, BrowserFolder folder)
		{
			sourceNodes = ArgumentValidation.AssertNotNull(sourceNodes, nameof(sourceNodes));
			return sourceNodes.WhereTopLevelAndNotInverseLinks().All(node => node.CanMoveTo(folder));
		}

		public static void MoveTo(this IEnumerable<BrowserNode> sourceNodes, ParentNode destinationNode)
		{
			sourceNodes = ArgumentValidation.AssertNotNull(sourceNodes, nameof(sourceNodes));

			// Get the destination folders at the destination node for each source node
			var destinationFolders = new Dictionary<BrowserNode, BrowserFolder>();
			foreach (var sourceNode in sourceNodes.WhereTopLevelAndNotInverseLinks())
			{
				if (sourceNode.CanMoveTo(destinationNode, out var permittedFolders) && permittedFolders.Count() == 1)
				{
					destinationFolders[sourceNode] = permittedFolders.Single();
				}
				else
				{
					// Can't move to the given node
					throw new ArgumentException(string.Format(ExceptionMessage.CannotMoveToDestinationNode, sourceNode.Type.DisplayName, 
						sourceNode.DisplayId, sourceNode.Type.DisplayName, sourceNode.DisplayId), nameof(sourceNodes));
				}
			}

			// Execute the move in groups to the destination folders
			var mover = new NodeMover(assureDestinationIds: true);
			destinationFolders.ToLookup(kvp => kvp.Value, kvp => kvp.Key)
				.ForEach(grouping => mover.Move(grouping, Validatable.Validated(grouping.Key)));
		}

		public static void MoveTo(this IEnumerable<BrowserNode> sourceNodes, BrowserFolder folder)
		{
			sourceNodes = ArgumentValidation.AssertNotNull(sourceNodes, nameof(sourceNodes));
			sourceNodes = sourceNodes.WhereTopLevelAndNotInverseLinks();

			// Need to do a complete check first before we start moving anything
			if (!sourceNodes.CanMoveTo(folder))
			{
				// Can't move to the given folder
				throw new ArgumentException(string.Format(ExceptionMessage.CannotMoveOneOfTheGivenNodesToDestinationFolder, folder.Path), nameof(folder));
			}

			new NodeMover(assureDestinationIds: true).Move(sourceNodes, Validatable.Validated(folder));
		}

		public static bool CanCopyTo(this IEnumerable<BrowserNode> sourceNodes, ParentNode destinationNode, out IEnumerable<BrowserFolder> permittedFolders)
		{
			sourceNodes = ArgumentValidation.AssertNotNull(sourceNodes, nameof(sourceNodes));

			permittedFolders = null;
			foreach (var node in sourceNodes.WhereTopLevelAndNotInverseLinks())
			{
				if (node.CanCopyTo(destinationNode, out var folders) && (permittedFolders == null || permittedFolders.Count() > 0))
				{
					permittedFolders = permittedFolders == null ? folders : permittedFolders.Intersect(folders);
				}
				else
				{
					permittedFolders = new BrowserFolder[] { };
					return false;
				}
			}

			return true;
		}

		public static bool CanCopyTo(this IEnumerable<BrowserNode> sourceNodes, ParentNode destinationNode)
		{
			sourceNodes = ArgumentValidation.AssertNotNull(sourceNodes, nameof(sourceNodes));
			return sourceNodes.WhereTopLevelAndNotInverseLinks().All(node => node.CanCopyTo(destinationNode));
		}

		public static bool CanCopyTo(this IEnumerable<BrowserNode> sourceNodes, BrowserFolder folder)
		{
			sourceNodes = ArgumentValidation.AssertNotNull(sourceNodes, nameof(sourceNodes));
			return sourceNodes.WhereTopLevelAndNotInverseLinks().All(node => node.CanCopyTo(folder));
		}

		public static IReadOnlyDictionary<BrowserNode, BrowserNode> CopyTo(this IEnumerable<BrowserNode> sourceNodes, ParentNode destinationNode)
		{
			sourceNodes = ArgumentValidation.AssertNotNull(sourceNodes, nameof(sourceNodes));
			var copies = new Dictionary<BrowserNode, BrowserNode>();

			// Get the destination folders at the destination node for each source node
			var destinationFolders = new Dictionary<BrowserNode, BrowserFolder>();
			foreach (var sourceNode in sourceNodes.WhereTopLevelAndNotInverseLinks())
			{
				if (sourceNode.CanCopyTo(destinationNode, out var permittedFolders) && permittedFolders.Count() == 1)
				{
					destinationFolders[sourceNode] = permittedFolders.Single();
				}
				else
				{
					// Can't copy to the given node
					throw new ArgumentException(string.Format(ExceptionMessage.CannotCopyToDestinationNode, sourceNode.Type.DisplayName,
						sourceNode.DisplayId, sourceNode.Type.DisplayName, sourceNode.DisplayId), nameof(sourceNodes));
				}
			}

			// Execute the copy in groups to the destination folders
			var copier = new NodeCopier();
			destinationFolders.ToLookup(kvp => kvp.Value, kvp => kvp.Key)
				.ForEach(grouping => copies.Add(copier.Copy(grouping, Validatable.Validated(grouping.Key))));
			return copies;
		}

		public static IReadOnlyDictionary<BrowserNode, BrowserNode> CopyTo(this IEnumerable<BrowserNode> sourceNodes, BrowserFolder folder)
		{
			sourceNodes = ArgumentValidation.AssertNotNull(sourceNodes, nameof(sourceNodes));
			sourceNodes = sourceNodes.WhereTopLevelAndNotInverseLinks();

			// Need to do a complete check first before we start copying anything
			if (!sourceNodes.CanCopyTo(folder))
			{
				// Can't copy to the given folder
				throw new ArgumentException(string.Format(ExceptionMessage.CannotCopyOneOfTheGivenNodesToDestinationFolder, folder.Path), nameof(folder));
			}

			return new NodeCopier().Copy(sourceNodes, Validatable.Validated(folder));
		}

		public static bool CanAddTo(this IEnumerable<DefinitionNode> sourceNodes, DefinitionNode destinationNode,
			out IEnumerable<BrowserFolder> permittedFolders)
		{
			sourceNodes = ArgumentValidation.AssertNotNull(sourceNodes, nameof(sourceNodes));

			permittedFolders = null;
			foreach (var node in sourceNodes.WhereTopLevelAndNotInverseLinks())
			{
				if (node.CanAddTo(destinationNode, out var folders) && (permittedFolders == null || permittedFolders.Count() > 0))
				{
					permittedFolders = permittedFolders == null ? folders : permittedFolders.Intersect(folders);
				}
				else
				{
					permittedFolders = new BrowserFolder[] { };
					return false;
				}
			}

			return true;
		}

		public static bool CanAddTo(this IEnumerable<DefinitionNode> sourceNodes, DefinitionNode destinationNode)
		{
			sourceNodes = ArgumentValidation.AssertNotNull(sourceNodes, nameof(sourceNodes));
			return sourceNodes.WhereTopLevelAndNotInverseLinks().All(node => node.CanAddTo(destinationNode));
		}

		public static bool CanAddTo(this IEnumerable<DefinitionNode> sourceNodes, BrowserFolder folder)
		{
			sourceNodes = ArgumentValidation.AssertNotNull(sourceNodes, nameof(sourceNodes));
			return sourceNodes.WhereTopLevelAndNotInverseLinks().All(node => node.CanAddTo(folder));
		}

		public static IReadOnlyDictionary<DefinitionNode, LinkNode> AddTo(this IEnumerable<DefinitionNode> sourceNodes,
			DefinitionNode destinationNode)
		{
			sourceNodes = ArgumentValidation.AssertNotNull(sourceNodes, nameof(sourceNodes));
			sourceNodes = sourceNodes.WhereTopLevelAndNotInverseLinks();

			// Get the destination folders for all nodes to be added
			var destinationFolders = new Dictionary<DefinitionNode, BrowserFolder>();
			foreach (var sourceNode in sourceNodes.WhereTopLevelAndNotInverseLinks())
			{
				if (sourceNode.CanAddTo(destinationNode, out var permittedFolders) && permittedFolders.Count() == 1)
				{
					destinationFolders[sourceNode] = permittedFolders.Single();
				}
				else
				{
					// Can't add to the given node
					throw new ArgumentException(string.Format(ExceptionMessage.CannotAddToDestinationNode, sourceNode.Type.DisplayName, 
						sourceNode.DisplayId, sourceNode.Type.DisplayName, sourceNode.DisplayId), nameof(sourceNodes));
				}
			}

			// Execute the add in groups to the destination folders
			var addedNodes = new Dictionary<DefinitionNode, LinkNode>();
			destinationFolders.ToLookup(kvp => kvp.Value, kvp => kvp.Key)
				.ForEach(grouping => addedNodes.Add(grouping.AddTo(Validatable.Validated(grouping.Key))));
			return addedNodes;
		}

		public static IReadOnlyDictionary<DefinitionNode, LinkNode> AddTo(this IEnumerable<DefinitionNode> sourceNodes, BrowserFolder folder)
		{
			sourceNodes = ArgumentValidation.AssertNotNull(sourceNodes, nameof(sourceNodes));
			sourceNodes = sourceNodes.WhereTopLevelAndNotInverseLinks();

			// Need to do a complete check first before we start adding anything
			if (!sourceNodes.CanAddTo(folder))
			{
				// Can't move to the given node
				throw new ArgumentException(string.Format(ExceptionMessage.CannotAddOneOfTheGivenNodesToDestinationFolder, folder.Path), nameof(folder));
			}

			return sourceNodes.AddTo(Validatable.Validated(folder));
		}

		private static IReadOnlyDictionary<DefinitionNode, LinkNode> AddTo(this IEnumerable<DefinitionNode> sourceNodes, IValidated<BrowserFolder> folder)
		{
			// Add the nodes
			var addedNodes = new Dictionary<DefinitionNode, LinkNode>();
			sourceNodes.ForEach(node => addedNodes.Add(node, node.AddTo(folder)));
			return addedNodes;
		}

		public static void Delete(this IEnumerable<BrowserNode> nodes)
		{
			Delete(nodes, false);
		}

		public static void Delete(this IEnumerable<BrowserNode> nodes, bool deletePermanently)
		{
			nodes = ArgumentValidation.AssertNotNull(nodes, nameof(nodes));
			nodes.WhereTopLevelAndNotInverseLinks().ForEach(node => node.Delete());
		}

		public static PropertyCollection CommonProperties(this IEnumerable<DefinitionNode> nodes)
		{
			return new PropertyCollection(nodes);
		}

		/// <summary>
		/// Filters the collection of nodes to return only those nodes that are of the given type.
		/// </summary>
		/// <typeparam name="TBrowserNode">The type of node to return.</typeparam>
		/// <param name="nodes">The collection of folders to filter.</param>
		/// <returns>The collection of nodes that are of the given type.</returns>
		/// <remarks>
		/// This method exists because the <see cref="BrowserNode"/> class has derived classes and may participate in many-to-one
		/// polymorphic associations, in which it may be proxied by the persistence framework.  If this occurs, a member of a node
		/// collection may in fact be a proxy to a node, where the proxy derives directly from the <see cref="BrowserNode"/> 
		/// class, irrespective of whether the proxied object is a subclass of the browser node class.  Therefore, the standard
		/// LINQ <see cref="Enumerable.OfType{TResult}(System.Collections.IEnumerable)"/> extension method is not guaranteed to 
		/// always find the all nodes of the given type because some of the nodes in the collection may be proxies.  This extension
		/// method provides a replacement implementation of the <see cref="Enumerable.OfType{TResult}(System.Collections.IEnumerable)"/>
		/// method, which correctly evaluates the type of a proxied browser node object, which may be found in a collection of 
		/// browser nodes.
		/// </remarks>
		public static IEnumerable<TBrowserNode> OfType<TBrowserNode>(this IEnumerable<BrowserNode> nodes) where TBrowserNode : BrowserNode
		{
			return nodes.Where(node => node.Is<TBrowserNode>()).Cast<TBrowserNode>();
		}

		/// <summary>
		/// Casts the elements of a collection of nodes to the given type of node.
		/// </summary>
		/// <typeparam name="TBrowserNode">The type of node to which to cast.</typeparam>
		/// <param name="nodes">The collection of nodes to cast to the given type.</param>
		/// <returns>A collection of nodes with elements from the given collection cast to the given type of node.</returns>
		/// This method exists because the <see cref="BrowserNode"/> class has derived classes and may participate in many-to-one
		/// polymorphic associations, in which it may be proxied by the persistence framework.  If this occurs, a member of a node
		/// collection may in fact be a proxy to a node, where the proxy derives directly from the <see cref="BrowserNode"/> 
		/// class, irrespective of whether the proxied object is a subclass of the browser node class.  Therefore, the standard
		/// LINQ <see cref="Enumerable.Cast{TResult}(System.Collections.IEnumerable)"/> extension method is not guaranteed to 
		/// always successfully cast the elements of the collection to the given type because some of the nodes in the collection may 
		/// be proxies.  This extension method provides a replacement implementation of the 
		/// <see cref="Enumerable.Cast{TResult}(System.Collections.IEnumerable)"/> method, which is able to bypass the proxy of 
		/// a proxied node object, which may be found in a collection of nodes, in order to be able to return the 
		/// collection of nodes cast to the given type.
		/// </remarks>
		public static IEnumerable<TBrowserNode> Cast<TBrowserNode>(this IEnumerable<BrowserNode> nodes) where TBrowserNode : BrowserNode
		{
			return nodes.Select(node => node.Cast<TBrowserNode>());
		}
	}
}
