﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using MI.Core.Validation;
using MI.Core;

namespace MI.Framework.ObjectBrowsing
{
	/// <summary>
	/// A path of browser folders from a <see cref="RootNode"/> to the parent folder of a <see cref="BrowserNode"/>.
	/// </summary>
	public class FolderPath : IEnumerable<FolderPath.FolderSpecification>
	{
		/// <summary>
		/// An empty folder path.
		/// </summary>
		/// <remarks>
		/// The relative path of a folder to itself is an empty folder path.
		/// </remarks>
		public static FolderPath Empty { get; } = new FolderPath();

		/// <summary>
		/// Specifies a folder in a folder path
		/// </summary>
		public class FolderSpecification
		{
			/// <summary>
			/// The type of node containing the specified folder.
			/// </summary>
			public NodeType ParentNodeType => _parentNodeType;
			private NodeType _parentNodeType;

			/// <summary>
			/// The display ID of the node containing the specified folder.
			/// </summary>
			public string ParentNodeDisplayId { get => _parentNodeDisplayId; internal set => _parentNodeDisplayId = value; }
			private string _parentNodeDisplayId;

			/// <summary>
			/// The specified folder member of the specified parent node type.
			/// </summary>
			public Guid MemberId => _memberId;
			private Guid _memberId;

			/// <summary>
			/// The name of the specified folder.
			/// </summary>
			public string FolderName => _folderName;
			private string _folderName;

			/// <summary>
			/// Stipulates whether the specified folder is a property links folder.
			/// </summary>
			public bool IsPropertyLinksFolder => _isPropertyLinksFolder;
			private bool _isPropertyLinksFolder;

			/// <summary>
			/// Stipulates whether the specified folder is a primary folder.
			/// </summary>
			public bool IsPrimary => _isPrimary;
			private bool _isPrimary;

			/// <summary>
			/// Stipulates whether the specified folder is a recycle bin.
			/// </summary>
			public bool IsRecycleBin => _isRecycleBin;
			private bool _isRecycleBin;

			/// <summary>
			/// Initialises a new instance of the <see cref="FolderSpecification"/> class.
			/// </summary>
			/// <param name="folder">The folder for which to create the folder specification.</param>
			public FolderSpecification(BrowserFolder folder)
			{
				_memberId = folder.MemberId;
				_folderName = folder.DisplayName;
				_parentNodeType = folder.Parent.Type;
				_parentNodeDisplayId = folder.Parent.DisplayId;
				_isPrimary = folder.IsPrimary;
				_isPropertyLinksFolder = folder.IsPropertyLinksFolder;
				_isRecycleBin = folder.IsRecycleBin;
			}

			/// <summary>
			/// Returns a string representation of the folder specification.
			/// </summary>
			/// <returns>A string representation of the folder specification.</returns>
			public override string ToString()
			{
				return BrowserFolder.NodeQualifiedName(ParentNodeDisplayId, FolderName, IsPrimary, IsPropertyLinksFolder, IsRecycleBin);
			}

			/// <summary>
			/// Determines whether the folder specification equals the given object.
			/// </summary>
			/// <param name="object"></param>
			/// <returns><c>true</c> if the folder specification equals the given object; otherwise <c>false</c>.</returns>
			public override bool Equals(object @object)
			{
				// If parameter is null return false.
				if (@object == null)
				{
					return false;
				}

				// If parameter cannot be cast to FolderSpecification, return false.
				var fs = @object as FolderSpecification;
				if ((Object)fs == null)
				{
					return false;
				}

				// Return true if the fields match
				return _parentNodeType == fs._parentNodeType
					&& _parentNodeDisplayId == fs._parentNodeDisplayId
					&& _memberId == fs._memberId;
			}

			/// <summary>
			/// Returns a hash code for the folder specification.
			/// </summary>
			/// <returns>A hash code for the folder specification.</returns>
			public override int GetHashCode()
			{
				return (_memberId, _folderName, _parentNodeType, _parentNodeDisplayId).GetHashCode();
			}
		}

		/// <summary>
		/// Indicates whether the folder path is relative or absolute.
		/// </summary>
		public bool IsRelative => _isRelative;
		private bool _isRelative;

		/// <summary>
		/// The list of folder specifications comprising the folder path, ordered from the root node folder to the lowest-level folder.
		/// </summary>
		private IList<FolderSpecification> _pathFolders = new List<FolderSpecification>();

		/// <summary>
		/// The folder specified by the path.  Will be <c>null</c> if the folder specified by the path does not exist or if the path is relative.
		/// </summary>
		public BrowserFolder SpecifiedFolder => _specifiedFolder;
		private BrowserFolder _specifiedFolder;

		/// <summary>
		/// The folder's root node.  Only specified for absolute paths.
		/// </summary>
		private RootNode _rootNode;

		/// <summary>
		/// The length of the path.
		/// </summary>
		public int Length => _pathFolders.Count;

		/// <summary>
		/// Initialises a new instance of the <see cref="FolderPath"/> class as an absolute folder path.
		/// </summary>
		/// <param name="folder">The folder to which the new path shall be created.</param>
		private FolderPath(BrowserFolder folder)
		{
			_specifiedFolder = ArgumentValidation.AssertNotNull(folder, nameof(folder));
			_rootNode = _specifiedFolder.Parent.RootNode;
			_isRelative = false;
			while (folder != null)
			{
				_pathFolders.Insert(0, new FolderSpecification(folder));
				folder = folder.Parent.ParentFolder;
			}
		}

		/// <summary>
		/// Intialises a new empty instance of the <see cref="FolderPath"/> class.
		/// </summary>
		private FolderPath()
		{
		}

		/// <summary>
		/// Creates a new absolute path to the given folder.
		/// </summary>
		/// <param name="node">The folder to which to create the path.</param>
		/// <returns>An absolute path to the given folder.</returns>
		public static FolderPath To(BrowserFolder folder)
		{
			return new FolderPath(folder);
		}

		/// <summary>
		/// Returns the folder path relative to the given folder path.
		/// </summary>
		/// <param name="path">The folder path from which the relative path shall be determined.</param>
		/// <returns>The folder path relative to the given folder path.</returns>
		public FolderPath RelativeTo(FolderPath path)
		{
			path = ArgumentValidation.AssertNotNull(path, nameof(path));
			if (IsRelative || path.IsRelative)
			{
				// Relative paths can only be determined between absolute paths.
				throw new InvalidOperationException(ExceptionMessage.CanOnlyDeterminedRelativePathBetweenTwoAbsolutePaths);
			}

			if (Length < path.Length)
			{
				// The given path does not specify a folder that is an ascendant of the folder specified by this path.
				throw new ArgumentException(string.Format(ExceptionMessage.CanOnlyDetermineRelativePathToAnAscendantFolder, path), nameof(path));
			}

			if (Equals(path))
			{
				return Empty;
			}

			for (var i = 0; i < path.Length; i++)
			{
				if (!_pathFolders[i].Equals(path._pathFolders[i]))
				{
					// The folder path is not relative to the given folder path.
					throw new ArgumentException(string.Format(ExceptionMessage.FolderPathIsNotRelativeToTheGivenFolderPath, path), nameof(path));
				}
			}

			var result = new FolderPath() { _isRelative = true };
			for (var i = path.Length; i < Length; i++)
			{
				result._pathFolders.Add(_pathFolders[i]);
			}
			
			return result;
		}

		/// <summary>
		/// Combines a relative path with an absolute path.
		/// </summary>
		/// <param name="absolutePath">The absolute path with which to combine the relative path.</param>
		/// <param name="relativePath">The relative path to combine with the absolute path.</param>
		/// <returns>The combined path.</returns>
		public static FolderPath Combine(FolderPath absolutePath, FolderPath relativePath)
		{
			absolutePath = ArgumentValidation.AssertNotNull(absolutePath, nameof(absolutePath));
			relativePath = ArgumentValidation.AssertNotNull(relativePath, nameof(relativePath));

			if (!relativePath.IsRelative)
			{
				return relativePath;
			}

			if (absolutePath.IsRelative)
			{
				// Absolute path parameter is not an absolute folder path
				throw new ArgumentException(string.Format(ExceptionMessage.AbsolutePathParameterIsNotAbsolute, absolutePath), nameof(absolutePath));
			}

			var result = new FolderPath()
			{
				_specifiedFolder = absolutePath._specifiedFolder,
				_rootNode = absolutePath._rootNode
			};
			absolutePath._pathFolders.ForEach(pathFolder => result._pathFolders.Add(pathFolder));
			foreach (var pathFolder in relativePath._pathFolders)
			{
				result._pathFolders.Add(pathFolder);
				result._specifiedFolder = result._specifiedFolder?.Nodes
					.Where(node => node.Type == pathFolder.ParentNodeType && node.DisplayId == pathFolder.ParentNodeDisplayId)
					.OfType<DefinitionNode>()
					.SelectMany(node => node.NonEmptyNonInverseFolders)
					.Where(f => f.MemberId == pathFolder.MemberId)
					.SingleOrDefault();
			}
			
			return result;
		}

		/// <summary>
		/// Determines whether the folder path equals the given object.
		/// </summary>
		/// <param name="object"></param>
		/// <returns><c>true</c> if the folder path equals the given object; otherwise <c>false</c>.</returns>
		public bool Equals(FolderPath path)
		{
			if (path == null)
			{
				return false;
			}

			// Not equal if the root nodes don't match
			if (_rootNode != path._rootNode)
			{
				return false;
			}

			// Not equal if path lengths don't match
			if (Length != path.Length)
			{
				return false;
			}

			// Only equal if all folder specifiations match
			for (var i = 0; i < Length; i++)
			{
				if (!_pathFolders[i].Equals(path._pathFolders[i]))
				{
					return false;
				}
			}

			return true;
		}

		/// <summary>
		/// Determines whether the path folder equals the given object.
		/// </summary>
		/// <param name="object">The object with which to compare.</param>
		/// <returns><c>true</c> if the object equals the path folder; otherwise <c>false</c>.</returns>
		public override bool Equals(object @object)
		{
			// If parameter is null return false.
			if (@object == null)
			{
				return false;
			}

			// If parameter cannot be cast to FolderPath, return false.
			var path = @object as FolderPath;
			if ((object)path == null)
			{
				return false;
			}

			return Equals(path);
		}

		/// <summary>
		/// Returns a hash value for the folder path.
		/// </summary>
		/// <returns>A hash value for the folder path.</returns>
		public override int GetHashCode()
		{
			int hashCode = _rootNode?.GetHashCode() ?? 0;
			_pathFolders.ForEach(pathFolder => hashCode ^= pathFolder.GetHashCode());
			return hashCode;
		}

		/// <summary>
		/// Returns the first parent of the given type.
		/// </summary>
		/// <typeparam name="T">The type of parent to return.</typeparam>
		/// <returns>The first parent of the given type or <c>null</c> if no parent of the given type is found.</returns>
		public T ParentOfType<T>()
			where T : class
		{
			if (IsRelative)
			{
				// Cannot retrieve a parent from a relative folder path
				throw new InvalidOperationException();
			}

			var folder = SpecifiedFolder;
			foreach (var pathFolder in _pathFolders.Reverse())
			{
				if (pathFolder.ParentNodeType == NodeType.For(typeof(T)))
				{
					var candidate = folder?.Parent?.TargetObject as T;
					if (candidate != null)
					{
						return candidate;
					}

					if (folder?.Parent is DefinitionNode definitionNode)
					{
						candidate = definitionNode.TargetObject as T;
						if (candidate != null)
						{
							return candidate;
						}
					}
				}

				folder = folder?.Parent.ParentFolder;
			}

			return null;
		}

		/// <summary>
		/// Returns a string representation of the folder path.
		/// </summary>
		/// <returns>A string representation of the folder path.</returns>
		public override string ToString()
		{
			return (IsRelative ? string.Empty : @"\") + string.Join(@"\", _pathFolders);
		}

		/// <summary>
		/// Returns whether the given display ID is available at the folder specified by the path.
		/// </summary>
		/// <param name="nodeType">The type of node for which to determine whether the given ID is available.</param>
		/// <param name="displayId">The display ID to evaluate.</param>
		/// <returns><c>true</c> if the given display ID is available; otherwise <c>false</c>.</returns>
		internal bool IsIdAvailable(NodeType nodeType, string displayId)
		{
			if (IsRelative || this == Empty)
			{
				// Operation only available for non-empty absolute paths
				throw new InvalidOperationException(ExceptionMessage.OperationOnlyAvailableForNonEmptyAbsolutePaths);
			}

			return UnavailableIds(nodeType).Where(id => id == displayId).Count() == 0;
		}

		/// <summary>
		/// Returns the next available display ID for a node within the node context.
		/// </summary>
		/// <param name="nodeType">The type of node for which to get the next available ID.</param>
		/// <param name="initialisationParameters">The node's initialisation parameters.</param>
		/// <returns>The next available display ID.</returns>
		internal string NextAvailableId(NodeType nodeType, object[] initialisationParameters)
		{
			if (IsRelative || this == Empty)
			{
				// Operation only available for non-empty absolute paths
				throw new InvalidOperationException();
			}

			int maxSuffix;
			var idPrefix = nodeType.Configuration.DisplayId.PrefixFunc?.Invoke(initialisationParameters) ?? 
				nodeType.DisplayName.Replace(" ", string.Empty) + ".";
            var existingIdsResult = UnavailableIds(nodeType)
                .Where(id => id.ToLower().StartsWith(idPrefix.ToLower()));

            var existingIdsWithPrefix = MI.Core.EnumerableExtensions.ToHashSet(existingIdsResult);

			if (existingIdsWithPrefix.Count() == 0)
			{
				maxSuffix = 0;
			}
			else
			{
				Regex regex = new Regex("^" + idPrefix + @"(\d+)", RegexOptions.IgnoreCase);
				maxSuffix = existingIdsWithPrefix.Max(id => string.IsNullOrEmpty(regex.Match(id).Groups[1].Value)? 0 : int.Parse(regex.Match(id).Groups[1].Value));
			}

			return idPrefix + (maxSuffix + 1).ToString();
		}

		/// <summary>
		/// Returns a display ID that is guaranteed to be available at the folder specified by the path.
		/// </summary>
		/// <param name="nodeType">The type of node for which to get the assured available ID.</param>
		/// <param name="candidateId">The candidate display ID to assure.</param>
		/// <param name="idClashSuffix">The suffix to repeatedly append to the given candidate ID until the ID is available.</param>
		/// <returns>The given display ID if available; otherwise the display ID appended with a suffix such that it is available.</returns>
		/// <remarks>
		/// This method returns the given display ID if it is available, otherwise it repeatedly appends the given suffix until an available
		/// ID is found.
		/// </remarks>
		internal string AssuredAvailableId(NodeType nodeType, string candidateId, string idClashSuffix)
		{
			return AssuredAvailableId(candidateId, idClashSuffix, UnavailableIds(nodeType));
		}

		/// <summary>
		/// Returns a display ID that is not contained within the given set of unavailable IDs.
		/// </summary>
		/// <param name="candidateId">The candidate display ID to assure.</param>
		/// <param name="idClashSuffix">The suffix to repeatedly append to the given candidate ID until the ID is available.</param>
		/// <returns>The given display ID if available; otherwise the display ID appended with a suffix such that it is available.</returns>
		/// <remarks>
		/// This method returns the given display ID if it is available, otherwise it repeatedly appends the given suffix until an available
		/// ID is found.
		/// </remarks>
		internal static string AssuredAvailableId(string candidateId, string idClashSuffix, IEnumerable<string> unavailableIds)
		{
			while (unavailableIds.Contains(candidateId))
			{
				candidateId = candidateId + idClashSuffix;
			}

			return candidateId;
		}

		/// <summary>
		/// Returns the set of unavailable IDs at the folder specified by the folder path.
		/// </summary>
		/// <param name="nodeType">The type of node for which to get the set of unavailable IDs.</param>
		/// <returns>The set of unavailable IDs at the folder specified by the folder path.</returns>
		internal IQueryable<string> UnavailableIds(NodeType nodeType)
		{
			return UnavailableIds(nodeType, new object[] { });
		}

		/// <summary>
		/// Returns the set of unavailable IDs at the folder specified by the folder path.
		/// </summary>
		/// <param name="nodeType">The type of node for which to get the set of unavailable IDs.</param>
		/// <param name="exclusions">The existing objects to be excluded from the ID availability test.</param>
		/// <returns>The set of unavailable IDs at the folder specified by the folder path.</returns>
		internal IQueryable<string> UnavailableIds(NodeType nodeType, IEnumerable<object> exclusions)
		{
			return nodeType.Configuration.DisplayId.FindUnavailableIdsFunc?.Invoke(this, exclusions) ?? (new string[] { }).AsQueryable();
		}

		/// <summary>
		/// Gets an enumerator of folder specifications for the folder path.
		/// </summary>
		/// <returns>An enumerator of folder specifications for the folder path.</returns>
		public IEnumerator<FolderSpecification> GetEnumerator()
		{
			return _pathFolders.GetEnumerator();
		}

		/// <summary>
		/// Gets an enumerator of folder specifications for the folder path.
		/// </summary>
		/// <returns>An enumerator of folder specifications for the folder path.</returns>
		IEnumerator IEnumerable.GetEnumerator()
		{
			return _pathFolders.GetEnumerator();
		}
	}
}
