﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Core;
using MI.Core.Validation;
using MI.Framework.ObjectBrowsing.Configuration;

namespace MI.Framework.ObjectBrowsing
{
	/// <summary>
	/// A folder within a <see cref="ParentNode"/> object.
	/// </summary>
	public class BrowserFolder
	{
		/// <summary>
		/// Uniquely identifies the folder.
		/// </summary>
		public virtual Guid Id => _id;
		private readonly Guid _id = Guid.NewGuid();

		/// <summary>
		/// Uniquely identifies the node type member to which the folder corresponds.
		/// </summary>
		public virtual Guid MemberId => _memberId;
		private readonly Guid _memberId;

		/// <summary>
		/// The folder's configuration from the <see cref="NodeType"/> object.
		/// </summary>
		protected internal virtual FolderConfiguration Configuration => 
			_configuration ?? (_configuration = FolderConfiguration.WithMemberId(Parent.TypeId, MemberId));
		private FolderConfiguration _configuration;

		/// <summary>
		/// The node to which the folder belongs.
		/// </summary>
		public virtual ParentNode Parent { get => _parent; protected internal set => _parent = value; }
		private ParentNode _parent;

		/// <summary>
		/// The nodes contained within the folder.
		/// </summary>
		public virtual IEnumerable<BrowserNode> Nodes => IsInverse ? GetInverseFolderNodes() : _nodes;
		private readonly ISet<BrowserNode> _nodes = new HashSet<BrowserNode>();

		/// <summary>
		/// Indicates whether the folder holds property link nodes.
		/// </summary>
		public virtual bool IsPropertyLinksFolder => Configuration.IsPropertyLinksFolder;

		/// <summary>
		/// Indicates whether the folder allows definition nodes.
		/// </summary>
		public virtual bool AllowsDefinitionNodes => Configuration.AllowsDefinitionNodes;

		/// <summary>
		/// Indicates whether the folder allows link nodes.
		/// </summary>
		public virtual bool AllowsLinkNodes => Configuration.AllowsLinkNodes;

		/// <summary>
		/// Indicates whether the folder is the primary folder for its parent node.
		/// </summary>
		public virtual bool IsPrimary => Configuration.IsPrimary;

		/// <summary>
		/// Indicates whether the folder is an inverse folder.
		/// </summary>
		public virtual bool IsInverse => Configuration.IsInverse;

		/// <summary>
		/// Indicates whether the folder is a recycle bin.
		/// </summary>
		public virtual bool IsRecycleBin => Configuration.IsRecycleBin;
		
		/// <summary>
		/// Indicates whether the folder is system managed.  Property links and inverse folders are system managed.
		/// </summary>
		public virtual bool SystemManaged => Configuration.SystemManaged;
		
		/// <summary>
		/// The display name for the folder.
		/// </summary>
		public virtual string DisplayName => Configuration.DisplayName;

		/// <summary>
		/// The types of nodes permitted in the folder.
		/// </summary>
		public virtual IEnumerable<NodeType> PermittedTypes => Configuration.PermittedTypes;

		/// <summary>
		/// The path to the folder.
		/// </summary>
		public virtual FolderPath Path => FolderPath.To(this);

		/// <summary>
		/// Indicates whether the folder is empty.
		/// </summary>
		/// <remarks>
		/// Has a backing field rather than using the <see cref="IList{T}.Count"/> property of the <see cref="Nodes"/> collection to
		/// allow checking whether a folder is empty without doing a roundtrip to the database.
		/// </remarks>
		public virtual bool IsEmpty => IsInverse ? Nodes.Count() == 0 : _isEmpty;
		private bool _isEmpty = true;

		/// <summary>
		/// Gets the operations permitted to be performed on the folder.
		/// </summary>
		public virtual IEnumerable<OperationType> PermittedOperations
		{
			get
			{
				var permittedOperations = new List<OperationType>();
				if (CanAddNew(out _))
				{
					permittedOperations.Add(OperationType.AddNew);
				}

				Configuration.CustomOperations.Where(kvp => kvp.Value?.Invoke(this) ?? true).ForEach(kvp => permittedOperations.Add(kvp.Key));
				return permittedOperations;
			}
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="BrowserFolder"/> class.
		/// </summary>
		protected BrowserFolder()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="BrowserFolder"/> class.
		/// </summary>
		/// <param name="memberId">Identifies the node type member to which the new folder corresponds.</param>
		/// <param name="parent">The parent to which the new folder belongs.</param>
		protected BrowserFolder(Guid memberId, ParentNode parent)
		{
			_parent = ArgumentValidation.AssertNotNull(parent, nameof(parent));
			_memberId = memberId;
		}

		/// <summary>
		/// Creates a new browser folder.
		/// </summary>
		/// <param name="memberId">Identifies the node type member to which the new folder corresponds.</param>
		/// <param name="parent">The parent to which the new folder belongs.</param>
		/// <returns>The newly created folder.</returns>
		internal static BrowserFolder Create(Guid memberId, ParentNode parent)
		{
			var configuration = FolderConfiguration.WithMemberId(parent.TypeId, memberId);
			if (configuration.IsRecycleBin)
			{
				return new RecycleBin(parent);
			}

			if (configuration.IsPropertyLinksFolder)
			{
				return new PropertyLinksFolder(parent.Cast<DefinitionNode>());
			}

			var folder = new BrowserFolder(memberId, parent) { _configuration = configuration };
			return folder;
		}

		/// <summary>
		/// Casts the folder to the given type of folder.
		/// </summary>
		/// <typeparam name="TBrowserFolder">The type of folder to which to cast.</typeparam>
		/// <returns>The folder cast to the given type.</returns>
		/// <remarks>
		/// This method exists because the <see cref="BrowserFolder"/> class has derived classes and may participate in many-to-one
		/// polymorphic associations, in which it may be proxied by the persistence framework.  If this occurs, the proxy cannot 
		/// be safely down-cast to a <see cref="BrowserFolder"/> derived class without using this method because the proxy class is 
		/// a subclass of the <see cref="BrowserFolder"/> class, not any of the classes deriving from <see cref="BrowserFolder"/>.
		/// </remarks>
		public virtual TBrowserFolder Cast<TBrowserFolder>()
			where TBrowserFolder : BrowserFolder
		{
			return (TBrowserFolder)this;
		}

		/// <summary>
		/// Returns whether the folder is of the given type.
		/// </summary>
		/// <typeparam name="TBrowserFolder">The type of folder against which the node shall be evaluated.</typeparam>
		/// <returns><c>true</c> if the folder is of the given type; otherwise <c>false</c>.</returns>
		/// <remarks>
		/// This method exists because the <see cref="BrowserFolder"/> class has derived classes and may participate in many-to-one
		/// polymorphic associations, in which it may be proxied by the persistence framework.  If this occurs, the native language
		/// evaluation of the type of an instance of a <see cref="BrowserFolder"/> derived class will return <c>false</c> because the 
		/// proxy class is a subclass of <see cref="BrowserFolder"/>, not any of the classes deriving from <see cref="BrowserFolder"/>.
		/// </remarks>
		public virtual bool Is<TBrowserFolder>()
			where TBrowserFolder : BrowserFolder
		{
			return this is TBrowserFolder;
		}

		/// <summary>
		/// Returns whether the folder is of the given type.
		/// </summary>
		/// <typeparam name="TBrowserFolder">The type of node against which the node shall be evaluated.</typeparam>
		/// <param name="folder">The folder cast to the given type or <c>null</c> if the folder is not of the given type.</param>
		/// <returns><c>true</c> if the folder is of the given type; otherwise <c>false</c>.</returns>
		/// <remarks>
		/// This method exists because the <see cref="BrowserFolder"/> class has derived classes and may participate in many-to-one
		/// polymorphic associations, in which it may be proxied by the persistence framework.  If this occurs, the native language
		/// evaluation of the type of an instance of a <see cref="BrowserFolder"/> derived class will return <c>false</c> because the 
		/// proxy class is a subclass of <see cref="BrowserFolder"/>, not any of the classes deriving from <see cref="BrowserFolder"/>.
		/// </remarks>
		public virtual bool Is<TBrowserFolder>(out TBrowserFolder folder)
			where TBrowserFolder : BrowserFolder
		{
			folder = this as TBrowserFolder;
			return Is<TBrowserFolder>();
		}

		/// <summary>
		/// Returns the folder cast to the given type if it is of the given type, otherwise <c>null</c>.
		/// </summary>
		/// <typeparam name="TBrowserFolder">The type of folder to which to cast.</typeparam>
		/// <returns>The folder cast to the given type if it is of the given type, otherwise <c>null</c>.</returns>
		/// <remarks>
		/// This method exists because the <see cref="TBrowserFolder"/> class has derived classes and may participate in many-to-one
		/// polymorphic associations, in which it may be proxied by the persistence framework.  If this occurs, the native language
		/// cast will fail because the proxy class is a subclass of <see cref="TBrowserFolder"/>, not any of the classes deriving from 
		/// <see cref="TBrowserFolder"/>.
		/// </remarks>
		public virtual TBrowserFolder As<TBrowserFolder>()
			where TBrowserFolder : BrowserFolder
		{
			return this as TBrowserFolder;
		}
		
		/// <summary>
		/// Determines whether the given object equals the current object.
		/// </summary>
		/// <param name="obj">The object to evaluate.</param>
		/// <returns><c>true</c> if the given object equals the current object; otherwise <c>false</c>.</returns>
		/// <remarks>
		/// Returns <c>true</c> if the <see cref="Id"/> properties of the given object and the current object are the same.
		/// The <see cref="Equals(object)"/> method must be overridden because where a reference to a folder object is actually
		/// a reference to a proxy, the <see cref="Cast{TBrowserFolder}"/> and <see cref="Is{TBrowserFolder}(out TBrowserFolder)"/> 
		/// methods bypass the proxy, returning a reference to the proxied object.  This means that some external references
		/// may be to the proxied object, while some may be to the proxy object.  It is necessary to be able to compare two 
		/// instances of an object where one is proxied and one is not.  Therefore, because the proxy for an object is a different
		/// object than the proxied object, the standard comparison by reference is inadequate.
		/// </remarks>
		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}

			var node = obj as BrowserFolder;
			if (node == null)
			{
				return false;
			}

			return node.Id == Id;
		}

		/// <summary>
		/// Returns the hashcode for the folder.
		/// </summary>
		/// <returns>The hashcode for the folder.</returns>
		/// <remarks>
		/// This method is overridden because the <see cref="Equals(object)"/> method is overridden.  See the remarks section of
		/// <see cref="Equals(object)"/> for more information.
		/// </remarks>
		public override int GetHashCode()
		{
			return Id.GetHashCode();
		}

		/// <summary>
		/// Overloaded == operator.
		/// </summary>
		/// <param name="left">The left-side operand.</param>
		/// <param name="right">The right-side operand.</param>
		/// <returns><c>true</c> if the nodes are equal, otherwise <c>false</c>.</returns>
		/// <remarks>
		/// This overloaded operator is provided because the <see cref="Equals(object)"/> method is overridden.  See the remarks section
		/// of <see cref="Equals(object)"/> for more information.
		/// </remarks>
		public static bool operator ==(BrowserFolder left, BrowserFolder right)
		{
			if (ReferenceEquals(left, right))
			{
				return true;
			}

			if ((object)left == null || (object)right == null)
			{
				return false;
			}

			return left.Id == right.Id;
		}

		/// <summary>
		/// Overloaded != operator.
		/// </summary>
		/// <param name="left">The left-side operand.</param>
		/// <param name="right">The right-side operand.</param>
		/// <returns><c>true</c> if the nodes are not equal, otherwise <c>false</c>.</returns>
		/// <remarks>
		/// This overloaded operator is provided because the <see cref="Equals(object)"/> method is overridden.  See the remarks section
		/// of <see cref="Equals(object)"/> for more information.
		/// </remarks>
		public static bool operator !=(BrowserFolder left, BrowserFolder right)
		{
			return !(left == right);
		}

		/// <summary>
		/// Gets the collection of inverse link nodes contained within an inverse folder.
		/// </summary>
		/// <returns>The collection of inverse link nodes contained within an inverse folder.</returns>
		private IEnumerable<BrowserNode> GetInverseFolderNodes()
		{
			return Parent.Cast<DefinitionNode>().InboundLinks
				.Where(link => link.FromMemberId == Configuration.InverseFolderMemberId)
				.Where(link => PermittedTypes.Contains(link.Type))
				.Select(link => link.InverseLink);
		}

		/// <summary>
		/// Returns whether the folder permits the given type of node.
		/// </summary>
		/// <typeparam name="TBrowserNode">The type of browser node to determine whether it is permitted in the folder.</typeparam>
		/// <returns><c>true</c> if the given type of browser node is permitted in the folder; otherwise <c>false</c>.</returns>
		public virtual bool PermitsType<TBrowserNode>() 
			where TBrowserNode : BrowserNode
		{
			return PermittedTypes.Any(type => typeof(TBrowserNode).IsAssignableFrom(type.Configuration.NodeType));
		}

		/// <summary>
		/// Returns whether the folder permits the given type of node with the given type of target object.
		/// </summary>
		/// <typeparam name="TBrowserNode">The type of browser node to determine whether it is permitted in the folder.</typeparam>
		/// <typeparam name="TTargetObject">The type of target object to determine whether it is permitted in the folder.</typeparam>
		/// <returns><c>true</c> if the given type of browser node with the given type of target object is permitted in the folder; 
		/// otherwise <c>false</c>.</returns>
		public virtual bool PermitsType<TBrowserNode, TTargetObject>()
			where TBrowserNode : BrowserNode
			where TTargetObject : class
		{
			return PermittedTypes.Any(type => typeof(TBrowserNode).IsAssignableFrom(type.Configuration.NodeType)
				&& typeof(TTargetObject).IsAssignableFrom(type.TargetObjectType));
		}

		/// <summary>
		/// Determines whether new definition nodes can be added to the folder.
		/// </summary>
		/// <param name="permittedTypes">The permitted types of definition nodes that may be added to the folder.</param>
		/// <returns><c>true</c> if new definition nodes can be added to the folder; otherwise <c>false</c>.</returns>
		public virtual bool CanAddNew(out IEnumerable<NodeType> permittedTypes)
		{
			if (AllowsDefinitionNodes)
			{
				permittedTypes = Configuration.PermittedTypes;
				return true;
			}

			permittedTypes = new NodeType[] { };
			return false;
		}

		/// <summary>
		/// Determines whether a new definition noode of a given type with a given set of initialisation parameters can be added to the folder.
		/// </summary>
		/// <param name="nodeType">The type of node to consider.</param>
		/// <param name="initialisationParameters">The list of initialisation parameters to evaluate.</param>
		/// <returns><c>true</c> if a new definition node of the given type with the given set of initialisation parameters 
		/// can be added to the folder.</returns>
		public virtual bool CanAddNew(NodeType nodeType, object[] initialisationParameters)
		{
			nodeType = ArgumentValidation.AssertNotNull(nodeType, nameof(nodeType));
			if (CanAddNew(out _))
			{
				return Configuration.PermittedTypeConfigurations[nodeType.Id].CanAddNewPredicate?.Invoke(Parent.TargetObject, initialisationParameters 
					?? new object[] { }) ?? true;
			}

			return false;
		}

		/// <summary>
		/// Adds a new node of a given type to the folder using a given list of initialisation parameters.
		/// </summary>
		/// <param name="nodeType">The type of node to create and add to the folder.</param>
		/// <param name="initialisationParameters">The list of initialisation parameters to use when creating the new node.</param>
		/// <returns>The newly created definition node.</returns>
		public virtual DefinitionNode AddNew(NodeType nodeType, object[] initialisationParameters = null)
		{
			return Parent.AddNew(nodeType, this, initialisationParameters);
		}

		/// <summary>
		/// Adds a new node of a given type to the folder with a given display ID using a given list of initialisation parameters.
		/// </summary>
		/// <param name="nodeType">The type of node to create and add to the folder.</param>
		/// <param name="displayId">The display ID for the new node.</param>
		/// <param name="initialisationParameters">The list of initialisation parameters to use when creating the new node.</param>
		/// <returns>The newly created definition node.</returns>
		public virtual DefinitionNode AddNew(NodeType nodeType, string displayId, object[] initialisationParameters = null)
		{
			return Parent.AddNew(nodeType, this, displayId, initialisationParameters);
		}

		/// <summary>
		/// Adds a link node to the folder.
		/// </summary>
		/// <param name="node">The link node to add to the folder.</param>
		protected internal virtual void Add(LinkNode node)
		{
			Add(node, false);
		}

		/// <summary>
		/// Adds a node to the folder.
		/// </summary>
		/// <param name="node">The node to add to the folder.</param>
		/// <param name="evaluatePropertyFormulae">Stipulates whether property formulae for the newly added node shall be evaluated after
		/// the node has been added.</param>
		protected internal virtual void Add(BrowserNode node, bool evaluatePropertyFormulae)
		{
			node = ArgumentValidation.AssertNotNull(node, nameof(node));
			if (node.ParentFolder != null)
			{
				// Node already belongs to another folder
				throw new ArgumentException(string.Format(ExceptionMessage.CannotAddNodeToFolderBecauseNodeAlreadyBelongsToAnotherFolder,
					node.Type.DisplayName, node.DisplayId, this, node.ParentFolder), nameof(node));
			}

			_isEmpty = false;
			_nodes.Add(node);
			
			// Must set parent folder before setting any configured inverse property because setting the property
			// value will cause any property formulae to be evaluated, which may reference the parent folder or node.
			// Also, the parent folder must be set before invoking OnNodeAdded because setting the parent folder
			// populates the parent node's ascendants and the newly added node must be added to the descendants list
			// of all the parent's nodes ascendants.
			node.ParentFolder = this;
			node.RootNode = Parent.RootNode;
			Parent.OnNodeAdded(this, node);

			var definitionNode = node.As<DefinitionNode>();
			var inversePropertyMemberId = Configuration.PermittedTypeConfigurations[node.Type.Id].InversePropertyMemberId;
			if (inversePropertyMemberId.HasValue)
			{
				if (definitionNode != null && (!definitionNode.Properties[inversePropertyMemberId.Value]?.Configuration.IsReadOnly ?? false))
				{
					definitionNode.SetPropertyValue(inversePropertyMemberId.Value, Parent, refreshLinkNodes: true, evaluatePropertyForumula: false);
				}
			}

			if (definitionNode != null && evaluatePropertyFormulae)
			{
				definitionNode.EvaluatePropertyFormulae();
			}

			Configuration.PermittedTypeConfigurations[node.Type.Id].AddAction?.Invoke(_parent.TargetObject, node.TargetObject);
		}

		/// <summary>
		/// Removes the given node from the folder.
		/// </summary>
		/// <param name="node">The node to remove.</param>
		protected internal virtual void Remove(LinkNode node)
		{
			Remove(node, false);
		}

		/// <summary>
		/// Removes the given node from the folder.
		/// </summary>
		/// <param name="node">The node to remove.</param>
		/// <param name="evaluatePropertyFormulae">Stipulates whether the property formulae for the node shall be evaluated.</param>
		protected internal virtual void Remove(BrowserNode node, bool evaluatePropertyFormulae)
		{
			node = ArgumentValidation.AssertNotNull(node, nameof(node));

			// Ensure node belongs to this folder.
			if (node.ParentFolder != this)
			{
				// Node does not belong to this folder
				throw new ArgumentException(string.Format(ExceptionMessage.CannotRemoveNodeFromFolderBecauseNodeBelongsToAnotherFolder,
					node.Type.DisplayName, node.DisplayId, this, node.ParentFolder), nameof(node));
			}

			_nodes.Remove(node);
			_isEmpty = _nodes.Count == 0;
			
			// OnNodeRemoved must be invoked before the parent folder is set to null because setting the parent folder
			// to null clears the parent node's ascendants, which means the removed node and its descendants aren't
			// removed from the descendants list of the ascendants.
			Parent.OnNodeRemoved(this, node);
			node.ParentFolder = null;
			node.RootNode = null;

			var definitionNode = node.As<DefinitionNode>();
			var inversePropertyMemberId = Configuration.PermittedTypeConfigurations[node.Type.Id].InversePropertyMemberId;
			if (inversePropertyMemberId.HasValue)
			{
				if (definitionNode != null && (!definitionNode.Properties[inversePropertyMemberId.Value]?.Configuration.IsReadOnly ?? false))
				{
					definitionNode.SetPropertyValue(inversePropertyMemberId.Value, null, refreshLinkNodes: true, evaluatePropertyForumula: false);
				}
			}

			if (definitionNode != null && evaluatePropertyFormulae)
			{
				definitionNode.EvaluatePropertyFormulae();
			}

			Configuration.PermittedTypeConfigurations[node.Type.Id].RemoveAction?.Invoke(_parent.TargetObject, node.TargetObject);
		}

		/// <summary>
		/// Empties the folder.
		/// </summary>
		protected void Clear()
		{
			// ToList otherwise enumerable is modified
			Nodes.ToList().ForEach(node => node.Delete(true));
		}

		/// <summary>
		/// Finds the folder's inverse folder in a given node.
		/// </summary>
		/// <param name="node">The node in which to locate the corresponding inverse folder.</param>
		/// <returns>The inverse folder in the given node if present; otherwise <c>null</c>.</returns>
		public virtual BrowserFolder InverseFolderIn(ParentNode node)
		{
			node = ArgumentValidation.AssertNotNull(node, nameof(node));
			return IsInverse ? node.AllFolders.SingleOrDefault(f => f.MemberId == Configuration.InverseFolderMemberId)
				: node.AllFolders.SingleOrDefault(f => f.Configuration.InverseFolderMemberId == MemberId);
		}

		/// <summary>
		/// Returns a string representation of the folder.
		/// </summary>
		/// <returns>A string representation of the folder.</returns>
		public override string ToString()
		{
			return NodeQualifiedName(Parent.DisplayId, DisplayName, IsPrimary, IsPropertyLinksFolder, IsRecycleBin);
		}

		/// <summary>
		/// Returns node-qualified name of a folder.
		/// </summary>
		/// <param name="parentNodeDisplayId">The folder's parent node's display ID.</param>
		/// <param name="displayName">The folder's display name.</param>
		/// <param name="isPrimary">Stipulates whether the folder is primary.</param>
		/// <param name="isPropertyLinksFolder">Stipulates whether the folder is a property links folder.</param>
		/// <param name="isRecycleBin">Stipulates whether the folder is a recycle bin.</param>
		/// <returns>The node-qualified name.</returns>
		internal static string NodeQualifiedName(string parentNodeDisplayId, string displayName, 
			bool isPrimary, bool isPropertyLinksFolder, bool isRecycleBin)
		{
			return parentNodeDisplayId + @"::" + 
				(isPrimary ? "Primary Folder" : (isPropertyLinksFolder ? "Property Links Folder" : (isRecycleBin ? "Recycle Bin" :displayName)));
		}
	}
}
