﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using MI.Core;
using MI.Core.Validation;
using MI.Framework.ObjectBrowsing.Validation;

namespace MI.Framework.ObjectBrowsing
{
	/// <summary>
	/// A node in an object browser.
	/// </summary>
	public abstract class BrowserNode
	{
		/// <summary>
		/// Unique node identifier.
		/// </summary>
		public virtual Guid Id => _id;
		private Guid _id = Guid.NewGuid();

		/// <summary>
		/// Unique node type identifier.
		/// </summary>
		public virtual Guid TypeId => _typeId;
		private Guid _typeId;

		/// <summary>
		/// Describes the type of node.
		/// </summary>
		public virtual NodeType Type => _type ?? (_type = NodeType.WithId(TypeId));
		private NodeType _type;

		/// <summary>
		/// Indicates whether the node is a <see cref="LinkNode"/> object.
		/// </summary>
		public abstract bool IsLinkNode { get; }

		/// <summary>
		/// Indicates whether the node is a <see cref="DefinitionNode"/> object.
		/// </summary>
		public abstract bool IsDefinitionNode { get; }

		/// <summary>
		/// Indicates whether the node is a <see cref="RootNode"/> object.
		/// </summary>
		public abstract bool IsRootNode { get; }

		/// <summary>
		/// The node meta-type.
		/// </summary>
		public virtual NodeMetaType MetaType => new NodeMetaType(Type.Configuration.NodeType, Type.TargetObjectType, IsLinkNode, IsDefinitionNode);

		/// <summary>
		/// The root node for the node.
		/// </summary>
		public virtual RootNode RootNode { get; protected internal set; }
		
		/// <summary>
		/// The parent node for the node.
		/// </summary>
		public virtual ParentNode ParentNode => ParentFolder?.Parent;

		/// <summary>
		/// The folder within the parent node <see cref="ParentNode"/> containing the node.
		/// </summary>
		public virtual BrowserFolder ParentFolder
		{
			get => _parentFolder;
			protected internal set
			{
				var formerParentNode = ParentNode;
				_parentFolder = value;
				_isDeleted = _parentFolder?.IsRecycleBin ?? _isDeleted;
				if (formerParentNode != value?.Parent)
				{
					_depth = (_parentFolder?.Parent?.Depth ?? -1) + 1;

                    // This node and each of its descendants need their ascendants refreshed
                    new[] { this }.Union(DescendantsOrderedByDepth)
                        .ForEach(descendantOrThis => descendantOrThis.ProtectedAscendants = descendantOrThis.ParentNode == null ? new Collection<ParentNode>() :
                        (ICollection<ParentNode>)MI.Core.EnumerableExtensions.ToHashSet(new[] { descendantOrThis.ParentNode }.Union(descendantOrThis.ParentNode.Ascendants)));//.ToHashSet());
				}
			}
		}
		private BrowserFolder _parentFolder;

		/// <summary>
		/// The depth of the node in the node hierarchy.
		/// </summary>
		public virtual int Depth => _depth;
		private int _depth;

		/// <summary>
		/// The ascendants of the node.
		/// </summary>
		public virtual IEnumerable<ParentNode> Ascendants => _ascendants.OrderByDescending(ascendant => ascendant.Depth);

		/// <summary>
		/// The protected read/write descendants of the node.
		/// </summary>
		/// <remarks>
		/// This property exists because direct access to the backing field between different instances of the <see cref="BrowserNode"/> class
		/// must be avoided because a node instance may be a proxy, and proxy field accesses are not propagated to the proxied object.
		/// </remarks>
		protected virtual ICollection<ParentNode> ProtectedAscendants { get => _ascendants; set => _ascendants = value; }
		private ICollection<ParentNode> _ascendants = new Collection<ParentNode>();

		/// <summary>
		/// The descendant nodes of the node.
		/// </summary>
		public virtual IEnumerable<BrowserNode> Descendants => new BrowserNode[] { };

		/// <summary>
		/// The decendant nodes of the node ordered by node depth.
		/// </summary>
		public virtual IEnumerable<BrowserNode> DescendantsOrderedByDepth => Descendants.OrderBy(node => node.Depth);

		/// <summary>
		/// The target object for the node.
		/// </summary>
		/// <remarks>
		/// A node may be self-referencing, in which case the target object references the node itself.  However a node may also
		/// reference another object of whatever type is configured in the mapping for the node type.  This allows node logic to 
		/// be written in classes that do not have to inherit from the <see cref="DefinitionNode"/> class.
		/// </remarks>
		public virtual object TargetObject => this;

		/// <summary>
		/// The display ID for the node.
		/// </summary>
		public virtual string DisplayId { get => _displayId; set => _displayId = value; }
		private string _displayId = string.Empty;

		/// <summary>
		/// A description of the node.
		/// </summary>
		public virtual string Description { get => _description; set => _description = value; }
		private string _description = string.Empty;

		/// <summary>
		/// Indicates whether the node has been deleted.
		/// </summary>
		/// <remarks>
		/// Deleted nodes are either placed in the recycle bin (see <see cref="RecycleBin"/>) of a <see cref="RootNode"/> object or
		/// deleted permanently.  Permanently deleted transient nodes should not be persisted.  Permanently deleted persistent nodes
		/// should be removed from the database.  Therefore, the <see cref="IsDeleted"/> property should only ever be true in the
		/// database for nodes in the recycle bin.
		/// </remarks>
		public virtual bool IsDeleted => _isDeleted;
		private bool _isDeleted;

		/// <summary>
		/// The set of validation results for the node.
		/// </summary>
		public virtual IEnumerable<NodeValidationResult> ValidationResults => _validationResults;
		private ISet<NodeValidationResult> _validationResults = new HashSet<NodeValidationResult>();

		/// <summary>
		/// Gets the operations permitted to be performed on the node.
		/// </summary>
		public virtual IEnumerable<OperationType> PermittedOperations
		{
			get
			{
				var permittedOperations = new List<OperationType>();
				if (CanAddNew(out _))
				{
					permittedOperations.Add(OperationType.AddNew);
				}
				permittedOperations.Add(OperationType.Delete);
				Type.Configuration.CustomNodeOperations.Where(kvp => kvp.Value?.Invoke(this) ?? true).ForEach(kvp => permittedOperations.Add(kvp.Key));
				return permittedOperations;
			}
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="BrowserNode"/> class.
		/// </summary>
		protected BrowserNode()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="BrowserNode"/> class.
		/// </summary>
		/// <param name="nodeTypeId">Identifies the type of new node.</param>
		protected BrowserNode(Guid nodeTypeId)
		{
			_typeId = nodeTypeId;
		}

		/// <summary>
		/// Sets the node's node type ID.
		/// </summary>
		/// <param name="nodeTypeId">The node type ID to set.</param>
		/// <remarks>
		/// This method is needed because derived types need to set the node type in their constructors and the <see cref="TypeId"/>
		/// property must be virtual because it is persisted and therefore required to be virtual by the persistence framework.  It is bad 
		/// practice to access virtual members within a constructor, so therefore this non-virtual protected method is needed.</remarks>
		protected void SetNodeTypeId(Guid nodeTypeId)
		{
			_typeId = nodeTypeId;
		}

		/// <summary>
		/// Casts the node to the given type of node.
		/// </summary>
		/// <typeparam name="TBrowserNode">The type of node to which to cast.</typeparam>
		/// <returns>The node cast to the given type.</returns>
		/// <remarks>
		/// This method exists because the <see cref="BrowserNode"/> class has derived classes and may participate in many-to-one
		/// polymorphic associations, in which it may be proxied by the persistence framework.  If this occurs, the proxy cannot 
		/// be safely down-cast to a <see cref="BrowserNode"/> derived class without using this method because the proxy class is 
		/// a subclass of the <see cref="BrowserNode"/> class, not any of the classes deriving from <see cref="BrowserNode"/>.
		/// </remarks>
		public virtual TBrowserNode Cast<TBrowserNode>()
			where TBrowserNode : BrowserNode
		{
			return (TBrowserNode)this;
		}

		/// <summary>
		/// Returns whether the node is of the given type.
		/// </summary>
		/// <typeparam name="TBrowserNode">The type of node against which the node shall be evaluated.</typeparam>
		/// <returns><c>true</c> if the node is of the given type; otherwise <c>false</c>.</returns>
		/// <remarks>
		/// This method exists because the <see cref="BrowserNode"/> class has derived classes and may participate in many-to-one
		/// polymorphic associations, in which it may be proxied by the persistence framework.  If this occurs, the native language
		/// evaluation of the type of an instance of a <see cref="BrowserNode"/> derived class will return <c>false</c> because the 
		/// proxy class is a subclass of <see cref="BrowserNode"/>, not any of the classes deriving from <see cref="BrowserNode"/>.
		/// </remarks>
		public virtual bool Is<TBrowserNode>()
			where TBrowserNode : BrowserNode
		{
			return this is TBrowserNode;
		}

		/// <summary>
		/// Returns whether the node is of the given type.
		/// </summary>
		/// <typeparam name="TBrowserNode">The type of node against which the node shall be evaluated.</typeparam>
		/// <param name="node">The node cast to the given type or <c>null</c> if the node is not of the given type.</param>
		/// <returns><c>true</c> if the node is of the given type; otherwise <c>false</c>.</returns>
		/// <remarks>
		/// This method exists because the <see cref="BrowserNode"/> class has derived classes and may participate in many-to-one
		/// polymorphic associations, in which it may be proxied by the persistence framework.  If this occurs, the native language
		/// evaluation of the type of an instance of a <see cref="BrowserNode"/> derived class will return <c>false</c> because the 
		/// proxy class is a subclass of <see cref="BrowserNode"/>, not any of the classes deriving from <see cref="BrowserNode"/>.
		/// </remarks>
		public virtual bool Is<TBrowserNode>(out TBrowserNode node)
			where TBrowserNode : BrowserNode
		{
			node = this as TBrowserNode;
			return Is<TBrowserNode>();
		}

		/// <summary>
		/// Determines whether the node is of a given meta-type.
		/// </summary>
		/// <param name="metaType">The meta-type with which to compare.</param>
		/// <returns><c>true</c> if the node is of the given meta-type; otherwise <c>false</c>.</returns>
		public virtual bool Is(NodeMetaType metaType)
		{
			metaType = ArgumentValidation.AssertNotNull(metaType, nameof(metaType));
			return metaType.IsAssignableFrom(MetaType);
		}

		/// <summary>
		/// Returns the node cast to the given type if it is of the given type, otherwise <c>null</c>.
		/// </summary>
		/// <typeparam name="TBrowserNode">The type of node to which to cast.</typeparam>
		/// <returns>The node cast to the given type if it is of the given type, otherwise <c>null</c>.</returns>
		/// <remarks>
		/// This method exists because the <see cref="BrowserNode"/> class has derived classes and may participate in many-to-one
		/// polymorphic associations, in which it may be proxied by the persistence framework.  If this occurs, the native language
		/// cast will fail because the proxy class is a subclass of <see cref="BrowserNode"/>, not any of the classes deriving from 
		/// <see cref="BrowserNode"/>.
		/// </remarks>
		public virtual TBrowserNode As<TBrowserNode>()
			where TBrowserNode : BrowserNode
		{
			return this as TBrowserNode;
		}

		/// <summary>
		/// Determines whether the given object equals the current object.
		/// </summary>
		/// <param name="obj">The object to evaluate.</param>
		/// <returns><c>true</c> if the given object equals the current object; otherwise <c>false</c>.</returns>
		/// <remarks>
		/// Returns <c>true</c> if the <see cref="Id"/> properties of the given object and the current object are the same.
		/// The <see cref="Equals(object)"/> method must be overridden because where a reference to a node object is actually
		/// a reference to a proxy, the <see cref="Cast{TBrowserNode}"/> and <see cref="Is{TBrowserNode}(out TBrowserNode)"/> 
		/// methods bypass the proxy, returning a reference to the proxied object.  This means that some external references
		/// may be to the proxied object, while some may be to the proxy object.  It is necessary to be able to compare two 
		/// instances of an object where one is proxied and one is not.  Therefore, because the proxy for an object is a different
		/// object than the proxied object, the standard comparison by reference is inadequate.
		/// </remarks>
		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}

			var node = obj as BrowserNode;
			if (node == null)
			{
				return false;
			}

			return node.Id == Id;
		}

		/// <summary>
		/// Returns the hashcode for the node.
		/// </summary>
		/// <returns>The hashcode for the node.</returns>
		/// <remarks>
		/// This method is overridden because the <see cref="Equals(object)"/> method is overridden.  See the remarks section of
		/// <see cref="Equals(object)"/> for more information.
		/// </remarks>
		public override int GetHashCode()
		{
			return Id.GetHashCode();
		}

		/// <summary>
		/// Overloaded == operator.
		/// </summary>
		/// <param name="left">The left-side operand.</param>
		/// <param name="right">The right-side operand.</param>
		/// <returns><c>true</c> if the nodes are equal, otherwise <c>false</c>.</returns>
		/// <remarks>
		/// This overloaded operator is provided because the <see cref="Equals(object)"/> method is overridden.  See the remarks section
		/// of <see cref="Equals(object)"/> for more information.
		/// </remarks>
		public static bool operator ==(BrowserNode left, BrowserNode right)
		{
			if (ReferenceEquals(left, right))
			{
				return true;
			}

			if ((object)left == null || (object)right == null)
			{
				return false;
			}

			return left.Id == right.Id;
		}

		/// <summary>
		/// Overloaded != operator.
		/// </summary>
		/// <param name="left">The left-side operand.</param>
		/// <param name="right">The right-side operand.</param>
		/// <returns><c>true</c> if the nodes are not equal, otherwise <c>false</c>.</returns>
		/// <remarks>
		/// This overloaded operator is provided because the <see cref="Equals(object)"/> method is overridden.  See the remarks section
		/// of <see cref="Equals(object)"/> for more information.
		/// </remarks>
		public static bool operator !=(BrowserNode left, BrowserNode right)
		{
			return !(left == right);
		}

		/// <summary>
		/// Returns the set of operations permitted to be performed between the node and the given node, represented as a 
		/// dictionary of permitted folders indexed by permitted node operation type.
		/// </summary>
		/// <param name="destinationNode">The node to which permitted operations shall be evaluated.</param>
		/// <returns>The set of operations permitted to be performed between the node and the given node.</returns>
		public virtual IReadOnlyDictionary<OperationType, IEnumerable<BrowserFolder>> PermittedOperationsTo(BrowserNode destinationNode)
		{
			var permittedOperations = new Dictionary<OperationType, IEnumerable<BrowserFolder>>();
			if (destinationNode is ParentNode nodeAsParent)
			{
				if (destinationNode.Is<DefinitionNode>(out var definitionNode) && CanAddTo(definitionNode, out var permittedFolders))
				{
					permittedOperations.Add(OperationType.AddTo, permittedFolders);
				}

				if (CanMoveTo(nodeAsParent, out permittedFolders))
				{
					permittedOperations.Add(OperationType.MoveTo, permittedFolders);
				}

				if (CanCopyTo(nodeAsParent, out permittedFolders))
				{
					permittedOperations.Add(OperationType.CopyTo, permittedFolders);
				}
			}

			return permittedOperations;
		}

		/// <summary>
		/// Returns the set of operations permitted to be performed between the node and the given folder.
		/// </summary>
		/// <param name="destinationFolder">The folder to which permitted operations shall be evaluated.</param>
		/// <returns>The set of operations permitted to be performed between the node and the given folder.</returns>
		public virtual IEnumerable<OperationType> PermittedOperationsTo(BrowserFolder destinationFolder)
		{
			var permittedOperations = new List<OperationType>();
			if (CanAddTo(destinationFolder))
			{
				permittedOperations.Add(OperationType.AddTo);
			}

			if (CanMoveTo(destinationFolder))
			{
				permittedOperations.Add(OperationType.MoveTo);
			}

			if (CanCopyTo(destinationFolder))
			{
				permittedOperations.Add(OperationType.CopyTo);
			}

			return permittedOperations;
		}

		/// <summary>
		/// Determines whether a new node can be added to the node.
		/// </summary>
		/// <param name="permittedTypes">The permitted types of nodes that can be added to the node.</param>
		/// <returns><c>true</c> if a new node can be added to the node; otherwise <c>false</c>.</returns>
		public abstract bool CanAddNew(out IEnumerable<NodeType> permittedTypes);

		/// <summary>
		/// Determines whether a new node of a given type with a given set of initialisation parameters can be added to the node.
		/// </summary>
		/// <param name="nodeType">The type of node to determine whether a new node of that type can be added to the node.</param>
		/// <param name="initialisationParameters">The initialisation parameters that would be used to create the new node.</param>
		/// <returns><c>true</c> if a new node of the given type with the given initialisation parameters can be added to the node; 
		/// otherwise <c>false</c>.</returns>
		public abstract bool CanAddNew(NodeType nodeType, object[] initialisationParameters = null);

		/// <summary>
		/// Determines whether a new node of a given type can be added to the node.
		/// </summary>
		/// <param name="nodeType">The type of node to determine whether a new node of that type can be added to the node.</param>
		/// <param name="permittedFolders">The folders that are permitted to receive a new node of the given type.</param>
		/// <returns><c>true</c> if a new node of the given type can be added to the node; otherwide <c>false</c>.</returns>
		public abstract bool CanAddNew(NodeType nodeType, out IEnumerable<BrowserFolder> permittedFolders);

		/// <summary>
		/// Determines whether a new node of a given type with a given set of initialisation parameters can be added to the node.
		/// </summary>
		/// <param name="nodeType">The type of node to determine whether a new node of that type can be added to the node.</param>
		/// <param name="initialisationParameters">The initialisation parameters that would be used to create the new node.</param>
		/// <param name="permittedFolders">The folders that are permitted to receive a new node of the given type.</param>
		/// <returns><c>true</c> if a new node of the given type with the given initialisation parameters can be added to the node; 
		/// otherwise <c>false</c>.</returns>
		public abstract bool CanAddNew(NodeType nodeType, object[] initialisationParameters, out IEnumerable<BrowserFolder> permittedFolders);

		/// <summary>
		/// Determines whether the node can be added to a given node as a link node.
		/// </summary>
		/// <param name="node">The node to receive the new link node.</param>
		/// <param name="permittedFolders">The folders under the receiving node permitted to receive the new link node.</param>
		/// <returns><c>true</c> if the node can be added to the given node as a link node.</returns>
		public virtual bool CanAddTo(DefinitionNode node, out IEnumerable<BrowserFolder> permittedFolders)
		{
			node = ArgumentValidation.AssertNotNull(node, nameof(node));
			if (node.IsRootNode || node == this || ParentNode == node || node.Ascendants.Contains(this))
			{
				permittedFolders = new BrowserFolder[] { };
				return false;
			}

			// Get the link node type and target object based on the LinkAs configuration under the node type configuration
			var linkNodeType = Type.Configuration.LinkAs?.LinkNodeType ?? Type;
			var linkTargetObject = Type.Configuration.LinkAs?.LinkTargetObjectFunc?.Invoke(TargetObject) ?? TargetObject;

			// Filter the full set of folders to find the set of folders that may receive the new link node
			permittedFolders = node.AllFolders
				.Where(f => f != ParentFolder)
				.Where(f => !f.Configuration.IsPropertyLinksFolder)
				.Where(f => f.AllowsLinkNodes)
				.Where(f => f.Configuration.PermittedTypes.Contains(linkNodeType))
				.Where(f => !f.Nodes.OfType<LinkNode>().Any(link => link.LinkedDefinitionNode == node))
				.Where(f => (f.IsInverse ? f.Configuration.InverseFolderConfigurationIn(linkNodeType.Configuration)
						.PermittedTypeConfigurations[node.Type.Id].CanAddPredicate?.Invoke(linkTargetObject, node.TargetObject)
					: f.Configuration.PermittedTypeConfigurations[linkNodeType.Id].CanAddPredicate?.Invoke(node.TargetObject, linkTargetObject)) ?? true);

			return permittedFolders.Count() > 0;
		}

		/// <summary>
		/// Determines whether the node can be added to a given node as a link node.
		/// </summary>
		/// <param name="node">The node to receive the new link node.</param>
		/// <returns><c>true</c> if the node can be added to the given node as a link node.</returns>
		public virtual bool CanAddTo(DefinitionNode node)
		{
			return CanAddTo(node, out _);
		}

		/// <summary>
		/// Determines whether the node can be added to a given folder as a link node.
		/// </summary>
		/// <param name="folder">The folder to receive the new link node.</param>
		/// <returns><c>true</c> if the node can be added to the given folder as a link node.</returns>
		public virtual bool CanAddTo(BrowserFolder folder)
		{
			folder = ArgumentValidation.AssertNotNull(folder, nameof(folder));
			if (folder.Parent.Is<DefinitionNode>(out var parentDefinitionNode) && CanAddTo(parentDefinitionNode, out var permittedFolders))
			{
				return permittedFolders.Contains(folder);
			}

			return false;
		}

		/// <summary>
		/// Adds the node to a given node as a link node.
		/// </summary>
		/// <param name="node">The node to which to add the new link node.</param>
		/// <returns>The newly created link node.</returns>
		public virtual LinkNode AddTo(DefinitionNode node)
		{
			node = ArgumentValidation.AssertNotNull(node, nameof(node));
			if (!CanAddTo(node, out var permittedFolders) || permittedFolders.Count() != 1)
			{
				// Can't add to the given destination node, possibly because not exactly one folder that can receive the given node.
				throw new ArgumentException(string.Format(ExceptionMessage.CannotAddToDestinationNode, Type.DisplayName, DisplayId,
					node.Type.DisplayName, node.DisplayId), nameof(node));
			}

			return AddTo(Validatable.Validated(permittedFolders.Single()));
		}

		/// <summary>
		/// Adds the node to a given folder as a link node.
		/// </summary>
		/// <param name="folder">The folder to which to add the new link node.</param>
		/// <returns>The newly created link node.</returns>
		public virtual LinkNode AddTo(BrowserFolder folder)
		{
			folder = ArgumentValidation.AssertNotNull(folder, nameof(folder));
			if (!CanAddTo(folder))
			{
				// Can't add to the given folder
				throw new ArgumentException(string.Format(ExceptionMessage.CannotAddToDestinationFolder, Type.DisplayName, DisplayId,
					folder.Path), nameof(folder));
			}

			return AddTo(Validatable.Validated(folder));
		}

		/// <summary>
		/// Adds the node to the given folder as a link node.
		/// </summary>
		/// <param name="folder">The folder to which to add the new link node.</param>
		/// <returns>The newly created link node.</returns>
		protected internal abstract LinkNode AddTo(IValidated<BrowserFolder> folder);

		/// <summary>
		/// Determines whether the node can be copied to a given node.
		/// </summary>
		/// <param name="node">The node to receive the copied node.</param>
		/// <param name="permittedFolders">The folders that are permitted to receive the copied node.</param>
		/// <returns><c>true</c> if the node can be copied to the given node; otherwise <c>false</c>.</returns>
		public virtual bool CanCopyTo(ParentNode node, out IEnumerable<BrowserFolder> permittedFolders)
		{
			node = ArgumentValidation.AssertNotNull(node, nameof(node));

			permittedFolders = node.NonSystemFolders
				.Where(f => IsLinkNode || f.AllowsDefinitionNodes)
				.Where(f => f.Configuration.PermittedTypes.Contains(Type));
			
			return permittedFolders.Count() > 0;
		}

		/// <summary>
		/// Determines whether the node can be copied to a given node.
		/// </summary>
		/// <param name="node">The node to receive the copied node.</param>
		/// <returns><c>true</c> if the node can be copied to the given node; otherwise <c>false</c>.</returns>
		public virtual bool CanCopyTo(ParentNode node)
		{
			return CanCopyTo(node, out _);
		}

		/// <summary>
		/// Determines whether the node can be copied to a given folder.
		/// </summary>
		/// <param name="node">The folder to receive the copied node.</param>
		/// <returns><c>true</c> if the node can be copied to the given folder; otherwise <c>false</c>.</returns>
		public virtual bool CanCopyTo(BrowserFolder folder)
		{
			folder = ArgumentValidation.AssertNotNull(folder, nameof(folder));
			if (CanCopyTo(folder.Parent, out var permittedFolders))
			{
				return permittedFolders.Contains(folder);
			}

			return false;
		}

		/// <summary>
		/// Copies the node to the given node.
		/// </summary>
		/// <param name="node">The node to receive the copied node.</param>
		/// <returns>The copied node.</returns>
		public virtual BrowserNode CopyTo(ParentNode node)
		{
			if (!CanCopyTo(node, out var permittedFolders) || permittedFolders.Count() != 1)
			{
				// Can't copy to the given destination node, possibly because not exactly one folder that can receive the given node.
				throw new ArgumentException(string.Format(ExceptionMessage.CannotCopyToDestinationNode, Type.DisplayName, DisplayId,
					node.Type.DisplayName, node.DisplayId), nameof(node));
			}

			return new NodeCopier().Copy(this, Validatable.Validated(permittedFolders.Single()));
		}

		/// <summary>
		/// Copies the node to the given folder.
		/// </summary>
		/// <param name="folder">The folder to receive the copied node.</param>
		/// <returns>The copied node.</returns>
		public virtual BrowserNode CopyTo(BrowserFolder folder)
		{
			folder = ArgumentValidation.AssertNotNull(folder, nameof(folder));
			if (!CanCopyTo(folder))
			{
				// Cannot copy the node to the given folder
				throw new ArgumentException(string.Format(ExceptionMessage.CannotCopyToDestinationFolder, Type.DisplayName, DisplayId,
					folder.Path), nameof(folder));
			}

			return new NodeCopier().Copy(this, Validatable.Validated(folder));
		}

		/// <summary>
		/// Determines whether the node can be moved to a given node.
		/// </summary>
		/// <param name="node">The node to receive the moved node.</param>
		/// <param name="permittedFolders">The folders that are permitted to receive the moved node.</param>
		/// <returns><c>true</c> if the node can be moved to the given node; otherwise <c>false</c>.</returns>
		public virtual bool CanMoveTo(ParentNode node, out IEnumerable<BrowserFolder> permittedFolders)
		{
			// Validate the node
			node = ArgumentValidation.AssertNotNull(node, nameof(node));
			if (node == this || node.Ascendants.Contains(this))
			{
				permittedFolders = new BrowserFolder[] { };
				return false;
			}

			// Filter the folders to return only the folders that may receive the moved node
			permittedFolders = node.AllFolders
				.Where(f => f != ParentFolder)
				.Where(f => !f.Configuration.IsPropertyLinksFolder && !f.Configuration.IsRecycleBin)
				.Where(f => IsLinkNode || f.AllowsDefinitionNodes)
				.Where(f => f.Configuration.PermittedTypes.Contains(Type))
				.Where(f => !f.Nodes.Contains(node))
				.Where(f => f.Configuration.PermittedTypeConfigurations[Type.Id].CanAddPredicate?.Invoke(node.TargetObject, TargetObject) ?? true);

			return permittedFolders.Count() > 0;
		}

		/// <summary>
		/// Determines whether the node can be moved to a given node.
		/// </summary>
		/// <param name="node">The node to receive the moved node.</param>
		/// <returns><c>true</c> if the node can be moved to the given node; otherwise <c>false</c>.</returns>
		public virtual bool CanMoveTo(ParentNode node)
		{
			return CanMoveTo(node, out var permittedFolders) && permittedFolders.Count() == 1;
		}

		/// <summary>
		/// Determines whether the node can be moved to a given folder.
		/// </summary>
		/// <param name="folder">The node to receive the moved node.</param>
		/// <returns><c>true</c> if the node can be moved to the given folder; otherwise <c>false</c>.</returns>
		public virtual bool CanMoveTo(BrowserFolder folder)
		{
			folder = ArgumentValidation.AssertNotNull(folder, nameof(folder));
			if (folder.IsRecycleBin)
			{
				return true;
			}

			if (CanMoveTo(folder.Parent, out var permittedFolders))
			{
				return permittedFolders.Contains(folder);
			}

			return false;
		}

		/// <summary>
		/// Moves the node to a given node.
		/// </summary>
		/// <param name="node">The node to receive the moved node.</param>
		public virtual void MoveTo(ParentNode node)
		{
			if (!CanMoveTo(node, out var permittedFolders) || permittedFolders.Count() != 1)
			{
				// Can't move to the given destination node, possibly because not exactly one folder that can receive the given node.
				throw new ArgumentException(string.Format(ExceptionMessage.CannotMoveToDestinationNode, Type.DisplayName, DisplayId,
					node.Type.DisplayName, node.DisplayId), nameof(node));
			}

			new NodeMover(assureDestinationIds: true).Move(this, Validatable.Validated(permittedFolders.Single()));
		}

		/// <summary>
		/// Moves the node to a given folder.
		/// </summary>
		/// <param name="folder">The folder to which to move the node.</param>
		public virtual void MoveTo(BrowserFolder folder)
		{
			if (!CanMoveTo(folder))
			{
				// Can't copy to the given folder
				throw new ArgumentException(string.Format(ExceptionMessage.CannotMoveToDestinationFolder, Type.DisplayName, DisplayId,
					folder.Path), nameof(folder));
			}

			new NodeMover(assureDestinationIds: true).Move(this, Validatable.Validated(folder));
		}

		/// <summary>
		/// Deletes the node.
		/// </summary>
		public virtual void Delete()
		{
			Delete(false);
		}

		/// <summary>
		/// Deletes the node.
		/// </summary>
		/// <param name="deletePermanently">Stipulates whether to move the node to the recycle bin or delete the node permanently, simply
		/// removing it from its parent folder.</param>
		protected internal virtual void Delete(bool deletePermanently)
		{
			if (ParentFolder == null)
			{
				// Delete operation is valid only for nodes that have been added to another node
				throw new InvalidOperationException(ExceptionMessage.CannotDeleteNodeWithNoParent);
			}

			var originalFolder = ParentFolder;
			if (deletePermanently)
			{
				ParentFolder.Remove(this, true);
			}
			else
			{
				if (RootNode == null)
				{
					// Cannot determine recycle bin to which to move the deleted node because it has no root node.
					throw new InvalidOperationException(ExceptionMessage.CannotMoveNodeWithNoRootNodeToRecycleBin);
				}

				new NodeMover(assureDestinationIds: false).Move(this, Validatable.Validated(RootNode.RecycleBin));
			}

			NodeActivityLog.LogNodeDeleted(this, originalFolder, deletePermanently);
		}

		/// <summary>
		/// Determines whether the node is a descendant of the given node.
		/// </summary>
		/// <param name="node">The node to evaluate.</param>
		/// <returns><c>true</c> if the node is a descendant of the given node; otherwise <c>false</c>.</returns>
		public virtual bool IsDescendantOf(BrowserNode node)
		{
			return Ascendants.Contains(node);
		}

		/// <summary>
		/// Determines whether the node is an ascendant of the given node.
		/// </summary>
		/// <param name="node">The node to evaluate.</param>
		/// <returns><c>true</c> if the node is an ascendant of the given node; otherwise <c>false</c>.</returns>
		public virtual bool IsAscendantOf(BrowserNode node)
		{
			node = ArgumentValidation.AssertNotNull(node, nameof(node));
			return node.Ascendants.Contains(this);
		}

		/// <summary>
		/// Validates the definition node using the configured validators for the node type.
		/// </summary>
		/// <returns><c>true</c> if the node is valid; otherwise <c>false</c>.</returns>
		/// <remarks>
		/// The configured node validators will add validation results to the node, as well as potentially other 
		/// related nodes as required and appropriate.
		/// </remarks>
		public virtual void Validate()
		{
			Type.Configuration.Validators.ForEach(validator => validator.ValidateNode(this));
		}

		/// <summary>
		/// Applies a validation result to the node.
		/// </summary>
		/// <typeparam name="TNodeValidationResultType">The type of node validation result to apply.</typeparam>
		/// <param name="displayMessage">The display message for the new validation result.</param>
		/// <remarks>
		/// This method is intended to be used only by node validators.  Configured node validators are invoked through
		/// the <see cref="Validate"/> method.  This method will remove any pre-existing validation results specifically
		/// for this node of the given validation result type.  It will also record the validation result against all 
		/// ascendant nodes.
		/// </remarks>
		public virtual void ApplyValidationResult<TNodeValidationResultType>(string displayMessage)
			where TNodeValidationResultType : NodeValidationResultType
		{
			if (!Type.Configuration.Validators.Any(validator => validator.ResultTypes.Contains(typeof(TNodeValidationResultType))))
			{
				// A result type of {0} is not expected for a node type of {1} based on the declared result types for all node 
				// validators configured for that type of node.
				throw new ArgumentException(string.Format(ExceptionMessage.UnexpectedValidationResultTypeForNodeType, 
					typeof(TNodeValidationResultType).Name, Type.DisplayName), nameof(TNodeValidationResultType));
			}

			var resultType = NodeValidationResultType.DeclaredTypes[typeof(TNodeValidationResultType).Name];
			var validationResult = new NodeValidationResult(this, displayMessage, resultType);
			new[] { this }.Union(Ascendants).ForEach(thisOrAscendant => thisOrAscendant.ValidationResults
				.Where(result => result.Type == resultType && result.SourceNode == this).ToList()
				.ForEach(result => thisOrAscendant.RemoveValidationResult(result)));
			new[] { this }.Union(Ascendants).ForEach(ascendant => ascendant.AddValidationResult(validationResult));
		}

		/// <summary>
		/// Adds a validation result to the node.
		/// </summary>
		/// <param name="validationResult">The validation result to add.</param>
		protected internal virtual void AddValidationResult(NodeValidationResult validationResult)
		{
			_validationResults.Add(validationResult);
		}

		/// <summary>
		/// Removes a validation result from the node.
		/// </summary>
		/// <param name="validationResult">The validation result to remove.</param>
		protected internal virtual void RemoveValidationResult(NodeValidationResult validationResult)
		{
			_validationResults.Remove(validationResult);
		}
	}
}
