﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MI.Framework.ObjectBrowsing.Configuration
{
	/// <summary>
	/// Configuration for the display ID of a node type.
	/// </summary>
	internal class DisplayIdConfiguration
	{
		/// <summary>
		/// The suffix to automatically append to display IDs during copy operations in order to assure their availability at the destination.
		/// </summary>
		public string IdClashOnCopySuffix { get; internal set; } = " - Copy";

		/// <summary>
		/// The suffix to automatically append to display IDs during move operations in order to assure their availability at the destination.
		/// </summary>
		public string IdClashOnMoveSuffix { get; internal set; } = " - Moved";

		/// <summary>
		/// The property configuration for the display ID.
		/// </summary>
		internal PropertyConfiguration Property { get; set; }

		/// <summary>
		/// A function that determines the display ID prefix for a new definition node.
		/// </summary>
		internal Func<object[], string> PrefixFunc { get; set; }

		/// <summary>
		/// A function that finds the unavailable display IDs in a given folder path, excluding a set of objects that shall not be considered
		/// in determining the unavailable IDs.
		/// </summary>
		internal Func<FolderPath, IEnumerable<object>, IQueryable<string>> FindUnavailableIdsFunc { get; set; }

		/// <summary>
		/// Indicates whether the display ID for the node type is globally unique across all nodes, including all root nodes.
		/// </summary>
		internal bool IsGloballyUnique { get; set; }
	}
}
