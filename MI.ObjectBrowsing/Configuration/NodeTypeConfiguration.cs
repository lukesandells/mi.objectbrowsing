﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Core;
using MI.Core.Validation;
using MI.Framework.ObjectBrowsing.Validation;

namespace MI.Framework.ObjectBrowsing.Configuration
{
	/// <summary>
	/// The configuration for a type of node.
	/// </summary>
	public class NodeTypeConfiguration
	{
		/// <summary>
		/// Identifies the type of node to which the configuration pertains.
		/// </summary>
		public Guid NodeTypeId { get => _nodeTypeId; internal set => _nodeTypeId = value; }
		private Guid _nodeTypeId;

		/// <summary>
		/// The native .NET type for the configured node type.
		/// </summary>
		internal Type NodeType => _nodeType;
		private Type _nodeType;

		/// <summary>
		/// The type of target object for the node type.
		/// </summary>
		internal Type TargetObjectType => _targetObjectType;
		private Type _targetObjectType;

		/// <summary>
		/// Indicates whether nodes with this configuration are their own target object or whether they reference a different target object.
		/// </summary>
		internal bool IsSelfReferencing => _targetObjectType == _nodeType;

		/// <summary>
		/// The initialiser configuration.
		/// </summary>
		/// <remarks>
		/// Note: Must be public so that client code can determine what arguments must be provided when creating a new node.
		/// </remarks>
		public InitialiserConfiguration Initialiser { get; set; } = new InitialiserConfiguration();

		/// <summary>
		/// The configuration for linking definition nodes as a different type of node.
		/// </summary>
		internal LinkAsConfiguration LinkAs { get; set; }

		/// <summary>
		/// The display name for the node type.
		/// </summary>
		internal string DisplayName { get; set; }

		/// <summary>
		/// The display ID configuration for the node type.
		/// </summary>
		internal DisplayIdConfiguration DisplayId { get; set; } = new DisplayIdConfiguration();

		/// <summary>
		/// The configuration of the description property of the node type.
		/// </summary>
		internal PropertyConfiguration DescriptionProperty { get; set; }

		/// <summary>
		/// The target object pre-fetch strategy to be used when performing operations on nodes of the configured node type.
		/// </summary>
		/// <remarks>
		/// The pre-fetch strategy provides target object type-specific logic for pre-fetching target objects when performing
		/// operations on their corresponding nodes.  Pre-fetching improves performance by eliminating the "N+1" selects problem
		/// when that would otherwise occur when loading a node hierarchy from the database.
		/// </remarks>
		public ITargetObjectPreFetchStrategy PreFetchStrategy { get; internal set; }

		/// <summary>
		/// The configured properties for the configured node type.
		/// </summary>
		public IReadOnlyDictionary<Guid, PropertyConfiguration> Properties => _properties;
		private Dictionary<Guid, PropertyConfiguration> _properties = new Dictionary<Guid, PropertyConfiguration>();

		/// <summary>
		/// The configured folders for the configured node type.
		/// </summary>
		internal IEnumerable<FolderConfiguration> Folders => _folders.Values;
		private IDictionary<Guid, FolderConfiguration> _folders = new Dictionary<Guid, FolderConfiguration>();

		/// <summary>
		/// The configured primary folder for the configured node type.
		/// </summary>
		internal FolderConfiguration PrimaryFolder => _primaryFolder;
		internal FolderConfiguration _primaryFolder;

		/// <summary>
		/// Indicates whether the configured node type has a configured primary folder.
		/// </summary>
		internal bool HasPrimaryFolder => PrimaryFolder != null;

		/// <summary>
		/// The configured property links folder for the configured node type.
		/// </summary>
		internal FolderConfiguration PropertyLinksFolder => _propertyLinksFolder;
		internal FolderConfiguration _propertyLinksFolder;

		/// <summary>
		/// An action to be performed when a new node of the configured type is initialised.
		/// </summary>
		internal Action<object, FolderPath> OnInitialiseAction;

		/// <summary>
		/// An action to be performed when a node of the configured type is copied.
		/// </summary>
		internal Action<object, object> OnCopyAction;

		/// <summary>
		/// Actions to be performed when a node of the configured type is moved to a node of a particular type.
		/// </summary>
		internal IDictionary<Type, Action<object, object>> OnMoveToActions = new Dictionary<Type, Action<object, object>>();

		/// <summary>
		/// Actions to be performed when a node of the configured type is added to a node of a particular type.
		/// </summary>
		internal IDictionary<Type, Action<object, object>> OnAddToActions = new Dictionary<Type, Action<object, object>>();

		/// <summary>
		/// An action to be performed when a node of the configured type is deleted.
		/// </summary>
		internal Action<object> OnDeleteAction;

		/// <summary>
		/// The configured node validators for the configured type.
		/// </summary>
		internal ISet<INodeValidator> Validators { get; } = new HashSet<INodeValidator>();

		/// <summary>
		/// Custom node operations.
		/// </summary>
		internal IDictionary<OperationType, Func<BrowserNode, bool>> CustomNodeOperations { get; } = 
			new Dictionary<OperationType, Func<BrowserNode, bool>>();

		/// <summary>
		/// Custom folder operations to be applied to all folders.
		/// </summary>
		internal IDictionary<OperationType, Func<BrowserFolder, bool>> CustomFolderOperations { get; } =
			new Dictionary<OperationType, Func<BrowserFolder, bool>>();

		/// <summary>
		/// Initialises a new instance of the <see cref="NodeTypeConfiguration"/> class.
		/// </summary>
		/// <param name="nodeType">The native .NET type of the node.</param>
		/// <param name="targetObjectType">The type of target object for the node. May be the same as the node type if the node is self-refereencing.</param>
		internal NodeTypeConfiguration(Type nodeType, Type targetObjectType)
		{
			_nodeType = ArgumentValidation.AssertNotNull(nodeType, nameof(nodeType));
			_targetObjectType = ArgumentValidation.AssertNotNull(targetObjectType, nameof(targetObjectType));
		}

		/// <summary>
		/// Adds a property configuration to the node type configuration.
		/// </summary>
		/// <param name="propertyConfiguration">The property configuration to add.</param>
		internal void AddProperty(PropertyConfiguration propertyConfiguration)
		{
			if (Properties.ContainsKey(propertyConfiguration.MemberId))
			{
				// Property {0} with member ID {1} already declared for {2} node type
				throw new ArgumentException(string.Format(ExceptionMessage.DuplicatePropertyDeclaredForNodeType, propertyConfiguration.DisplayName, 
					propertyConfiguration.MemberId, DisplayName), nameof(propertyConfiguration));
			}

			_properties[propertyConfiguration.MemberId] = propertyConfiguration;
		}

		/// <summary>
		/// Adds a folder configuration to the node type configuration.
		/// </summary>
		/// <param name="folderConfiguration"></param>
		internal void AddFolder(FolderConfiguration folderConfiguration)
		{
			if (_folders.ContainsKey(folderConfiguration.MemberId))
			{
				// Folder with member ID {0} already declared for {1} node type.
				throw new ArgumentException(string.Format(ExceptionMessage.DuplicateFolderDeclaredForNodeType, folderConfiguration.DisplayName, 
					folderConfiguration.MemberId, DisplayName), nameof(folderConfiguration));
			}

			if (folderConfiguration.InverseFolderMemberId.HasValue && Folders.Any(f => f.InverseFolderMemberId == folderConfiguration.InverseFolderMemberId))
			{
				// Inverse folder {0} of node type {1} already declared for folder with member ID {3}.  Cannot define multiple inverse folders for a node 
				// type for the same inverse folder member.
				throw new ArgumentException(string.Format(ExceptionMessage.DuplicateInverseFolderDeclaredForNodeType, folderConfiguration.DisplayName,
					DisplayName, folderConfiguration.InverseFolderMemberId), nameof(folderConfiguration));
			}

			// Add pre-registered custom folder operations to the new folder, as long as it isn't a system managed folder
			if (!folderConfiguration.SystemManaged)
			{
				CustomFolderOperations.ForEach(kvp => folderConfiguration.CustomOperations.Add(kvp.Key, kvp.Value));
			}

			if (folderConfiguration.IsPrimary)
			{
				if (_primaryFolder != null)
				{
					// Cannot configure more than one primary folder for a node
					throw new ArgumentException(string.Format(ExceptionMessage.MultiplePrimaryFoldersDeclared, DisplayName), nameof(folderConfiguration));
				}

				_primaryFolder = folderConfiguration;
			}

			if (folderConfiguration.IsPropertyLinksFolder)
			{
				_propertyLinksFolder = folderConfiguration;
			}

			_folders.Add(folderConfiguration.MemberId, folderConfiguration);
		}

		/// <summary>
		/// Gets the folder configuration for the folder with a given node type member ID.
		/// </summary>
		/// <param name="memberId">The node type member ID for the folder to retrieve.</param>
		/// <returns>The folder configuration for the folder with a given node type member ID.</returns>
		public FolderConfiguration FolderWithMemberId(Guid memberId)
		{
			if (_folders.TryGetValue(memberId, out var folder))
			{
				return folder;
			}

			return null;
		}
	}
}
