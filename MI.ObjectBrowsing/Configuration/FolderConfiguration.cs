﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MI.Framework.ObjectBrowsing.Configuration
{
	/// <summary>
	/// The configuration for a folder member of a node type.
	/// </summary>
	public class FolderConfiguration
	{
		/// <summary>
		/// The configuration for a permitted node type within a folder.
		/// </summary>
		internal class PermittedTypeConfiguration
		{
			/// <summary>
			/// The configuration of the folder to which the permitted type configuration pertains.
			/// </summary>
			internal FolderConfiguration FolderConfiguration => _folderConfiguration;
			private FolderConfiguration _folderConfiguration;

			/// <summary>
			/// Identifies the inverse property member of all definition nodes of the permitted type in the configured folder.
			/// </summary>
			/// <remarks>
			/// When a definition node is added to a folder, one of its properties may reference the parent node of that folder.
			/// The inverse property configuration allows the object browsing framework to automatically set the value of this property
			/// of an added or removed definition node to the parent node (if added) or null (if removed).  It is not required that
			/// permitted types have an inverse property corresponding to the parent node type of the folder.
			/// </remarks>
			internal Guid? InversePropertyMemberId { get; set; }
			
			/// <summary>
			/// An action to be performed when a node of the permitted type is added to the folder.
			/// </summary>
			internal Action<object, object> AddAction { get; set; }

			/// <summary>
			/// An action to be performed when a node of the permitted type is removed from the folder.
			/// </summary>
			internal Action<object, object> RemoveAction { get; set; }

			/// <summary>
			/// A predicate to provide supplementary logic to determine whether an existing node of the permitted type may be added to the folder.
			/// </summary>
			internal Func<object, object, bool> CanAddPredicate { get; set; }

			/// <summary>
			/// A predicate to provide supplementary logic to determine whether a new node of the permitted type may be added to the folder.
			/// </summary>
			internal Func<object, object[], bool> CanAddNewPredicate { get; set; }

			/// <summary>
			/// Initialises a new instance of the <see cref="PermittedTypeConfiguration"/> class.
			/// </summary>
			/// <param name="folderConfiguration">The configuration of the folder to which the permitted type pertains.</param>
			internal PermittedTypeConfiguration(FolderConfiguration folderConfiguration)
			{
				_folderConfiguration = folderConfiguration;
			}
		}

		/// <summary>
		/// Synchronisation monitor for initialisation of the property links folder and recycle bin folder configurations.
		/// </summary>
		private static object _syncRoot = new object();

		/// <summary>
		/// The configuration for the property links folder member of all definition node types.
		/// </summary>
		public static FolderConfiguration PropertyLinksFolder
		{
			get
			{
				lock (_syncRoot)
				{
					if (_propertyLinksFolder == null)
					{
						// Never change this ID.  If this ID is changed, then previously persisted property links folders
						// will have the wrong member ID in the database.
						_propertyLinksFolder = new FolderConfiguration(new Guid("94c701d2-27ac-47ba-91fd-0729468506ea"))
						{
							PermitsAllTypes = true,
							AllowsDefinitionNodes = false,
							DisplayName = string.Empty,
							IsInverse = false,
							IsPrimary = false,
							IsPropertyLinksFolder = true,
							IsRecycleBin = false
						};
					}

					return _propertyLinksFolder;
				}
			}
		}
		private static FolderConfiguration _propertyLinksFolder;

		/// <summary>
		/// The recycle bin folder configuration for all root node types.
		/// </summary>
		public static FolderConfiguration RecycleBin
		{
			get
			{
				lock (_syncRoot)
				{
					if (_recycleBin == null)
					{
						// Never change this ID.  If this ID is changed, then the previously persisted recycle bins
						// will have the wrong member ID in the database.
						_recycleBin = new FolderConfiguration(new Guid("c5fa5763-4b6f-44ef-a185-9c03b180455d"))
						{
							PermitsAllTypes = true,
							AllowsDefinitionNodes = true,
							DisplayName = string.Empty,
							IsInverse = false,
							IsPrimary = false,
							IsPropertyLinksFolder = false,
							IsRecycleBin = true
						};
					}

					return _recycleBin;
				}
			}
		}
		private static FolderConfiguration _recycleBin;

		/// <summary>
		/// The node type member ID of the folder.
		/// </summary>
		internal Guid MemberId => _memberId;
		private Guid _memberId;

		/// <summary>
		/// Identifies the inverse folder member ID of one or more node types to which an inverse folder member pertains.
		/// </summary>
		internal Guid? InverseFolderMemberId { get; set; }

		/// <summary>
		/// Indicates whether the folder permits all types of nodes.
		/// </summary>
		internal bool PermitsAllTypes { get; set; }

		/// <summary>
		/// The collection of types permitted to be held in the folder.
		/// </summary>
		internal IEnumerable<NodeType> PermittedTypes => PermitsAllTypes ? NodeType.RegisteredTypes : _permittedTypes;
		private ISet<NodeType> _permittedTypes = new HashSet<NodeType>();

		/// <summary>
		/// The folder-specific configurations for the types permitted to be held in the folder.
		/// </summary>
		internal IReadOnlyDictionary<Guid, PermittedTypeConfiguration> PermittedTypeConfigurations => PermitsAllTypes ?
			NodeType.RegisteredTypes.ToDictionary(type => type.Id, type => new PermittedTypeConfiguration(this)) : _permittedTypeConfigurations;
		private Dictionary<Guid, PermittedTypeConfiguration> _permittedTypeConfigurations = new Dictionary<Guid, PermittedTypeConfiguration>();

		/// <summary>
		/// The unresolved set of permitted type configurations that are configured during node mapping and then used to initialise the 
		/// <see cref="PermittedTypeConfiguration"/> collection when the folder configuation is compiled.
		/// </summary>
		private Dictionary<Type, PermittedTypeConfiguration> _unresolvedPermittedTypeConfigurations = new Dictionary<Type, PermittedTypeConfiguration>();

		/// <summary>
		/// Custom operations for the folder.
		/// </summary>
		internal IDictionary<OperationType, Func<BrowserFolder, bool>> CustomOperations { get; } =
			new Dictionary<OperationType, Func<BrowserFolder, bool>>();

		/// <summary>
		/// The configured display name for the folder.
		/// </summary>
		internal string DisplayName { get; set; } = string.Empty;

		/// <summary>
		/// Indicates whether the folder is a primary folder.
		/// </summary>
		internal bool IsPrimary { get; set; }

		/// <summary>
		/// Indicates whether the folder permits definition nodes.
		/// </summary>
		internal bool AllowsDefinitionNodes { get; set; }

		/// <summary>
		/// Indicates whether the folder permits link nodes.
		/// </summary>
		internal bool AllowsLinkNodes { get; set; }

		/// <summary>
		/// Indicates whether the folder is a system-managed folder.  The contents of system-managed folders are managed by the framework.
		/// </summary>
		internal bool SystemManaged => IsPropertyLinksFolder || IsInverse || IsRecycleBin;

		/// <summary>
		/// Indicates whether the folder is a property links folder.  Property links are created to hold references to definition nodes that 
		/// are get and set as values of definition node properties.
		/// </summary>
		internal bool IsPropertyLinksFolder { get; set; }

		/// <summary>
		/// Indicates whether the folder is an inverse folder.
		/// </summary>
		/// <remarks>
		/// The contents of inverse folders are automatically generated by the framework based on the contents of their corresponding inverse 
		/// folder across all definition nodes of the types permitted by the folder.
		/// </remarks>
		internal bool IsInverse { get; set; }

		/// <summary>
		/// Indicates whether the folder is a recycle bin.
		/// </summary>
		/// <remarks>
		/// Each configured root node has a single recycle bin into which all nodes belonging to that root node are placed when deleted, but
		/// not deleted permanently.  This allows delete operations to be undone when node transactions are rolled back.
		/// </remarks>
		internal bool IsRecycleBin { get; set; }

		/// <summary>
		/// Specifies the ordering of the folder in the collection of a node's folders.
		/// </summary>
		internal int Ordering
		{
			get
			{
				if (IsPrimary) return 1;
				if (IsPropertyLinksFolder) return 3;
				if (IsInverse) return 4;
				return 2;
			}
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="FolderConfiguration"/> class.
		/// </summary>
		/// <param name="memberId">The node type member ID of the new folder configuration.</param>
		internal FolderConfiguration(Guid memberId)
		{
			_memberId = memberId;
		}

		/// <summary>
		/// Returns the configuration of the inverse folder corresponding to the folder in a given node type configuration.
		/// </summary>
		/// <param name="nodeTypeConfiguration">The configuration of the node type for which to retrieve the inverse folder configuration.</param>
		/// <returns>The configuration of the inverse folder corresponding to the folder in a given node type configuration.</returns>
		internal FolderConfiguration InverseFolderConfigurationIn(NodeTypeConfiguration nodeTypeConfiguration)
		{
			return IsInverse ? nodeTypeConfiguration.FolderWithMemberId(InverseFolderMemberId.Value)
				: nodeTypeConfiguration.Folders.SingleOrDefault(fc => fc.InverseFolderMemberId == MemberId);
		}

		/// <summary>
		/// Adds a new permitted type to the folder configuration with a given permitted type configuration.
		/// </summary>
		/// <param name="targetObjectType">The new type of target object permitted by the configured folder.</param>
		/// <param name="permittedTypeConfiguration">The folder-specific permitted type configuration.</param>
		internal void AddPermittedType(Type targetObjectType, PermittedTypeConfiguration permittedTypeConfiguration)
		{
			if (_unresolvedPermittedTypeConfigurations.ContainsKey(targetObjectType))
			{
				// Permitted type already configured
				throw new ArgumentException(string.Format(ExceptionMessage.PermittedTypeAlreadyConfigured, targetObjectType.Name), nameof(targetObjectType));
			}

			_unresolvedPermittedTypeConfigurations.Add(targetObjectType, permittedTypeConfiguration);
		}

		/// <summary>
		/// Resolves the target object types permitted by the folder to their corresponding node types.
		/// </summary>
		internal void ResolvePermittedTypes()
		{
			if (_unresolvedPermittedTypeConfigurations.Count > 0 && _permittedTypes.Count == 0)
			{
				foreach (var kvp in _unresolvedPermittedTypeConfigurations)
				{
					var nodeType = NodeType.For(kvp.Key);
					_permittedTypes.Add(nodeType);
					_permittedTypeConfigurations.Add(nodeType.Id, kvp.Value);
				}
			}
		}

		/// <summary>
		/// Gets the configuration for the folder with the given member ID.
		/// </summary>
		/// <param name="nodeTypeId">The node type ID.</param>
		/// <param name="memberId">The member ID.</param>
		/// <returns>The configuration for the folder with the given member ID.</returns>
		internal static FolderConfiguration WithMemberId(Guid nodeTypeId, Guid memberId)
		{
			var folderConfiguration = NodeType.WithId(nodeTypeId)?.Configuration.FolderWithMemberId(memberId);
			if (folderConfiguration == null)
			{
				// No folder is configured for a member with the given ID
				throw new ArgumentException(string.Format(ExceptionMessage.NoFolderConfiguredWithGivenId, memberId, nodeTypeId), nameof(memberId));
			}

			return folderConfiguration;
		}
	}
}
