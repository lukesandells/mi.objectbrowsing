﻿using System;
using System.Collections.Generic;
using MI.Core.Validation;
using MI.Framework.ObjectBrowsing.Validation;

namespace MI.Framework.ObjectBrowsing.Configuration
{
	/// <summary>
	/// A property configuration for a configured node type.
	/// </summary>
	public class PropertyConfiguration
	{
		/// <summary>
		/// Uniquely identifies the configured property member within the corresponding node type.
		/// </summary>
		public Guid MemberId => _memberId;
		private Guid _memberId;

		/// <summary>
		/// The native .NET type of the value held by the configured property.
		/// </summary>
		public Type NativeType => Accessor.MemberType;

		/// <summary>
		/// The node type of the value held by the configured property where the property holds a reference to a definition node.
		/// </summary>
		public NodeType NodeType => IsNodeType ? (_nodeType ?? (_nodeType = NodeType.For(NativeType))) : null;
		private NodeType _nodeType;

		/// <summary>
		/// Indicates whether the property holds a reference to a definition node.
		/// </summary>
		public bool IsNodeType => _isNodeType ?? (_isNodeType = NodeType.IsConfiguredFor(NativeType)).Value;
		private bool? _isNodeType;

		/// <summary>
		/// The group name for the configured property. Group names allow node properties to be grouped for display purposes.
		/// </summary>
		public string GroupName { get; set; }

		/// <summary>
		/// The property accessor used to read/write property values from/to definition node target objects.
		/// </summary>
		internal Accessor Accessor => _accessor;
		private Accessor _accessor;

		/// <summary>
		/// The display name of the configured property.
		/// </summary>
		public string DisplayName => _displayName;
		private string _displayName;

		/// <summary>
		/// An optional formula for determining the value of the configured property.
		/// </summary>
		internal Func<DefinitionNode, object> FormulaFunc { get; set; }

		/// <summary>
		/// Indicates whether the value of a configured property is defined by a formula.
		/// </summary>
		internal bool HasFormula => FormulaFunc != null;

		/// <summary>
		/// Indicates whether the configured property is read only.
		/// </summary>
		public bool IsReadOnly
		{
			get => _isReadOnly ?? HasFormula || Accessor.AccessorType == AccessorType.ReadOnlyProperty;
			set => _isReadOnly = value;
		}
		private bool? _isReadOnly;

		/// <summary>
		/// Indicates whether the configured property supports a single value being applied to multiple instances of the property across multiple
		/// definition nodes.
		/// </summary>
		internal bool SupportsBatchUpdate { get; set; }

		/// <summary>
		/// The configured input validators for the configured property.
		/// </summary>
		internal ISet<IInputValidator> Validators { get; } = new HashSet<IInputValidator>();

		/// <summary>
		/// The formula for determining if a property is applicable or not
		/// </summary>
		internal Func<DefinitionNode, bool> ApplicableIfFunc
		{
			get => _applicableIfFunc ?? (o => true);
			set => _applicableIfFunc = value;
		}

		private Func<DefinitionNode, bool> _applicableIfFunc;

		/// <summary>
		/// Initialises a new instance of the <see cref="PropertyConfiguration"/> class.
		/// </summary>
		/// <param name="memberId">The unique member ID of the property within its corresponding node type.</param>
		/// <param name="declaringTargetObjectType">The declaring target object type with the field or property member 
		/// corresponding to the new property configuration.</param>
		/// <param name="memberName">The name of the field or property member within the declaring target object type
		/// corresponding to the new property configuration.</param>
		/// <param name="displayName">The display name of the property to which the new configuration applies.</param>
		internal PropertyConfiguration(Guid memberId, Type declaringTargetObjectType, string memberName, string displayName)
		{
			_memberId = memberId;
			_accessor = new Accessor(declaringTargetObjectType, memberName);
			_displayName = ArgumentValidation.AssertNotNullOrEmpty(displayName, nameof(displayName));
		}
	}
}
