﻿using System;
using System.Reflection;
using MI.Core.Validation;

namespace MI.Framework.ObjectBrowsing.Configuration
{
	/// <summary>
	/// Accesses (reads/writes) values of a field or property member of an object.
	/// </summary>
	internal class Accessor
	{
		/// <summary>
		/// The type declaring the field or property member.
		/// </summary>
		public Type DeclaringType => _declaringType;
		private Type _declaringType;

		/// <summary>
		/// The type of value read and/or written by the accessor.
		/// </summary>
		public Type MemberType => _fieldInfo != null ? _fieldInfo.FieldType : _propertyInfo.PropertyType;

		/// <summary>
		/// The name of the field or property member to be read or written.
		/// </summary>
		public string MemberName => ((MemberInfo)_fieldInfo ?? _propertyInfo).Name;

		/// <summary>
		/// The field member information if the accessor reads/writes a field.
		/// </summary>
		private FieldInfo _fieldInfo;

		/// <summary>
		/// The property member information if the accessor reads/writes a property.
		/// </summary>
		private PropertyInfo _propertyInfo;

		/// <summary>
		/// Identifies the type of accessor.
		/// </summary>
		public AccessorType AccessorType => _accessorType;
		private AccessorType _accessorType;

		/// <summary>
		/// Initialises a new instance of the <see cref="Accessor"/> class.
		/// </summary>
		/// <param name="declaringType">The type declaring the field or property member to be read and/or written.</param>
		/// <param name="memberName">The name of the field or property member to be read and/or written.</param>
		public Accessor(Type declaringType, string memberName)
		{
			_declaringType = ArgumentValidation.AssertNotNull(declaringType, nameof(declaringType));
			memberName = ArgumentValidation.AssertNotNullOrEmpty(memberName, nameof(memberName));
			var flags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy;

			// Try to find a property first
			_propertyInfo = _declaringType.GetProperty(memberName, flags);
			if (_propertyInfo != null)
			{
				if (_propertyInfo.CanRead && _propertyInfo.CanWrite)
				{
					_accessorType = AccessorType.Property;
				}
			}

			// If not a property, try to find a field by applying a field naming strategy to the member name
			if (_accessorType != AccessorType.Property)
			{
				_fieldInfo = _declaringType.GetField(memberName, flags)
					?? _declaringType.GetField("_" + memberName, flags)
					?? _declaringType.GetField("m_" + memberName, flags);

				if (_propertyInfo != null && _propertyInfo.CanRead)
				{
					if (_fieldInfo != null)
					{
						_accessorType = AccessorType.NoSetter;
					}
					else
					{
						_accessorType = AccessorType.ReadOnlyProperty;
					}
				}
				else if (_fieldInfo != null)
				{
					_accessorType = AccessorType.Field;
				}
				else
				{
					// No way to get read access.  Read access is required at a minimum.
					throw new ArgumentException(string.Format(ExceptionMessage.NoReadAccessForAccessor, memberName, declaringType.FullName),
						nameof(memberName));
				}
			}
		}

		/// <summary>
		/// Gets the value of the member accessed by the accessor for a given object.
		/// </summary>
		/// <param name="obj">The object for which the member value shall be retrieved.</param>
		/// <returns>The value of the member accessed by the accessor for a given object.</returns>
		public object GetValue(object obj)
		{
			if (AccessorType == AccessorType.Field)
			{
				return _fieldInfo.GetValue(obj);
			}
			
			return _propertyInfo.GetValue(obj);
		}

		/// <summary>
		/// Sets the value of the member accessed by the accessor for a given object.
		/// </summary>
		/// <param name="obj">The object for which the member value shall be set.</param>
		/// <param name="value">The value to which to set the member of the given object.</param>
		public void SetValue(object obj, object value)
		{
			if (AccessorType == AccessorType.ReadOnlyProperty)
			{
				throw new InvalidOperationException(string.Format(ExceptionMessage.CannotWriteBecauseAccessorIsReadOnly, 
					MemberName, DeclaringType.FullName));
			}

			if (AccessorType == AccessorType.Field)
			{
				_fieldInfo.SetValue(obj, value);
			}

			_propertyInfo.SetValue(obj, value);
		}
	}
}
