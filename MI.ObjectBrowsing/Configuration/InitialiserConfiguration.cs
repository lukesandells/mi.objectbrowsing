﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Core.Validation;

namespace MI.Framework.ObjectBrowsing.Configuration
{
	/// <summary>
	/// Configures the initialiser for a node type.
	/// </summary>
	public class InitialiserConfiguration
	{
		/// <summary>
		/// The ordered collection of properties, the values of which are provided (in the same order) when creating a new node of the 
		/// type of node to which the initialiser configuration pertains.
		/// </summary>
		public IEnumerable<PropertyConfiguration> Properties => _properties;
		private IList<PropertyConfiguration> _properties = new List<PropertyConfiguration>();

		/// <summary>
		/// Adds a new property to the initialiser configuration.
		/// </summary>
		/// <param name="property">The configuration of the property to add to the initialiser configuration.</param>
		internal void AddProperty(PropertyConfiguration property)
		{
			property = ArgumentValidation.AssertNotNull(property, nameof(property));
			if (_properties.Any(p => p.MemberId == property.MemberId))
			{
				// Initialiser already contains property {0} with member ID {1}
				throw new ArgumentException(string.Format(ExceptionMessage.DuplicateInitialiserProperty, property.DisplayName, property.MemberId), 
					nameof(property));
			}

			_properties.Add(property);
		}
	}
}
