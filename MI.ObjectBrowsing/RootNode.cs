﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Core.Validation;

namespace MI.Framework.ObjectBrowsing
{
	public class RootNode : ParentNode
	{
		public override bool IsLinkNode => false;
		public override bool IsDefinitionNode => false;
		public override bool IsRootNode => true;

		public virtual RecycleBin RecycleBin => _recycleBin = _recycleBin
			?? AllFolders.Where(bf => bf.IsRecycleBin).SingleOrDefault().Cast<RecycleBin>()
			?? new RecycleBin(this);
		private RecycleBin _recycleBin;

		protected RootNode()
		{
			RootNode = this;
		}

		public RootNode(Guid rootNodeTypeId) : base(rootNodeTypeId)
		{
			RootNode = this;
		}

		public override bool CanAddTo(DefinitionNode node, out IEnumerable<BrowserFolder> permittedFolders)
		{
			// Root nodes can't be added to anything
			permittedFolders = new BrowserFolder[] { };
			return false;
		}

		protected internal override LinkNode AddTo(IValidated<BrowserFolder> folder)
		{
			// Root nodes can't be added to anything
			throw new NotSupportedException(ExceptionMessage.RootNodesCannotBeAddedToAnything);
		}

		public override bool CanCopyTo(ParentNode node, out IEnumerable<BrowserFolder> permittedFolders)
		{
			// Root nodes can't be copied to anything
			permittedFolders = new BrowserFolder[] { };
			return false;
		}

		public override bool CanMoveTo(ParentNode node, out IEnumerable<BrowserFolder> permittedFolders)
		{
			// Root nodes can't be moved to anything
			permittedFolders = new BrowserFolder[] { };
			return false;
		}

		public override void MoveTo(BrowserFolder folder)
		{
			// Root nodes can't be moved to anything
			throw new NotSupportedException(ExceptionMessage.RootNodesCannotBeMovedToAnything);
		}

		public override void Delete()
		{
			// Root nodes can't be deleted because they can't be put in their own recycle bin.
			throw new NotSupportedException(ExceptionMessage.RootNodesCannotBeDeleted);
		}

		public override string ToString()
		{
			return "Root Node: " + TargetObject.GetType().Name;
		}
	}
}
