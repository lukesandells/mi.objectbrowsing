﻿using System;
using System.Collections.Generic;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	public class NodeChangesListener : NodeActivityListener
	{
		public IEnumerable<BrowserNode> AddedNodes => _addedNodes;
		private ISet<BrowserNode> _addedNodes = new HashSet<BrowserNode>();

		public IEnumerable<BrowserNode> UpdatedNodes => _updatedNodes;
		private ISet<BrowserNode> _updatedNodes = new HashSet<BrowserNode>();

		public IEnumerable<BrowserNode> DeletedNodes => _deletedNodes;
		private ISet<BrowserNode> _deletedNodes = new HashSet<BrowserNode>();

		public IEnumerable<BrowserNode> PermanentlyDeletedNodes => _permanentlyDeletedNodes;
		private ISet<BrowserNode> _permanentlyDeletedNodes = new HashSet<BrowserNode>();

		public override void NodeCreated(BrowserNode newNode)
		{
			_addedNodes.Add(newNode);
		}

		public override void NodeDeleted(BrowserNode deletedNode, BrowserFolder deletedFrom, bool permanentlyDeleted)
		{
			if (permanentlyDeleted)
			{
				_permanentlyDeletedNodes.Add(deletedNode);
			}
			else
			{
				_deletedNodes.Add(deletedNode);
			}
		}

		public override void NodeMoved(BrowserNode movedNode, BrowserFolder movedFrom, BrowserFolder movedTo)
		{
			_updatedNodes.Add(movedNode);
		}

		public override void NodePropertySet(DefinitionNode node, Guid memberId, object formerValue)
		{
			_updatedNodes.Add(node);
		}
	}


}
